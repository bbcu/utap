.. raw:: html

    <script type="text/javascript">
    if (String(window.location).indexOf("readthedocs") !== -1) {
        window.location.replace('https://utap2.readthedocs.io/en/latest/');
    }
    </script>


.. _manual-main:

UTAP: User-friendly Transcriptome Analysis Pipeline
===================================================

RNA-Seq technology is routinely used to characterize the transcriptome and detect gene expression differences among cell types, genotypes and conditions. Advances in short-read sequencing instruments such as Illumina Next-Seq, have yielded easy-to-operate machines, with higher throughput, at a lower price per base. However, processing this data requires bioinformatics expertise to tailor and execute specific solutions for each type of library preparation.

In order to enable fast and user-friendly data analysis, we developed an intuitive and scalable transcriptome pipeline that executes the full process, starting from sequences (RNA-Seq and bulk MARS-Seq), and ending with sets of differentially expressed genes. Output files are placed in a structured folder system, and summarization of the results is displayed in a rich and comprehensive report containing dozens of plots, tables and links.


.. toctree::
    :maxdepth: 2

    rst/demo-site
    rst/rna-mars-seq-results
    rst/installation
    rst/user-guide
    rst/releases
    rst/source-code

License
=======
UTAP is licensed under GNU General Public License version 3. License needed for commercial use.


Author
======

Refael Kohen (until version 1.0.7),

refael.kohen@weizmann.ac.il, refael.kohen@gmail.com

support:
utap@weizmann.ac.il

Bioinformatics unit at Life Sciences Core Facilities (LSCF)

Weizmann Institute of Science, Rehovot 76100, Israel.

Acknowlegments
==============

Please cite: Kohen R, Barlev J, Hornung G, Stelzer G, Feldmesser E, Kogan K, Safran M, Leshkowitz D: UTAP: User-friendly Transcriptome Analysis Pipeline. BMC Bioinformatics 2019, 20(1):154.
