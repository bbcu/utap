"""
This file is part of UTAP!.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import smtplib
from email.mime.text import MIMEText
from string import Template

from analysis.backend.settings import NOTIFICATION_INTERNAL_MAILING_LIST, SMTP_SERVER_NAME, SMTP_SERVER_PORT, \
    BBCU_INTERNAL, PIPELINE_URL, PIPELINE_URL_HTTP


class EmailNotifier(object):
    def __init__(self, smtp_host=SMTP_SERVER_NAME, smtp_port=SMTP_SERVER_PORT,
                 internal_mailing_list=NOTIFICATION_INTERNAL_MAILING_LIST):
        self.host = smtp_host
        self.port = smtp_port
        self.internal_mailing_list = internal_mailing_list

    def _notify(self, notification, recipients, bcc):
        msg = MIMEText(notification.content)
        if BBCU_INTERNAL:
            sender = 'utap@localhost.localdomain'
        else:
            sender = self.internal_mailing_list[0]
        msg['Subject'] = notification.subject
        msg['From'] = sender
        msg['To'] = ','.join(recipients)
        msg['Bcc'] = ','.join(bcc)
        # Send the message via our own SMTP server, but don't include the envelope header.
        s = smtplib.SMTP(host=self.host, port=self.port)
        s.sendmail(sender, recipients + bcc, msg.as_string())
        s.quit()

    def notify_user(self, notification, user_email):
        self._notify(notification, [user_email], self.internal_mailing_list)


class Notification(object):
    def __init__(self, subject, template_params):
        self.subject = subject
        self.content = self.template.substitute(**template_params)


class ConfirmUserNotification(Notification):
    def __init__(self, username, first_name, last_name, domain, activate_url, uidb64, token, password):
        subj = 'Activate Your UTAP Account'
        template_params = dict(username=username, first_name=first_name, last_name=last_name, domain=domain,
                               activate_url=activate_url, uidb64=uidb64, token=token, password=password)
        super(ConfirmUserNotification, self).__init__(subj, template_params)

    template = Template("""
    Dear $first_name $last_name,

    Please click on the link below to confirm your registration with an username "$username":

    http://$domain/$activate_url/$uidb64/$token

    For upload input data by ssh (if your administrator enabled this feature), use this password: $password
      
      
    Sincerely,
 
    Life Sciences Core Facilities
    Bioinformatics team
 
    For questions:
 
    Regarding accessing the data and troubleshooting - vitaly.golodnitsky@weizmann.ac.il,
    UTAP - bioinformatics pipeline assistance - utap@weizmann.ac.il, http://utap.wexac.weizmann.ac.il
    Bioinformatics analysis:
    dena.leshkowitz@weizmann.ac.il
    ester.feldmesser@weizmann.ac.il
    gil.stelzer@weizmann.ac.il
    bareket.dassa@weizmann.ac.il
    noa.wigoda@weizmann.ac.il
    
    """)


class ResetPassNotification(Notification):
    def __init__(self, username, first_name, last_name, domain, activate_url, uidb64, token):
        subj = 'Reset password of UTAP Account'
        template_params = dict(username=username, first_name=first_name, last_name=last_name, domain=domain,
                               activate_url=activate_url, uidb64=uidb64, token=token)
        super(ResetPassNotification, self).__init__(subj, template_params)

    template = Template("""
    Dear $first_name $last_name,

    To initiate the password reset process for your $username TestSite Account, click the link below:

    http://$domain/$activate_url/$uidb64/$token
      
      
    Sincerely,
 
    Life Sciences Core Facilities
    Bioinformatics team
 
    For questions:
 
    Regarding accessing the data and troubleshooting - vitaly.golodnitsky@weizmann.ac.il,
    UTAP - bioinformatics pipeline assistance - utap@weizmann.ac.il, http://utap.wexac.weizmann.ac.il
    Bioinformatics analysis:
    dena.leshkowitz@weizmann.ac.il
    ester.feldmesser@weizmann.ac.il
    gil.stelzer@weizmann.ac.il
    bareket.dassa@weizmann.ac.il
    noa.wigoda@weizmann.ac.il
    
    """)


class FailedNotification(Notification):
    def __init__(self, pipeline, data_name, user_full_name, stderr, logs, errors):
        subj = 'UTAP run failed'
        template_params = dict(pipeline=pipeline, data_name=data_name, user_full_name=user_full_name, stderr=stderr,
                               logs=logs, errors=errors)
        super(FailedNotification, self).__init__(subj, template_params)

    template = Template("""
    Dear $user_full_name,

    The UTAP run $data_name of the \"$pipeline\" pipeline failed.

    The errors are reported in the following files:
      $stderr
      $logs
    The following errors were found:
      $errors
      
      
    Sincerely,
 
    Life Sciences Core Facilities
    Bioinformatics team
 
    For questions:
 
    Regarding accessing the data and troubleshooting - vitaly.golodnitsky@weizmann.ac.il,
    UTAP - bioinformatics pipeline assistance - utap@weizmann.ac.il, http://utap.wexac.weizmann.ac.il
    Bioinformatics analysis:
    dena.leshkowitz@weizmann.ac.il
    ester.feldmesser@weizmann.ac.il
    gil.stelzer@weizmann.ac.il
    bareket.dassa@weizmann.ac.il
    noa.wigoda@weizmann.ac.il

    """)


class AnalysisEndedNotification(Notification):
    def __init__(self, pipeline, analysis_name, user_full_name, result_link, parameters_link, server=PIPELINE_URL_HTTP):
        subj = 'Your UTAP analysis results are now available'
        template_params = dict(pipeline=pipeline, analysis_name=analysis_name, user_full_name=user_full_name,
                               server=server, result_link=result_link, parameters_link=parameters_link)
        super(AnalysisEndedNotification, self).__init__(subj, template_params)

    template = Template("""

    Dear $user_full_name,

    We have completed the $analysis_name analysis with the \"$pipeline\" pipeline.
    You can see the output at the UTAP website:
    $server

    The complete analysis report file is available at:
    $result_link
    The analysis ran with the parameters listed at:
    $parameters_link

    Please cite our manuscript: (Kohen et al) UTAP: User-friendly Transcriptome Analysis Pipeline. BMC Bioinformatics 2019, 20(1):154
    
      
    Sincerely,
 
    Life Sciences Core Facilities
    Bioinformatics team
 
    For questions:
 
    Regarding accessing the data and troubleshooting - vitaly.golodnitsky@weizmann.ac.il,
    UTAP - bioinformatics pipeline assistance - utap@weizmann.ac.il, http://utap.wexac.weizmann.ac.il
    Bioinformatics analysis:
    dena.leshkowitz@weizmann.ac.il
    ester.feldmesser@weizmann.ac.il
    gil.stelzer@weizmann.ac.il
    bareket.dassa@weizmann.ac.il
    noa.wigoda@weizmann.ac.il
    
    """)


class Notifier(object):
    def __init__(self, notification, user_email):
        EmailNotifier().notify_user(notification, user_email)
