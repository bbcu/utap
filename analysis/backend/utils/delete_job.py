"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import subprocess
from glob import glob

from analysis.backend.utils.consts import RunStatus
from analysis.backend.utils.general import logger
from analysis.models import Analysis


def delete_job(job_name, pipeline, dry_run=True):
    obj = Analysis.objects.get(name=job_name)
    job_path = str(obj.output_folder)
    if not pipeline.endswith('DESeq2') and glob(os.path.join(job_path, 'Advanced_analysis', '*')):
        msg = 'Cannot delete because there are files in Advanced_analysis folder. You need remove these files (through file system) before you could delete the folder'
        logger.info(msg)
        return Exception(msg)
    try:
        if not dry_run:
            process = subprocess.Popen("lsof +D %s" % job_path, stdout=subprocess.PIPE, shell=True, close_fds=True)
            stdout, stderr = process.communicate()
            '''
            example of stdout:
            ==================
            lsof: WARNING: can't stat() overlay file system /var/lib/docker/overlay/fbe3fedade8949b3e4cdb72b45ac5fb4ff7e7465a592ca0c80690630c7b5a361/merged Output information may be incomplete.
            lsof: WARNING: can't stat() tmpfs file system /var/lib/docker/containers/d530ba35b9d87c79e34188f6fbf75336cc411b7ccf32364e500eeda6237b3c16/shm Output information may be incomplete.
            COMMAND     PID    USER   FD   TYPE DEVICE SIZE/OFF       NODE NAME
            snakemake 18548 bioinfo  cwd    DIR   0,68     1814 7839404573 20171107_112219_marstest8_transcriptome_MARS-seq
            snakemake 19389 bioinfo  255r   REG   0,41     1765 7403709045 20171107_112219_marstest8_transcriptome_MARS-seq/.nfs00000001b94ba275000100fc
            snakemake 19410 bioinfo    1w   REG   0,41    18909 7396306835 20171107_112219_marstest8_transcriptome_MARS-seq/.nfs00000001b8daaf93000100fd
            snakemake 19410 bioinfo    2w   REG   0,41    18909 7396306835 20171107_112219_marstest8_transcriptome_MARS-seq/.nfs00000001b8daaf93000100fd
            ....sometimes there is empty line.....
            '''
            if stdout:
                outstr = str(stdout)
                logger.info(stdout)
                raw_lines = outstr.split('\n')
                lines = [line for line in raw_lines if
                         line and not line.startswith('lsof')]  # remove empty lines and lines with lsof
                if len(lines) > 1:
                    jobs_all = [line.split()[1] for line in lines[1:] if line]  # skip on header line
                    fds = [line.split()[3] for line in lines[1:] if line]
                    jobs = []
                    for job, fd in zip(jobs_all, fds):
                        if fd != 'cwd':  # Sometimes kill this process causes to problems
                            jobs.append(job)
                    for job in set(jobs):
                        process = subprocess.Popen('kill -9 %s' % job, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                                   shell=True, close_fds=True)
                        stdout, stderr = process.communicate()
                        if stderr:
                            logger.info('Cannot kill job %s of nfs files within folder %s' % (job, job_path))
                            raise Exception(stderr)
                        else:
                            logger.info('Killed process id: %s of nfs file within folder %s' % (job, job_path))
        if pipeline.endswith('DESeq2'):
            date = '_'.join(job_name.split('_')[:2])
            to_delete1 = '%s/*%s' % (job_path, date)
            to_delete2 = '%s/*_reports/*%s' % (job_path, date)
            to_delete_paths = to_delete1 + "\n" + to_delete2
            if dry_run:
                return to_delete_paths
            logger.info("rm -rf %s; rm -rf %s" % (to_delete1, to_delete2))
            process = subprocess.Popen("rm -rf %s; rm -rf %s" % (to_delete1, to_delete2), stdout=None, stderr=None,
                                       shell=True, close_fds=True)
        else:
            to_delete_paths = job_path
            if dry_run:
                return to_delete_paths
            logger.info("rm -rf %s" % job_path)
            process = subprocess.Popen("rm -rf %s" % job_path, stdout=None, stderr=None, shell=True, close_fds=True)
        # I deleted it, in order the form will not need to wait to the end of the deletion.
        # stdout, stderr = process.communicate()
        # if stderr:
        #     raise Exception(stderr)
        obj.status = obj.status + " " + RunStatus.DELETED
        obj.save()
    except Exception as e:
        msg = 'Cannot delete the output folder of job: %s, %s. Please try again.' % (
            job_name, str(e).decode('utf-8').strip())
        return Exception(msg)
    logger.info('Deleted folder: %s' % to_delete_paths)
    return to_delete_paths
