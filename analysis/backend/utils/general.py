"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os
import re
import subprocess
from collections import namedtuple
from glob import glob
import paramiko
import hashlib
import collections

from analysis.backend.settings import LAB_DIR, MIN_DISK_FREE, PIPELINE_OUTPUT_FOLDER, BBCU_INTERNAL, COLLABORATIONS_DIR, \
    QUEUE_STAFF, QUEUE_USERS, USER_CLUSTER, PIPELINE_SERVER, PIPELINE_SERVER_PORT, DEMO_SITE, UTAP_VERSION
from analysis.backend.utils.consts import SINGLECELL_CELRANGER_ANALYSIS_TYPE, MARSSEQ_ANALYSIS_TYPE

logger = logging.getLogger(__name__)

DiskUsage = namedtuple('DiskUsage', 'total used free')

def render_constant_vars(request): #constant variables that are beeing renderd im all templates via the Django settings file
    return {
        "server_port": str(PIPELINE_SERVER_PORT),
        "demosite": DEMO_SITE,
        "version": UTAP_VERSION,
        "pipeline_server": PIPELINE_SERVER,}

def get_cluster_queue(request):
    if not BBCU_INTERNAL:
        return QUEUE_USERS
    elif request.user.is_superuser:
        if request.user.username == 'submit':
            return QUEUE_USERS
        else:
            return QUEUE_STAFF
    else:
        return QUEUE_USERS


def disk_usage(path):
    """Return disk usage statistics about the given path.

    Will return the namedtuple with attributes: 'total', 'used' and 'free',
    which are the amount of total, used and free space, in bytes.
    """
    st = os.statvfs(path)
    free = int((st.f_bavail * st.f_frsize) / (1024 ** 3))
    total = int((st.f_blocks * st.f_frsize) / (1024 ** 3))
    used = int((st.f_blocks - st.f_bfree) * st.f_frsize) / (1024 ** 3)
    return DiskUsage(total, used, free)


def open_remote_ssh_session(server_name, server_user, server_pass):
    try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server_name, 22, username=server_user, password=server_pass)
        return ssh
    except Exception as e:
        raise Exception('Cannot connect to Stefan with ssh: %s' % str(e))


#genral validation
def validate_dir(dir):
    reg = re.compile('^[a-zA-Z0-9/._-]+\Z')
    if not bool(reg.match(dir)):
        return False, 'The path:\n %s \ncontains invalid charcters. \n Please use only valid charcters (a-z,A-Z,0-9,_,.,/).' % dir
    if not os.access(dir, os.R_OK) or not os.access(dir, os.W_OK):
        return False,'You don\'t have permissions to the input: %s. Please verify your input permissions before running the pipeline.' % dir
    return True, None

#validate input directory
def folder_exists(dir, input=True):
    if not os.path.isdir(dir):
        return False, 'No such folder: ' + dir
    is_valid, message= validate_dir(dir)
    if is_valid and input:
        if len(os.listdir(dir)) == 0:
            return False, 'The folder: ' + dir + ' is empty'
    else:
        return is_valid, message
    return True,True

#validate input file (used for exp: in pipeline DEseq2 from conts matrix
def validate_input_file(dir):
    if  not os.path.isfile(dir):
        return False, 'No such file: ' + dir + ', please select another existing file'
    is_valid, message = validate_dir(dir)
    if is_valid:
        if os.stat(dir).st_size == 0:
            return False, 'The file: \n %s is empty. Please verify that all your files exist before running the pipeline' % dir
    else:
        return is_valid, message
    return True,True


# free space in Gb
def has_space_on_disc(dir):
    free = disk_usage(dir).free
    if free < MIN_DISK_FREE:
        return False, 'The free disk space of your lab is %sG. The analysis may require until %sG of free space. Please remove old file before running the analysis.' % (
            free, MIN_DISK_FREE)
    else:
        return True, 'The free disk space of your lab is %sG.' % free





def root_dir_of_cellranger(root_dir):
    out_dir = os.path.join(root_dir, 'outs')
    general_message='The folders \"outs\" and \"fastq_path\" are expected to be inside the input directory %s. \n but the following error occurred: \n' %root_dir
    is_valid_outs,message_outs = folder_exists(out_dir)
    if not is_valid_outs:
        return is_valid_outs, general_message + message_outs
    fastq_path = os.path.join(out_dir, 'fastq_path')
    is_valid_fastq,message_fastq = folder_exists(fastq_path)
    if not is_valid_fastq:
        return is_valid_fastq, general_message + message_fastq
    return True, fastq_path



#
# def validate_dir(root_dir):
#     logger.info("inside function")
#     if ' ' in root_dir:
#         return False, 'The path:\n %s \ncannot contain spaces characters, please change the name of the directories.' % root_dir
#     if not os.access(root_dir, os.R_OK) or not os.access(root_dir, os.W_OK):
#         return False,'You don\'t have permissions to the input: %s. Please verify your input permissions before running the pipeline.' % root_dir
#     if os.path.isdfile(root_dir):
#         if os.stat(root_dir).st_size == 0:
#             return False, 'The file: \n %s is empty. Please verify that all your files exist before running the pipeline' % root_dir
#         else:
#             return True,''
#     return True, ''
#



def get_samples_dimultiplexing(root_dir):
    is_valid_root_dir, message_root_dir = folder_exists(root_dir)
    if not is_valid_root_dir:
        return is_valid_root_dir, message_root_dir
    samples=[]
    num_samples =0
    for fastq in os.listdir(root_dir):
        fastq_path = os.path.join(root_dir, fastq)
        if fastq_path.endswith(tuple(['fq', 'fastq', 'fq.gz', 'fastq.gz'])):
            is_valid_fastq, message_fastq = validate_input_file(fastq_path)
            if not is_valid_fastq:
                return is_valid_fastq, message_fastq
            num_samples += 1
            samples.append(fastq)
            logger.info(samples)
    if num_samples >= 1:
        return  sorted(samples), num_samples
    else:
        return False, "No fastq files were found"


        

def samples_in_fastq_dir(root_dir, pipeline):
    logger.info('User is running %s pipeline and chose the following input directory  %s' % (pipeline, root_dir))
    if pipeline == SINGLECELL_CELRANGER_ANALYSIS_TYPE:
        status, path = root_dir_of_cellranger(root_dir)
        if not status:
            return status, path
        root_dir = path
    else:
        is_valid_root_dir, message_root_dir = folder_exists(root_dir)
        if not is_valid_root_dir:
            return is_valid_root_dir, message_root_dir
    num_samples = 0
    samples = []
    paired_end = None
    for sample_dir in os.listdir(root_dir):
        if sample_dir.startswith('.'):
            continue
        non_fastq_folders=['FastQC','FastQCinput', 'Stats', 'Reports', 'SampleSheet.csv', 'Mars-seq_users.xlsx', 'Undetermined']
        if sample_dir in non_fastq_folders:
            continue
        full_sample_dir = os.path.join(root_dir, sample_dir)
        is_valid_full_sample_dir, message_full_sample_dir = folder_exists(full_sample_dir)
        if not is_valid_full_sample_dir:
            return is_valid_full_sample_dir, message_full_sample_dir
        s = sample_dir
        listdir = os.listdir(full_sample_dir)
        included_extensions = ['fq', 'fastq', 'fq.gz', 'fastq.gz']
        not_fastq_files = [f for f in listdir if not f.startswith('.') and not any(f.endswith(ext) for ext in included_extensions)]
        if not_fastq_files:
            return False, 'The sample %s \ncontains non-fastq files (you need to delete them).' % sample_dir
        glob_r1 = glob(os.path.join(full_sample_dir, '*_R1*.fastq')) + glob(
            os.path.join(full_sample_dir, '*_R1*.fastq.gz')) + glob(
            os.path.join(full_sample_dir, '*_R1_0*.fastq')) + glob(
            os.path.join(full_sample_dir, '*_R1_0*.fastq.gz'))
        glob_r1 = list(set(glob_r1))
        if glob_r1:
            is_valid_fastq_R1, message_fastq_R1 = validate_input_file(glob_r1[0])
            if not is_valid_fastq_R1:
                return is_valid_fastq_R1, message_fastq_R1
        glob_r2 = glob(os.path.join(full_sample_dir, '*_R2*.fastq')) + glob(
            os.path.join(full_sample_dir, '*_R2*.fastq.gz')) + glob(
            os.path.join(full_sample_dir, '*_R2_0*.fastq')) + glob(
            os.path.join(full_sample_dir, '*_R2_0*.fastq.gz'))
        glob_r2 = list(set(glob_r2))
        if glob_r2:
            is_valid_fastq_R2, message_fastq_R2 = validate_input_file(glob_r2[0])
            if not is_valid_fastq_R2:
                return is_valid_fastq_R2, message_fastq_R2
        if paired_end is None:
            paired_end = True if glob_r2 else False
        if pipeline == MARSSEQ_ANALYSIS_TYPE and not paired_end:
            return False, 'The MARS-Seq pipeline requires paired-end data (both *R1* and *R2* files for each sample). \n Unfortunately, the sample directory contains only the *R1* file.\n Please fix and try again.\n %s' %s
        if (glob_r2 and not paired_end) or (not glob_r2 and paired_end):
            return False, 'Part of the samples are paired-end and other are single-read'
        if not glob_r1:
            return False, 'No valid *R1* fastq files in sample folder\n %s. \nThe fastq files must be in this format: %s_R[1|2]*.fq(.gz) \nor %s_R[1|2]*.fastq(.gz) \nor %s_R2.fq \nor %s_R2.fastq(.gz) ' % (
                s, s, s, s, s)
        if len(glob_r1) > 1 and not True in [True if '_R1_0' in f else False for f in glob_r1]:
            return False, 'There is more than two *R1* files in sample folder\n %s. \nIn each sample folder can be only one *R1* file, or more than one file if the file names are in this format *_R1_001*.fq, *_R1_002*.fq, etc ...' % sample_dir
        if len(glob_r2) > 1 and not True in [True if '_R2_0' in f else False for f in glob_r2]:
            return False, 'There is more than two *R2* files in sample folder\n %s. \nIn each sample folder can be only one *R2* file, or more than one file if the file names are in this format *_R2_001*.fq, *_R2_002*.fq, etc ...' % sample_dir
        if glob_r2 and (len(glob_r2) > 1 or len(glob_r1) > 1):
            if len(glob_r1) != len(glob_r2):
                return False, 'When the file names are in format *_R[1|2]_0* - the file numbers of *R1* and *R2* files must be equeal in folder %s' % sample_dir
            r2_replaced = [f.replace('R2', 'R1') for f in glob_r2]
            for r1, r2 in zip(sorted(glob_r1), sorted(r2_replaced)):
                if r1 != r2:
                    return False, 'When the file names are in format *_R[1|2]_0* - the file names of *R2* files must be equal to *R1* file names in folder %s' % sample_dir
        num_samples += 1
        samples.append(sample_dir)
    if num_samples < 1:  # at least 2 valid samples
        return False, 'No valid sample folders in\n %s' % root_dir
    logger.info('User selected these samples %s' % ' '.join(samples))
    return sorted(samples), num_samples



#when the input is a file like in pipeline DEseq2 from counts matrix
def get_samples_names(root_dir):
    is_valid_sample_dir, message_sample_dir  = validate_input_file(root_dir)
    if not is_valid_sample_dir:
        return is_valid_sample_dir, message_sample_dir
    file_extension = os.path.splitext(root_dir)[1]
    with open(root_dir, 'r') as input_file:
        lines = input_file.readlines()
        if file_extension == '.csv':
            samples = lines[0].split(',')
            num_samples = len(samples)
        elif file_extension == '.txt':
            samples = lines[0].split()
            num_samples  = len(samples)
        else:
            return False,'Please select csv or txt file format as input file.'
        d = collections.defaultdict(list)
        for line in lines:
            if file_extension == '.txt':
              id = hashlib.sha256(line.split()[0]).digest()
            else:
              id = hashlib.sha256(line.split(',')[0]).digest()
            # Or id = line[:n]
            k = id[0:2]
            v = id[2:]
            if v in d[k]:
              logger.info(line.split()[0])
              return False, 'You have the duplicated gene %s in your file (try to set the gene column as text format when recreating the file in excel)' % line.split()[0]
            else:
              d[k].append(v)
    #samples = [s.strip('\n') for s in samples]
    samples = [re.sub('[^0-9a-zA-Z-_.]+', '_', s.strip()) for s in samples]
    samples = list(set(samples))
    logger.info(samples)
    logger.info('User selected these samples %s' % ' '.join(samples))
    return sorted(samples), num_samples


def validate_sample(sample):
    reg = re.compile(r'[^a-zA-Z0-9-_\s]')
    illegal_characters=reg.findall(sample)
    if bool(illegal_characters):
        illegal_char=(', ').join([str(i) for i in illegal_characters])
        return False, "The sample name %s contains the illegal character/s %s" % (sample, illegal_char)
    return True, ""


def validate_index(index_list,index,pools,index2=False):
    reg = re.compile('^[AGCT\s]+$')
    if not reg.match(index):
        logger.info(index)
        return False, "The sample's index %s contain illegal base/s. Index can contain only [AGCT] characters" % index
    if index_list:
        if len(index_list[0]) != len(index):
            return False, 'The barcodes must have the same length'
        if index in index_list :
            if pools:
                prev_ind = index_list.index(index)
                curr_ind = len(index_list)
                if pools[prev_ind] == pools[curr_ind]:
                    return False, 'Found two identical indexes %s in the same pool %s' % (index, pools[prev_ind])
            else:
                if not index2:
                    return False, 'Found two identical indexes %s' % index
    return True, ""


def check_samplesheet(samplesheet):
    clean_samplesheet = {'Sample': [], 'Index':[], 'Index2':[], 'Pool':[]}
    headers = [s for s in samplesheet[0:3] if '$' in s and s[1:] in clean_samplesheet.keys()]
    header_len= len(headers)
    logger.info(headers)
    splited_ss = samplesheet[header_len:]
    logger.info(splited_ss)
    logger.info(type(samplesheet))
    index_message=""
    if '$Index2' in headers or ('$Pool' in headers and '$Sample'in headers):
        logger.info("inside")
        for head3 in splited_ss[2::header_len]:
            if head3 :
                if '$Index2' in headers:
                    valid_head3, head3_message = validate_index(clean_samplesheet['Index2'], head3.strip(), None,True)
                    # if head3 in clean_samplesheet['Index']:
                    #     return None, "Please notice that its recommended to use unique Dual indexes"
                    clean_samplesheet['Index2'].append(head3.strip())
                else:
                    valid_head3, head3_message = validate_sample(head3)
                    # if index_message and index_message in clean_samplesheet['Index']:
                    #     ind = clean_samplesheet['Index'].index(index)
                    #     logger.info(ind)
                    #     if len(clean_samplesheet['Pool'])-1 >= ind and clean_samplesheet['Pool'][ind] == head3:
                    #        return False, 'Found two identical indexes %s in the same pool %s' % (index, head3)
                    clean_samplesheet['Pool'].append(head3.strip())
                if not (valid_head3):
                    return None, head3_message
    for samp, index in zip(splited_ss[0::header_len], splited_ss[1::header_len]):
        if samp and index:
            valid_samp, samp_meassage = validate_sample(samp)
            valid_index, index_message = validate_index(clean_samplesheet['Index'], index.strip(), clean_samplesheet['Pool'])
            if not (valid_samp and valid_index ):
                return None, samp_meassage + '\n' + index_message + '\n'
            clean_samplesheet['Sample'].append(samp.strip())
            clean_samplesheet['Index'].append(index.strip())
    logger.info(clean_samplesheet['Sample'])
    logger.info(clean_samplesheet['Index'])
    if not (clean_samplesheet['Sample'] and  clean_samplesheet['Index']):
        return None, 'The SampleSheet is not corrected: missing barcodes or sample name.'
    if len(set(clean_samplesheet['Sample'])) != len(clean_samplesheet['Sample']):
        return None, 'Some of the samples are identical'
    l = len(clean_samplesheet['Sample'])
    logger.info(l)
    # if not all(len(v)==l for k, v in clean_samplesheet.items() if v):
    #     return None, 'The number of samples is not equal to the number of indexes'
    return clean_samplesheet,""


def check_pools(samplesheet,samplesheet_pools):
    for pool in set(samplesheet['Pool']):
        if pool not in samplesheet_pools['Sample']:
            return False, "Pool names must be identical in both tables"
    return True,""



def get_user_labname_on_wexac(username):
    process = subprocess.Popen(
        'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l %s %s getent passwd | grep ^%s:' % (
            USER_CLUSTER, PIPELINE_SERVER, username),
        stdout=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate()
    if stderr:
        error_msg = 'the command \"getend passwd\" for user %s is failed: ' % username
        logger.error(error_msg)
        raise Exception(error_msg)
    return os.path.basename(os.path.dirname(stdout.split(':')[5]))


# In each login the fuction is called to check the updated lab of the user
def updated_user_dir(username):
    if not BBCU_INTERNAL:
        return os.path.join(LAB_DIR, username)

    user_dir = None
    try:
        if username == 'admin':
            user_dir = os.path.join(LAB_DIR, username)
        elif username == 'testuser' or username == 'submit':
            user_dir = os.path.join(LAB_DIR, username)
        elif get_user_labname_on_wexac(username) == "bioservices":
            user_dir = os.path.join(COLLABORATIONS_DIR, get_user_labname_on_wexac(username), 'Collaboration')
            #user_dir = os.path.join(LAB_DIR, username)
        elif 'class' in username:
            #user_dir = os.path.join(COLLABORATIONS_DIR, 'testing', 'Collaboration', 'users', username)
            user_dir = os.path.join(COLLABORATIONS_DIR, 'testing', 'Collaboration')
        else:
            user_dir = os.path.join(COLLABORATIONS_DIR, get_user_labname_on_wexac(username), 'Collaboration')
        return user_dir
    except Exception as e:
        raise Exception(
            'No %s folder within %s folder in collaboration folder for user %s, the exception is: %s' % (
                PIPELINE_OUTPUT_FOLDER, user_dir, username, str(e)))

