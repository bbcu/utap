"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
from collections import namedtuple
from xml.etree import ElementTree

from analysis.backend.utils.consts import DemultConst

logger = logging.getLogger(__name__)


class CreateSampleSheet(object):
    def __init__(self, filename, input_type, sample_sheet):  # input_type = bcl or fastq
        logger.info("inside CreateSampleSheet")
        logger.info(sample_sheet)
        self.filename = filename
        self.input_type = input_type
        # self.samples = [samp_barc[0] for samp_barc in sample_sheet]
        # self.barcodes = [samp_barc[1] for samp_barc in sample_sheet]
        self.samples=sample_sheet['Sample']
        self.index=sample_sheet['Index']
        self.index2=sample_sheet['Index2']
        self.pools=sample_sheet['Pool']
        logger.info(self.pools)
        self.bar_len = len(self.index[0])

    # def validate_samplesheet(self):
    #     if len(set(self.samples)) != len(self.samples):
    #         raise Exception('Some of the samples are identical')
    #     if len(set(self.barcodes)) != len(self.barcodes):
    #         raise Exception('Some of the barcodes are identical')
    #     invalid_chars = ['\"', '\'', '`', '?', ',', ';', '=', '@', '#', '$', '%', '^', '&', '(', ')', '[', ']',
    #                      '{', '}', '<', '>', '/', '\\', ' ', '\t']
    #     for i in xrange(len(self.samples)):
    #         for char in invalid_chars:
    #             self.samples[i] = self.samples[i].strip()
    #             self.samples[i] = self.samples[i].replace(char, '_').replace('+', '_plus_')
    #     for i in xrange(len(self.barcodes)):
    #         self.barcodes[i] = self.barcodes[i].strip()
    #     for i in xrange(len(self.barcodes)):
    #         if self.bar_len != len(self.barcodes[i]):
    #             raise Exception('The barcodes must to be in the same length')

    def write_samplesheet(self):
        if self.input_type == DemultConst.BCL_V:
            self.write_sample_sheet_Illumina()
        elif self.input_type == DemultConst.FASTQ_V:
            self.write_sample_sheet_fastq()

    def write_sample_sheet_fastq(self):
#        header = 'rd2:%s:0' % self.bar_len
    #with open(self.filename, 'w') as ssf:
#            ssf.write(header + '\n')

        if self.index2:
            ssf = open(self.filename, 'w')
            for sample, index, index2 in zip(self.samples, self.index, self.index2):
                ssf.write('%s %s-%s\n' % (sample.rstrip(), index.rstrip(), index2.rstrip()))
        else:
            if self.pools:
                for p in set(self.pools):
                    file = open(self.filename + "_" + p, 'w+')
                    for sample, index, pool in zip(self.samples, self.index, self.pools):
                        if pool == p:
                            file.write('%s %s\n' % (sample.rstrip(), index.rstrip()))
                        else:
                            continue
            else:
                ssf = open(self.filename, 'w')
                for sample, index in zip(self.samples, self.index):
                        ssf.write('%s %s\n' % (sample, index))


    def write_sample_sheet_Illumina(self):
        header = """
[Header],,,,,,,
IEMFileVersion,4,,,,,,
Date,13/11/2015,,,,,,
Workflow,GenerateFASTQ,,,,,,
Application,NextSeq_FASTQ_Only,,,,,,
Assay,TruSeq_HT,,,,,,
Description,,,,,,,
Chemistry,Default,,,,,,
,,,,,,,
[Reads],,,,,,,
80,,,,,,,
80,,,,,,,
,,,,,,,
[Settings],,,,,,,
,,,,,,,
[Data],,,,,,,
Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,%sSample_Project,Description
""" % ('index2,' if self.index2 else "")
        with open(self.filename, 'w') as ssf:
            ssf.write(header)
            if self.index2:
                for sample, index, index2 in zip(self.samples, self.index, self.index2):
                    ssf.write('%s,%s,,,,%s,%s,%s,user\n' % (sample, sample, index, index2, sample))
            else:
                for sample, barcode in zip(self.samples, self.index):
                    ssf.write('%s,%s,,,,%s,%s,user\n' % (sample, sample, barcode, sample))

            ssf.close()

Read =  namedtuple('Read', ['number', 'num_cycles', 'is_indexed', 'name'])


class RunParametersReader(object):
    @staticmethod
    def parse_reads(element_tree):
        reads = []
        num_indexed_reads = num_not_indexed_reads = 0
        for xml_read in element_tree.findall('.//Reads/Read'):
            is_indexed = True if xml_read.attrib.get('IsIndexedRead') == 'Y' else False
            if is_indexed:
                num_indexed_reads += 1
                read_name = 'I%d' % num_indexed_reads
            else:
                num_not_indexed_reads += 1
                read_name = 'R%d' % num_not_indexed_reads
            reads.append(Read(number=int(xml_read.attrib.get('Number')),
                              num_cycles=int(xml_read.attrib.get('NumCycles')),
                              is_indexed=is_indexed,
                              name=read_name))
        assert len(reads) > 0, 'Did not find read parameters in XML'
        return reads

    def parse(self, filename):
        """
        Retrieve several parameters from Illumina's run parameters file, commonly called RunInfo.xml.
        In any case of parse error, raise an IOError
        """
        try:
            element_tree = ElementTree.parse(filename)
            reads = self.parse_reads(element_tree)
            return reads
        except Exception as e:
            raise IOError(e)
