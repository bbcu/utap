"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging

from django.conf import settings as django_settings
from django.contrib.auth import load_backend, login
from django_auth_ldap.backend import LDAPBackend

from analysis.models import CustomUser

logger = logging.getLogger(__name__)


def get_user_dir(user):
    user_obj = CustomUser.objects.get(username=user)
    if user_obj.loggedas_username:
        return user_obj.loggedas_lab_path
    return user_obj.lab_path


def login_exists_user(username, request):
    """
    Utility function for forcing a login as specific user -- be careful about
    calling this carelessly :)
    """
    # Find a suitable backend.
    try:
        # Create user if not exists
        if not CustomUser.objects.filter(username=username).exists():
            user_obj = LDAPBackend().populate_user(username)
            user_obj.save()
        else:
            user_obj = CustomUser.objects.get(username=username)
    except Exception:
        return False, 'Cannot login as user: %s, No such user.' % username

    if not hasattr(user_obj, 'backend'):
        for backend in django_settings.AUTHENTICATION_BACKENDS:
            if user_obj == load_backend(backend).get_user(user_obj.pk):
                user_obj.backend = backend
                break
    # Log the user in.
    if hasattr(user_obj, 'backend'):
        try:
            login(request, user_obj)
            return True, 'You logged in as user: %s' % user_obj.username
        except Exception:
            return False, 'Cannot login as user: %s' % user_obj.username
    else:
        return False, 'To user %s has not backend' % user_obj.username


def get_user_email(username):
    obj = CustomUser.objects.get(username=username)
    user_email = obj.email
    if not '@' in user_email:
        user_email += '@weizmann.ac.il'
    return user_email
