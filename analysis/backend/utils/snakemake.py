"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os
import subprocess
import time
from string import Template

from analysis.backend.settings import JOBS_STATUS_FILE, JOB_REPEATS_NUMBER, CLUSTER_EXE, SNAKEMAKE_EXE, PIPELINE_SERVER, \
    USER_CLUSTER, CLUSTER_TYPE, DEMULTIPLEXING_MEMORY, SNAKEFILE_VERSION, UTAP_VERSION, CLUSTER_RESOURCES_PARAMS, \
    MAX_CORES

logger = logging.getLogger(__name__)


class Snakemake(object):
    def __init__(self, jobs_name, pipeline, date, snakefile_name, queue, config_file=None, num_jobs=1, num_cores=1, cluster=True):
        # commands_names is list of pairs (command, name of the step)
        self.job_name = jobs_name
        self.date = date
        self.snakefile_name = snakefile_name  # full path
        self.snake_dir = os.path.dirname(snakefile_name)
        self.cmd_file = os.path.join(self.snake_dir, 'snakemake_cmd_%s' % self.date)
        self.stdout_file = os.path.join(self.snake_dir, 'snakemake_stdout_%s' % self.date)
        self.snakefile_version = os.path.join(self.snake_dir, 'VERSION_%s.txt' % self.date)
        self.queue = queue
        self.num_jobs = num_jobs
        self.num_cores = num_cores
        self.cluster = cluster
        self.config_file = config_file if config_file else None
        self.pipeline= pipeline.replace(" ", "_")
        self.pipeline = self.pipeline.replace("-", "_")

    def rule_wildcards(self):
        return Template("""
$WILDCARDS_NAME=$WILDCARDS_VALUE
""")

    def rule_template(self):
        return Template("""
rule $NAME:
    input:
        $INPUT
    output:
        $OUTPUT
    $PARAMS
    log:
        $LOG
    threads: threads_num($THREADS, $MAX_THREADS_NUM)
    resources:
        mem_mb_per_thread=mem_per_thread($DEMULTIPLEXING_MEMORY_TOTAL, $THREADS, $MAX_THREADS_NUM),
        mem_mb_total=$DEMULTIPLEXING_MEMORY_TOTAL
    shell:
        \'\'\'
        $COMMAND > {log} 2>&1
        \'\'\'
""")

    def rule_all_template(self):
        return Template("""
rule all:
    input:
        $INPUT
""")

    def rules_functions(self):
        return """
import os
import time

def threads_num(desired_t_num, max_threads_num):
    return min(desired_t_num,max_threads_num)

def mem_per_thread(mem_mb_total,desired_t_num, max_threads_num):
    return int(mem_mb_total/threads_num(desired_t_num, max_threads_num))
        """

    # def fill_rules(self, rules, inputs, outputs, params, commands,logs, names, threads, rull_all_inputs, wildcards):
    def fill_rules(self, rules,commands_names_threads, wildcards):
        logger.info("inside fill rules")
        rules.append(self.rules_functions())
        if wildcards:
            for key in wildcards.keys():
                rules.append(self.rule_wildcards().substitute(dict(WILDCARDS_NAME=key,WILDCARDS_VALUE=wildcards[key])))
                logger.info("after wilcards fill")
        start_input="\'"+os.path.join(self.snake_dir, 'snakemake_start_pipeline_' + self.date)+"\'"
        os.system('touch %s' % os.path.join(start_input))
        time.sleep(1)
        rull_all_inputs=[]
        for rule in commands_names_threads:
            input, output, param, cmd, log, name, threads_cmd, rull_all_input =rule
            if rull_all_input:
                rull_all_inputs.append(rull_all_input)
            if not input:
                input = start_input
            if not output:
                output = "\'"+os.path.join(self.snake_dir, 'snakemake_' + name + "_" + self.date)+"\'"
                rull_all_inputs.append(output)
            if not log:
                log= "\'"+os.path.join(self.snake_dir, 'log_' + name + "_" + self.date)+"\'"
            template_params = dict(NAME=name, INPUT=input, OUTPUT=output, PARAMS= "" if not param else "params:\n        " + param, LOG=log, THREADS=str(threads_cmd),
                                   MAX_THREADS_NUM=str(MAX_CORES),
                                   DEMULTIPLEXING_MEMORY_TOTAL=str(int(DEMULTIPLEXING_MEMORY) * threads_cmd),
                                   COMMAND=cmd)
            rules.append(self.rule_template().substitute(**template_params))
        logger.info("after loop")
        template_params = dict(
            INPUT=',\n\t'.join(rull_all_inputs))
        rules.insert(len(wildcards)+1,self.rule_all_template().substitute(**template_params))
        logger.info(rules)


    def write_file(self, rules):
        with open(self.snakefile_name, 'w') as sf:
            for rule in rules:
                sf.writelines(rule)

    # Either create a new snakefile or copy exist snakefile with the next function copy_snakefile
    def create_snakefile(self, commands_names_threads, wildcards):
        logger.info(commands_names_threads)
        rules=[]
        self.fill_rules(rules,commands_names_threads , wildcards)
        self.write_file(rules)

    def copy_snakefile(self, snakefile_source):
        os.system('cp %s %s' % (snakefile_source, self.snakefile_name))
        os.system('cp %s %s' % (SNAKEFILE_VERSION, self.snakefile_version))
        os.system("sed -i '1iSnakemake version: ' %s" % (self.snakefile_version))
        os.system("sed -i '1iUTAP version: %s' %s" % (UTAP_VERSION, self.snakefile_version))

    def prepare_cmd_file(self, allowed_step=None):
        cluster_err = 'cluster_%s.error' % self.date
        cluster_out = 'cluster_%s.output' % self.date
        specific_steps, configfile = '', ''
        if allowed_step:
            specific_steps = '--nolock --forcerun %s --allowed-rules %s' % (allowed_step, allowed_step)
        if self.config_file:
            configfile = '--configfile %s' % self.config_file

        if self.cluster:
            if CLUSTER_TYPE == 'lsf':
                snakemake_cmd = 'LSB_JOB_REPORT_MAIL=N; cd %s && %s --timestamp --benchmark-repeats %s %s %s -p --jobs %s --latency-wait 120 --snakefile %s --cluster "%s -q %s -J %s -Jd %s_{rule}_{wildcards} -o %s_{rule}_{wildcards} -e %s_{rule}_{wildcards} %s " &> %s' % (
                    self.snake_dir, SNAKEMAKE_EXE, JOB_REPEATS_NUMBER, specific_steps, configfile, self.num_jobs,
                    self.snakefile_name, CLUSTER_EXE, self.queue,self.pipeline, self.pipeline, cluster_out, cluster_err, CLUSTER_RESOURCES_PARAMS,
                    self.stdout_file)
            if CLUSTER_TYPE == 'pbs':
                # Version for physics faculty - Weizmann institute (OpenPBS)
                # snakemake_cmd = 'cd %s && %s --timestamp --benchmark-repeats %s %s %s -p --jobs %s --latency-wait 120 --snakefile %s --cluster "%s -m n -q %s -l mem={resources.mem_mb_total}mb,nodes=1:ppn={threads} -o %s -e %s" &> %s' % (
                # Version of Tel-Aviv university (PBSPro)
                # snakemake_cmd = 'cd %s && %s --timestamp --benchmark-repeats %s %s %s -p --jobs %s --latency-wait 120 --snakefile %s --cluster "%s -m n -q %s -l select=1:ncpus={threads}:mem={resources.mem_mb_total}mb -o %s -e %s" &> %s' % (
                snakemake_cmd = 'cd %s && %s --timestamp --benchmark-repeats %s %s %s -p --jobs %s --latency-wait 120 --snakefile %s --cluster "%s -m n -q %s %s -J %s -Jd %s_{rule}_{wildcards} -o %s_{rule}_{wildcards} -e %s_{rule}_{wildcards}" &> %s' % (
                    self.snake_dir, SNAKEMAKE_EXE, JOB_REPEATS_NUMBER, specific_steps, configfile, self.num_jobs,
                    self.snakefile_name, CLUSTER_EXE, self.queue, CLUSTER_RESOURCES_PARAMS, self.pipeline, self.pipeline, cluster_out, cluster_err,
                    self.stdout_file)
        else:  # run on local machine
            snakemake_cmd = 'cd %s && %s --timestamp --benchmark-repeats %s %s %s -p --cores %s --latency-wait 120 --snakefile %s &> %s' % (
                self.snake_dir, SNAKEMAKE_EXE, JOB_REPEATS_NUMBER, specific_steps, configfile, self.num_cores,
                self.snakefile_name,
                self.stdout_file)
        with open(self.cmd_file, 'w') as sf:
            sf.writelines("#!/bin/bash\n")
            sf.writelines("set history = 1\n")  # For tcsh shell
            sf.writelines("export HISTSIZE=1\n")  # For bash shell
            sf.writelines(snakemake_cmd + '\n')
            sf.writelines('rc=$?\n')
            sf.writelines('echo "Command return code for job %s $rc" >> %s\n' % (self.job_name, JOBS_STATUS_FILE))
            sf.writelines('cat %s_* >> %s\n' % (cluster_err, cluster_err))
            sf.writelines('cat %s_* >> %s\n' % (cluster_out, cluster_out))
            sf.writelines('rm -f %s_* %s_*\n' % (cluster_out, cluster_err))
            sf.writelines('cp %s commands_log_%s.txt\n' % (cluster_err, self.date))
            sf.writelines('mv commands_log_%s.txt *_reports\n' % self.date)
            sf.writelines('date\n')
            sf.writelines('chmod -R ug+w %s\n' % self.snake_dir)
        os.system('chmod ug+x %s \n' % self.cmd_file)


    def run_snakefile(self):
        # close_fds: release the port, the website of the client can to continue run
        subprocess.Popen(self.cmd_file, shell=True, close_fds=True)

    def run_snakefile_ssh(self):
        # close_fds: release the port, the website of the client can to continue run
        # Example: ssh -l USERNAME SERVERNAME "qsub -o out -e err -q S com.sh"
        ssh_command = 'ssh  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l %s %s "bash -s; export HISTSIZE=1; set history = 1" < %s' % (
            USER_CLUSTER, PIPELINE_SERVER, self.cmd_file)
        logger.info(ssh_command)
        subprocess.Popen(ssh_command, shell=True, close_fds=True)
