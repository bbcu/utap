"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

# import logging
#
# logger = logging.getLogger(__name__)
# from analysis.backend.settings import QUEUE_STAFF, QUEUE_USERS, BBCU_INTERNAL
#
#
# class LSFCommand(object):
#     @staticmethod
#     def get_cluster_queue(request):
#         if not BBCU_INTERNAL:
#             return QUEUE_USERS
#         elif request.user.is_superuser:
#             if request.user.username == 'submit':
#                 return QUEUE_USERS
#             else:
#                 return QUEUE_STAFF
#         else:
#             return QUEUE_USERS
#
# @staticmethod
# def create_script(out_dir, file_out, job_name, queue, memory, num_cores, cmd):
#     """
#     Create a shell script file with the proper PBS header lines so it can be distributed to the cluster
#     """
#     MAX_BSUB_JOB_NAME_SIZE = 15
#     cluster_err = 'cluster_%s.error' % job_name
#     cluster_out = 'cluster_%s.output' % job_name
#     fid = open(file_out, 'w')
#     fid.writelines("#!/bin/bash\n")
#     fid.writelines("LSB_JOB_REPORT_MAIL=N\n")
#     fid.writelines('date \n')
#     fid.writelines(
#         "%s -n %s -q %s -o %s -e %s -R \"rusage[mem=%s]\" -R \"span[hosts=1]\" %s \n" % (
#             CLUSTER_EXE, num_cores, queue, cluster_out, cluster_err, memory, cmd))
#     fid.writelines('rc=$?\n')
#     fid.writelines('echo "Command return code for job %s $rc" >> %s\n' % (job_name, JOBS_STATUS_FILE))
#     fid.writelines('date\n')
#     fid.close()
#
# @staticmethod
# def create_run_command(job_name, out_dir, basic_command, queue, memory, processes_num, cmd_file_name):
#     cmd_file_name = re.sub('[^0-9a-zA-Z-]', '_', cmd_file_name.strip())
#     command_file = os.path.join(out_dir, "bsub_command_%s" % (cmd_file_name,))
#     LSFCommand.create_script(out_dir, command_file, job_name[:MAX_BSUB_JOB_NAME_SIZE], queue, memory,
#                              processes_num,
#                              basic_command)
#     return "cd %s && chmod 777 %s && %s %s" % (out_dir, command_file, CLUSTER_EXE, command_file)
#
#     # Example to run:
#     # ===============
#     #
#     # bcl2fastq_cluster_cmd = LSFCommand.create_run_command(self.job_name, self.user_commands_dir, bcl2fastq_cmd, self.queue,
#     #                                                       self.memory, 16, 'bcl2fastq_%s' % self.job_name)
#     # bcl2fastq_process = subprocess.Popen(bcl2fastq_cluster_cmd, shell=True)
#     # status = bcl2fastq_process.wait()
#     # if status:
#     #     logger.error('bcl2fastq run is failed on directory %s: ' % self.bcl_dir)
#     #     raise Exception('bcl2fastq run is failed on directory %s: ' % self.bcl_dir)
#     # else:
#     #     return True
