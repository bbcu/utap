import glob
import os
import re


class Errors(object):
    def __init__(self, project_name, output_folder):
        self.project_name = project_name
        self.output_folder = output_folder
        self.run_id = '_'.join(project_name.split("_")[0:2])

    def no_mapping(self):
        err = ""
        mapping = re.compile('\d_mapping')
        if not os.path.isdir(self.output_folder):
            return ""
        map_name = [filename for filename in os.listdir(self.output_folder) if mapping.search(filename)]
        new_path = os.path.join(self.output_folder, ''.join(map_name))
        if new_path:
            for file in os.listdir(new_path):
                if 'Log.final.out' in file and 'Uniquely mapped reads % |\t0.00%' in open(os.path.join(new_path, file),
                                                                                          'r').read():
                    err += "The sample %s has 0%% mapping in file: %s.\n" % (
                        file[:file.rfind("Log.final.out")], os.path.join(new_path, file))
        # if err:
        #     err += "\nRemove these samples from the input folder and run again.\n"
        return err

    def R_err(self):
        path = os.path.join(self.output_folder, "logs_" + self.run_id)
        if not os.path.isdir(path):
            return ""
        report_file = [file for file in os.listdir(path) if file.endswith("reports.txt")]
        if report_file:
            with(open(os.path.join(path, ''.join(report_file)), "r")) as f:
                for line in f.readlines():
                    if 'Error' in line or 'error' in line or 'Exception' in line:
                        return "Problem at R analysis: %s.\n" % line
        return ""

    def no_memory_and_other_errors(self):
        logs_folder = os.path.join(self.output_folder, 'logs_' + self.run_id, '*.txt')
        errors = ''
        if not os.path.isdir(logs_folder):
            return errors
        path = glob.glob(logs_folder)
        if path:
            for file in path:
                if not os.path.isdir(file):
                    with open(file, "r") as fh:
                        for line in fh:
                            if 'ERROR: number of bytes expected from the BAM bin does not agree with the actual size on disk' in line or 'IOError: [Errno 28] No space left on device' in line:
                                errors += "No space left on device, please delete old files\n"
                            elif 'Error' in line or 'error' in line or 'Exception' in line:
                                errors += "Exception error in file %s: %s\n" % (file, line)
        return errors


    def snakemake_errors(self):
        snakemake_std = os.path.join(self.output_folder, 'snakemake_stdout_' + self.run_id)
        if not os.path.isfile(snakemake_std):
            return ""
        with open(snakemake_std, "r") as fh:
            for line in fh:
                if 'Error' in line or 'error' in line or 'Exception' in line:
                    return "Exception error in snakemake file %s: %s\n" % (snakemake_std, line)
        return ""

    def all_err(self):
        #return self.no_memory_and_other_errors() + '\n' + self.R_err() + '\n' + self.no_mapping() + '\n' + self.snakemake_errors()
        return self.no_memory_and_other_errors() + '\n' + self.R_err() + '\n' + self.no_mapping() + '\n'
