"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.utils.translation import ugettext_lazy as _
from analysis.backend.settings import BBCU_INTERNAL, CELLRANGER_EXE
from analysis.backend.settings_base import RUN_LOCAL
from django import forms
import re

ADVANCED_ID = 'advanced'
BASIC_ID = 'basic'

FUTURE_DATA_TYPE = 'future'
DEMULTIPLEXING_ANALYSIS_TYPE = 'Demultiplexing'
DEMULTIPLEXING_BCL_ANALYSIS_TYPE = 'Demultiplexing_from_BCL'
DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE = 'Demultiplexing_from_FASTQ'
DEMULTIPLEXING_RUNID_ANALYSIS_TYPE = 'Demultiplexing_from_RUNID'
RNASEQ_ANALYSIS_TYPE = 'Transcriptome RNA-Seq'
MARSSEQ_ANALYSIS_TYPE = 'Transcriptome MARS-Seq'
SCRBSEQ_ANALYSIS_TYPE = 'Transcriptome SCRB-Seq'
RNASEQ_DESEQ_ANALYSIS_TYPE = 'Transcriptome RNA-Seq DESeq2'
MARSSEQ_DESEQ_ANALYSIS_TYPE = 'Transcriptome MARS-Seq DESeq2'
SCRBSEQ_DESEQ_ANALYSIS_TYPE = 'Transcriptome SCRB-Seq DESeq2'
TRANSCRIPTOME_DESEQ2_PIPELINES= [RNASEQ_DESEQ_ANALYSIS_TYPE, MARSSEQ_DESEQ_ANALYSIS_TYPE, SCRBSEQ_DESEQ_ANALYSIS_TYPE]
TRANSCRIPTOME_PIPELINES= [RNASEQ_ANALYSIS_TYPE, MARSSEQ_ANALYSIS_TYPE , SCRBSEQ_ANALYSIS_TYPE]
ALL_TRANSCRIPTOME_PIPELINES= TRANSCRIPTOME_PIPELINES + TRANSCRIPTOME_PIPELINES
CELLRANGER_VERSION=re.findall(r'cellranger-([\d{1}.]*\d+)', CELLRANGER_EXE)[0]
SINGLECELL_CELRANGER_ANALYSIS_TYPE = 'Single-cell RNA Cell Ranger' + ' V'+ CELLRANGER_VERSION
ATACSEQ_ANALYSIS_TYPE = 'ATAC-Seq'
CHIPSEQ_ANALYSIS_TYPE = 'ChIP-Seq'
RIBOSEQ_ANALYSIS_TYPE ='Ribo-Seq'
DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE = 'DESeq2 from counts matrix'
CORE_APP = ''  # Field form with this application (and advanced_id) will be written before APP title
BOWTIE = 'bowtie'
BOWTIE2 = 'BOWTIE2'
DESEQ2 = 'DESeq2'
CUTADAPT = 'CUTADAPT'
MACS2 = 'MACS2'
STAR = 'STAR'
Differential_gene_expression_analysis = 'Differential_gene_expression_analysis'
#the DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE is commented since its not working (19.1.20) after fixing the pipeline we have to uncomment it

ELEARNING_PIPELINES_LIST= [{'pipeline': RNASEQ_ANALYSIS_TYPE, 'link': "http://dors.weizmann.ac.il/course/e-learning/rna-seq-bioinformatics/"}
    ,{'pipeline': MARSSEQ_ANALYSIS_TYPE, 'link': "http://dors.weizmann.ac.il/course/e-learning/mars-seq-bioinformatics/"}]


ANALYSIS_TYPES_DISPLAY = ((RNASEQ_ANALYSIS_TYPE, _(RNASEQ_ANALYSIS_TYPE)),
                         (MARSSEQ_ANALYSIS_TYPE, _(MARSSEQ_ANALYSIS_TYPE)),
                         (SCRBSEQ_ANALYSIS_TYPE, _(SCRBSEQ_ANALYSIS_TYPE)),
                         (DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE, _(DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE)),
                         (DEMULTIPLEXING_BCL_ANALYSIS_TYPE, _(DEMULTIPLEXING_BCL_ANALYSIS_TYPE)))
# ANALYSIS_TYPES_DISPLAY = ((RNASEQ_ANALYSIS_TYPE, _(RNASEQ_ANALYSIS_TYPE)),
#                            (MARSSEQ_ANALYSIS_TYPE, _(MARSSEQ_ANALYSIS_TYPE)),
#                            (DEMULTIPLEXING_BCL_ANALYSIS_TYPE, _(DEMULTIPLEXING_BCL_ANALYSIS_TYPE)))

ANALYSIS_TYPES_HIDE = ((MARSSEQ_DESEQ_ANALYSIS_TYPE, _(MARSSEQ_DESEQ_ANALYSIS_TYPE)),
                       (RNASEQ_DESEQ_ANALYSIS_TYPE, _(RNASEQ_DESEQ_ANALYSIS_TYPE)),
                       (SCRBSEQ_DESEQ_ANALYSIS_TYPE, _(SCRBSEQ_DESEQ_ANALYSIS_TYPE)))

ANALYSIS_TYPES_DISPLAY_SUPERUSER = ANALYSIS_TYPES_DISPLAY
# For chromatin pipelines (ATAC-Seq and CHIP-seq) and DESeq from counts matrix and also single cell pipeline for now not installed inside the docker:
ANALYSIS_TYPES_NOT_LOCAL = (
    (ATACSEQ_ANALYSIS_TYPE, _(ATACSEQ_ANALYSIS_TYPE)),
    (CHIPSEQ_ANALYSIS_TYPE, _(CHIPSEQ_ANALYSIS_TYPE)),
    (DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE, _(DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE)),
)

if not RUN_LOCAL:
    ANALYSIS_TYPES_DISPLAY += ANALYSIS_TYPES_NOT_LOCAL
    ANALYSIS_TYPES_DISPLAY_SUPERUSER += ANALYSIS_TYPES_NOT_LOCAL        

if BBCU_INTERNAL: # bbcu internal means weizmann internal users
    ANALYSIS_TYPES_DISPLAY_SUPERUSER += ((DEMULTIPLEXING_RUNID_ANALYSIS_TYPE, _(DEMULTIPLEXING_RUNID_ANALYSIS_TYPE)),
                                         (SINGLECELL_CELRANGER_ANALYSIS_TYPE, _(SINGLECELL_CELRANGER_ANALYSIS_TYPE)),
                                         (RIBOSEQ_ANALYSIS_TYPE, _(RIBOSEQ_ANALYSIS_TYPE)))# For singlecell pipeline, it should be displayed for BBCU users (superuser) only
    ANALYSIS_TYPES_DISPLAY += ((DEMULTIPLEXING_RUNID_ANALYSIS_TYPE, _(DEMULTIPLEXING_RUNID_ANALYSIS_TYPE)),)
    ANALYSIS_TYPES = ANALYSIS_TYPES_DISPLAY + ANALYSIS_TYPES_HIDE + ANALYSIS_TYPES_DISPLAY_SUPERUSER
else:
    ANALYSIS_TYPES = ANALYSIS_TYPES_DISPLAY + ANALYSIS_TYPES_HIDE + ANALYSIS_TYPES_DISPLAY_SUPERUSER


# NGS: Create such APP for interior applications within the pipeline
# CONSENSUS_APP = 'Consensus' #Field form with this application (and advanced_id) will be written under this APP title

class RunStatus(object):
    CREATED = 'CREATED'
    SUBMITTED = 'SUBMITTED'
    RUNNING = 'RUNNING'
    SUCCESSFUL = 'SUCCESSFUL'
    FAILED = 'FAILED'
    KILLED = 'KILLED'
    STOPPED = 'STOPPED'
    COPY = 'COPY FASTQ FILES'
    DELETED = 'DELETED'


####################################################################################


class DemultConst(object):

    TRUE_SEQ = 'TruSeq'
    MARS_SEQ = 'MARS-Seq'
    SCRAB_SEQ = 'SCRB-Seq'
    RUN_ID_K = 'Run id from Stefan server'
    FASTQ_K = 'Fastq files'
    BCL_K = 'BCL directory'
    RUN_ID_V = 'run_id'
    FASTQ_V = 'fastq'
    BCL_V = 'bcl'

    INPUT_TYPE_TYPES = ((RUN_ID_V, _(RUN_ID_K)), (FASTQ_V, _(FASTQ_K)), (BCL_V, _(BCL_K)))
    INPUT_TYPE_CHOICES = (('', '---------')) + INPUT_TYPE_TYPES
    INPUT_TYPE_HELP = "Type of input files"
    INPUT_TYPE_LABEL = "Input type"

    PROTOCOLS_TYPES = ((TRUE_SEQ, _(TRUE_SEQ)), (MARS_SEQ, _(MARS_SEQ)),(SCRAB_SEQ, _(SCRAB_SEQ)))
    PROTOCOL_INIT = TRUE_SEQ
    PROTOCOL_HELP = "Sample preparation protocol."
    PROTOCOL_LABEL = "Sample Preparation Protocol"
    PROTOCOL_CHOICES = PROTOCOLS_TYPES

    def __init__(self):
        self.input = ""
        self.output = ""
        self.params = ""
        self.cmd = ""
        self.logs = ""
        self.name=""
        self.threads=20
        self.rull_all = ""

    def get_rule_atrr(self):
        # l = []
        # for attribute, value in self.__dict__.items():
        #     l.append(value)
        # return l
        return [self.input,self.output,self.params,self.cmd,self.logs,self.name,self.threads,self.rull_all]


    def get_fastqc_atrr(self):
        self.input = '\'{output}/{prev_folder}_{_sample_}s/{sample}/{sample}_R{strand_num}.fastq.gz\''
        self.output = '\'{output}/{folder_num}_fastqc/{sample}/{sample}_R{strand_num}_fastqc/fastqc_data.txt\''
        self.params = 'output_dir =\'{output}/{folder_num}_fastqc/{sample}\''
        self.logs = "\'{command_dir}/logs/{folder_num}_{sample}.txt\'"
        self.cmd = "mkdir -p {{params.output_dir}};" \
                     "{exe} --extract -o {{params.output_dir}} -f fastq --threads {{threads}} {{input}}"
        self.name='fastqc_{_sample_}s'
        self.rull_all="expand(\'{output}/{folder_num}_fastqc/{sample}/{sample}_R1_fastqc/fastqc_data.txt\', {_sample_}={SAMPLES})"
        return  self.get_rule_atrr()


    def get_multiqc_atrr(self):
        self.input = "expand(\'{output}/{prev_folder}_fastqc/{sample}/{sample}_R{strand_num}_fastqc/fastqc_data.txt\',{_sample_}={SAMPLES})"
        self.output = "\'{output}/{folder_num}_multiQC/multiqc_report.html\'"
        self.params = "input_dir =\'{output}/{prev_folder}_fastqc\',\n        output_dir=\'{output}/{folder_num}_multiQC\'"
        self.logs = "\'{command_dir}/logs/{folder_num}_multiQC.txt\'"
        self.cmd ="{exe} -o {{params.output_dir}} {{params.input_dir}}"
        self.name="multiQC_{_sample_}s"
        self.rull_all="\'{output}/{folder_num}_multiQC/multiqc_report.html\'"
        return  self.get_rule_atrr()





class DimultiplexBCL(DemultConst):
    def __init__(self):
        super(DimultiplexBCL, self).__init__()
        # self.input = self.rull_all =self.params =  ""
        # self.threads=20
        self.input="\'{input}\'"
        self.name='RunDemultiplexingBcl'
        self.output = "R{strand_num}=expand(\'{output}/1_{_sample_}s/{sample}/{sample}_R{strand_num}.fastq.gz\',{_sample_}={SAMPLES})"
        self.params = "output_dir =\'{output}/1_{_sample_}s\'"
        self.cmd = r"({exe} --runfolder-dir {input} --processing-threads 8 " \
                   "--loading-threads 4 --writing-threads 4 --mask-short-adapter-reads {num} --output-dir {{params.output_dir}} " \
                   "--use-bases-mask {bases_mask} --sample-sheet {sample_sheet} --barcode-mismatches 0 --no-lane-splitting & wait) && find {{params.output_dir}} -type f -name '*fastq*' | grep -v Undetermined | while read FILE ; do export basename=$(echo \\\"${{{{FILE##*/}}}}\\\"); " \
                                                                    "export basepath=$(echo \\\"${{{{FILE%/*}}}}\\\"); export s=$(echo \\\"${{{{basepath##*/}}}}\\\"); export newfile=\\\"$( echo ${{{{basename}}}} | sed -e 's/\('\\\"${{{{s}}}}\\\"'_\).*\(R[1-2]\).*\.fastq*/\\\\1\\\\2.fastq/')\\\" ; " \
                                                                    "mv \\\"${{{{FILE}}}}\\\" \\\"${{{{basepath}}}}\\\"/\\\"${{{{newfile}}}}\\\"; done;"

        self.logs = '\'{command_dir}/logs/1_bcl2fastq.txt\''

    def get_deleted_bcl(self):
        self.input="temp("+ self.input+")"
        return self.get_rule_atrr()

    def get_unzip_bcl_out(self):
        self.cmd = self.cmd + r" find {{params.output_dir}} -type f -name \'*.fastq.gz\' | while read filename; do gunzip \"${{{{filename}}}}\"; done;"
        self.output = "R{strand_num}=expand(\'{output}/1_{_sample_}s/{sample}/{sample}_R{strand_num}.fastq\',{_sample_}={SAMPLES})"
        return self.get_rule_atrr()

class DimultiplexFASTQ(DemultConst):
    def __init__(self):
        super(DimultiplexFASTQ, self).__init__()
        self.cmd = 'mkdir -p {output} && cd {output} && find {input} -type f \( -name \*R1*.fastq.gz -o -name \*R1*.fastq \) -print0 | sort -z | xargs -0 -I file cat file > merged_R1.fastq.gz; \
                   find {input} -type f \( -name \*R2*.fastq.gz -o -name \*R2*.fastq \) -print0 | sort -z | xargs -0 -I file cat file > merged_R2.fastq.gz && \n\
                   if [ -s merged_R2.fastq.gz ];\n\
                       then\n\
                           {exe} -B {sample_sheet} merged_R2.fastq.gz merged_R1.fastq.gz -x -o %_R2.fastq -o %_R1.fastq -m 0\n\
                       else\n\
                           {exe} -B {sample_sheet} merged_R1.fastq.gz -x -o %_R1.fastq -m 0\n\
                   fi &&\n\
                   for sample in ''{{SAMPLES}}'';\n\
                     do\n\
                       gzip "$sample"_*.fastq && mv -t {output}/{folder_num}_samples/$sample "$sample"_*.fastq.gz;\n\
                     done;'
        self.output = "R{strand_num}=expand(\'{output}/{folder_num}_samples/{sample}/{sample}_R{strand_num}.fastq.gz\',{_sample_}={SAMPLES})"
        self.logs = '\'{command_dir}/logs/{folder_num}_dimultiplex_from_fastq.txt\''
        self.name="DimultiplexFASTQ"


    def get_scrb_pool_atrr(self):
        self.cmd=  \
              'cd  {{params.fastq_output}};\n \
               for pool in {{POOLS}}; do\n \
                    if [ -s {{params.bcl_output}}/"$pool"/"$pool"_R2.fastq.gz ];\n \
                    then\n \
                        {exe} -B {sample_sheet}_"$pool" {{params.bcl_output}}/"$pool"/"$pool"_R1.fastq.gz {{params.bcl_output}}/"$pool"/"$pool"_R2.fastq.gz -x -o %_R2.fastq -o %_R1.fastq -m 0\n\
                    else  \n \
                        {exe} -B {sample_sheet}_"$pool" {{params.bcl_output}}/"$pool"/"$pool"_R1.fastq.gz  -x -o %_R1.fastq -m 0\n \
                    fi\n\
               done &&\n\
               for sample in ''{{SAMPLES}}'';\n\
                 do\n\
                    mv -t {output}/{folder_num}_samples/$sample "$sample"_*.fastq;\n\
                 done;'
        self.input="R{strand_num}=rules.RunDemultiplexingBcl.output.R{strand_num}"
        self.output="R{strand_num}=expand(\'{output}/{folder_num}_samples/{sample}/{sample}_R{strand_num}.fastq\',{_sample_}={SAMPLES})"
        self.params="fastq_output =\'{output}/{folder_num}_samples/\',\n        bcl_output=\'{output}/1_pools\'"
        self.logs="\'{command_dir}/logs/{folder_num}_dimultiplex_from_fastq.txt\'"
        return self.get_rule_atrr()

    def get_modify_fastq(self):
        self.input = "R{strand_num}=\'{output}/{folder_num}_samples/{sample}/{sample}_R{strand_num}.fastq\'"
        self.output = "R{strand_num}=\'{output}/{folder_num}_samples/{sample}/{sample}_R{strand_num}.fastq.gz\'"
        self.params = "fastq_output =\'{output}/{folder_num}_samples/\'"
        self.logs = "\'{command_dir}/logs/{folder_num}_modify_fastq_{sample}.txt\'"
        # self.cmd = r"cat {{input.R2}} | sed -E '/^@/s/(.*:)(.*)/\\1/' | sed '/^@/N;s/\\n//' | sed -E '/^@/s/(.*:)(.{{{{{barcode}}}}})(.{{{{{UMI}}}}})(.*)/\\1\\2\\n\\3/' | sed '/^+/N;s/\\n//' | sed -E '/^\+/s/(.{{{{{barcode}}}}})(.{{{{{UMI}}}}})(.*)/+\\n\\2/' | sed '/^@/N;s/1:N:0:/2:N:0:/' >  {{input.R2}}_new && rm {{input.R2}} && export barcode=$(cat {{input.R2}}_new | grep @ | sed -E '/^@/s/(.*)(.{{{{{barcode}}}}})/\\2/' | head -1) && cat {{input.R1}} | sed -E '/^@/s/(.*:)(.*)/\\1/' | sed -e \"/^@/s/2:N:0:/1:N:0:$barcode/\" > {{input.R1}}_new && rm {{input.R1}} && mv {{input.R2}}_new {{input.R2}} && mv {{input.R1}}_new {{input.R1}} && gzip {{input.R1}} && gzip {{input.R2}} && mv -t {{params.fastq_output}}/{{wildcards.sample}} {{input.R1}}.gz {{input.R2}}.gz ;"
        self.cmd = r"cat {{input.R2}} | sed -E '/^@/s/(.*:)(.*)/\\1/' | sed '/^@/N;s/\\n//' | sed -E '/^@/s/(.*:)(.{{{{{barcode}}}}})(.{{{{{UMI}}}}})(.*)/\\1\\2\\n\\3/' | sed '/^+/N;s/\\n//' | sed -E '/^\+/s/(.{{{{{barcode}}}}})(.{{{{{UMI}}}}})(.*)/+\\n\\2/' | sed '/^@/N;s/1:N:0:/2:N:0:/' >  {{input.R2}}_new && rm {{input.R2}} && export barcode=$(cat {{input.R2}}_new | grep @ | sed -E '/^@/s/(.*)(.{{{{{barcode}}}}})/\\2/' | head -1) && cat {{input.R1}} | sed -E '/^@/s/(.*:)(.*)/\\1/' | sed -e \"/^@/s/2:N:0:/1:N:0:$barcode/\" > {{input.R1}}_new && rm {{input.R1}} && mv {{input.R2}}_new {{input.R2}} && mv {{input.R1}}_new {{input.R1}} && gzip {{input.R1}} && gzip {{input.R2}};"
        self.rule_all = "expand(\'{output}/folder_num}_fastq/{sample}/{sample}_R2.fastq\'), {_sample_}={SAMPLES})"
        self.name='ModifyFastq'
        return self.get_rule_atrr()





class ATACseqConst(object):
    COMMANDS = [CUTADAPT, MACS2]
    ADAPTER1_INIT = 'CTGTCTCTTATACACATCTCCGAGCCCACGAGAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1'

    ADAPTER2_INIT = 'CTGTCTCTTATACACATCTGACGCTGCCGACGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2'

    TRT_CONT_CHOICES = (('yes', _('yes')), ('no', _('no')))
    TRT_CONT_INIT = 'no'
    TRT_CONT_HELP = 'Run treatment sample versus control'
    TRT_CONT_LABEL = 'Run with control'


class CHIPseqConst(object):
    COMMANDS = [CUTADAPT, MACS2]
    ADAPTER1_INIT = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1'

    ADAPTER2_INIT = 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2'

    TRT_CONT_CHOICES = (('yes', _('yes')), ('no', _('no')))
    TRT_CONT_INIT = 'no'
    TRT_CONT_HELP = 'Run treatment sample versus control'
    TRT_CONT_LABEL = 'Run with control'

class RiboSeqConst(object):
    COMMANDS = [CUTADAPT, MACS2]
    ADAPTER1_INIT = 'CTGTAGGCACCATCAATAGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1'


    ADAPTER2_INIT = ''
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2'

    # TRT_CONT_CHOICES = (('yes', _('yes')), ('no', _('no')))
    # TRT_CONT_INIT = 'no'
    # TRT_CONT_HELP = 'Run treatment sample versus control'
    # TRT_CONT_LABEL = 'Run with control'

class RNAseqConst(object):
    COMMANDS = [CUTADAPT, Differential_gene_expression_analysis]
    # STRANDED_CHOICES = (('no', _('non stranded')), ('yes', _('stranded')), ('0.3', _('find automaticaly')))
    # STRANDED_HELP = 'The protocol is stranded or not. If find automaticaly is selected, the protocol will be determined according the the counts of the reads on the strands'
    STRANDED_CHOICES = (('no', _('non stranded')), ('yes', _('stranded')))
    STRANDED_HELP = 'The protocol is stranded or not.'
    STRANDED_LABEL = 'Stranded protocol'

    ADAPTER1_INIT = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1 (default: TruSeq kit)'

    ADAPTER2_INIT = 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2 (default: TruSeq kit)'




class TranscriptomeConst(object):

    UMI_LENGTH_INIT=''
    UMI_LENGTH_HELP = 'If your RNA-Seq data contains UMI barcode, please fill in the UMI barcode length otherwise the UMI barcode will be ignored in the analysis'
    UMI_LENGTH_LABLE = 'UMI barcode length'

    DESEQRUN_CHOICES = (('yes', _('Run DESeq2')), ('no', _('Don\'t run DESeq2')))
    DESEQRUN_INIT = 'no'
    DESEQRUN_HELP = 'DESeq2 is software for differential expression. You can run DESeq2 if you know the catergories of your samples (for example: knockout vs. control)'
    DESEQRUN_LABEL = 'DESeq2 run'


    UMI_CHOICES = (('yes', _('Run with UMI correction')), ('no', _('Don\'t run with UMI correction')))
    UMI_INIT = 'no'
    UMI_HELP = 'If your data contain UMI barcode, chose yes in order to correct for UMI'
    UMI_LABEL = 'UMI'

    PE_CHOICES = (('yes', _('paired-end data')), ('no', _('single-end data')))
    PE_INIT = 'no'
    PE_HELP = 'if your data is paired, chose yes'
    PE_LABEL = 'paired end'



class MarsSeqConst(object):
    COMMANDS = [CUTADAPT, Differential_gene_expression_analysis]

class ScrbSeqConst(object):
    COMMANDS = [CUTADAPT, Differential_gene_expression_analysis]

class DESeqFromCountsMatrixConst(object):
    COMMANDS = [Differential_gene_expression_analysis]

class AdvancedParameters(object):
    # CUTADAPT advancd parametres
    SELECT_ADAPTER1_INIT = ''  # the user have to enter the adapter
    SELECT_ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    SELECT_ADAPTER1_LABLE = 'Additional adapter to remove from R1'

    SELECT_ADAPTER2_INIT = ''  # the user have to enter the adapter
    SELECT_ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    SELECT_ADAPTER2_LABLE = 'Additional adapter to remove from R2'

    CUTADAPT_QVALUE_INIT = '20'
    CUTADAPT_QVALUE_INIT_ATAC = '25'
    CUTADAPT_QVALUE_HELP = '5\'CUTOFF,]3\'CUTOFF, --quality-cutoff=[5\'CUTOFF,]3\'CUTOFF Trim low-quality bases from 5\' and/or 3\' ends of each read before adapter removal. Applied to both reads \
if data is paired. If one value is given, only the 3\'end is trimmed. If two comma-separated cutoffs are given, the 5\' end is trimmed with the first cutoff, the 3\'end with the second.'
    CUTADAPT_QVALUE_LABEL = 'Quality-cutoff (-q)'

    CUTADAPT_MINIMUM_LENGTH_INIT = '25'
    CUTADAPT_MINIMUM_LENGTH_INIT_ATAC = '30'
    CUTADAPT_MINIMUM_LENGTH_HELP = '-m LENGTH, --minimum-length=LENGTH Discard reads shorter than LENGTH. Default: 0'
    CUTADAPT_MINIMUM_LENGTH_LABEL = 'Minimum reads length (-m)'

    CUTADAPT_MAXIMUM_LENGTH_INIT = ''
    CUTADAPT_MAXIMUM_LENGTH_INIT_RIBO = '32'
    CUTADAPT_MAXIMUM_LENGTH_HELP = '-M LENGTH, --maximum-length=LENGTH Discard reads longer than LENGTH. Default: 0'
    CUTADAPT_MAXIMUM_LENGTH_LABEL = 'Maximum reads length (-M)'

    CUTADAPT_CUT_READS_START_INIT = ''
    CUTADAPT_CUT_READS_START_MARSEQ_INIT = '1'
    CUTADAPT_CUT_READS_START_HELP = '-u LENGTH, --cut=LENGTH Remove bases from each read (first read only if paired). If LENGTH is positive, remove bases from the beginning. If LENGTH is negative, remove bases from the end. Can be used twice if LENGTHs have different signs. This is applied *before* adapter trimming.'
    CUTADAPT_CUT_READS_START_LABEL = 'Remove bases from read\'s start (-u)'

    CUTADAPT_CUT_READS_END_INIT = ''
    #CUTADAPT_CUT_READS_END_MARSEQ_INIT = '-3'
    CUTADAPT_CUT_READS_END_HELP = '-u LENGTH, --cut=LENGTH Remove bases from each read (first read only if paired). If LENGTH is positive, remove bases from the beginning. If LENGTH is negative, remove bases from the end. Can be used twice if LENGTHs have different signs. This is applied *before* adapter trimming.'
    CUTADAPT_CUT_READS_END_LABEL = 'Remove bases from read\'s end (-u)'

    # DEseq adavced parameters

    FDR_CORRECTION_CHOICES = (('yes', _('Correct with FDR tool')), ('no', _('Don\'t correct with FDR tool')))
    FDR_CORRECTION_HELP = 'Do you want to correct the data with FDR tool.'
    FDR_CORRECTION_LABEL = 'FDR tool correction'
    FDR_CORRECTION_INIT= 'no'

    LOG2FC_INIT = '1'
    LOG2FC_HELP = 'Set the threshold for the log2 Fold Change value'
    LOG2FC_LABEL = 'Log2 Fold Change threshold'

    P_ADJ_INIT = '0.05'
    P_ADJ_HELP = 'Set the threshold for the adjusted P value'
    P_ADJ_LABEL = 'Adjusted P-value'

    BASEMEAN_INIT = '5'
    BASEMEAN_HELP = 'Set the threshold for the base mean'
    BASEMEAN_LABEL = 'Base mean'

    # MACS2 adavnced parameters
    MACS_QVALUE_INIT = '0.05'
    MACS_QVALUE_HELP = 'The qvalue (minimum FDR) cutoff to call significant regions. Default is 0.05. For broad marks, you can try 0.05 as cutoff. Q-values are calculated from p-values using Benjamini-Hochberg procedure.'
    MACS_QVALUE_LABEL = 'Q-value'

    MACS_BW_CHIP_INIT = '300'
    MACS_BW_ATAC_INIT = '120'
    MACS_BW_HELP = 'The bandwidth which is used to scan the genome ONLY for model building. Can be set to the expected fragment size.'
    MACS_BW_LABEL = 'Band-width'


class ReturnAllClassFields(object):
    def __init__(self):
        self.char_fields = []
        self.choise_fields = []
        self.float_fields = []
        self.integer_fields = []

    def get_char_fields(self):
        for name, form_field, required, initial, help_text, lable, widget in self.char_fields:
            yield name, form_field, required, initial, help_text, lable, widget

    def get_choise_fields(self):
        for name, form_field, required, initial, Choise, help_text, lable, widget in self.choise_fields:
            yield name, form_field, required, initial, Choise, help_text, lable, widget

    def get_float_fields(self):
        for name, form_field, required, initial, help_text, lable, widget in self.float_fields:
            yield name, form_field, required, initial, help_text, lable, widget

    def get_integer_fields(self):
        for name, form_field, required, initial, help_text, lable, widget in self.integer_fields:
            yield name, form_field, required, initial, help_text, lable, widget





class RNAseqFields(ReturnAllClassFields):
    def __init__(self):
        super(RNAseqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, True, RNAseqConst.ADAPTER1_INIT, RNAseqConst.ADAPTER1_HELP,
             RNAseqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, RNAseqConst.ADAPTER2_INIT, RNAseqConst.ADAPTER2_HELP,
             RNAseqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        self.choise_fields.extend([
            ['stranded', forms.ChoiceField, True, '', RNAseqConst.STRANDED_CHOICES, RNAseqConst.STRANDED_HELP,
             RNAseqConst.STRANDED_LABEL, forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})],
            ['correct_with_FDR_tool', forms.ChoiceField, True, AdvancedParameters.FDR_CORRECTION_INIT, AdvancedParameters.FDR_CORRECTION_CHOICES,
             AdvancedParameters.FDR_CORRECTION_HELP, AdvancedParameters.FDR_CORRECTION_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'command': Differential_gene_expression_analysis,
                                 'form_id': ADVANCED_ID})],
            ['paired_end', forms.ChoiceField, True, TranscriptomeConst.PE_INIT,
             TranscriptomeConst.PE_CHOICES,
             TranscriptomeConst.PE_HELP, TranscriptomeConst.PE_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})],
            ['UMI', forms.ChoiceField, True, TranscriptomeConst.UMI_INIT,
             TranscriptomeConst.UMI_CHOICES,
             TranscriptomeConst.UMI_HELP, TranscriptomeConst.UMI_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})],
            ['deseq_run', forms.ChoiceField, True, TranscriptomeConst.DESEQRUN_INIT, TranscriptomeConst.DESEQRUN_CHOICES,
             TranscriptomeConst.DESEQRUN_HELP, TranscriptomeConst.DESEQRUN_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])


        self.float_fields.extend([
            ['log2_Fold_Change', forms.FloatField, False, AdvancedParameters.LOG2FC_INIT,
             AdvancedParameters.LOG2FC_HELP, AdvancedParameters.LOG2FC_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['adjusted_P_value', forms.FloatField, False, AdvancedParameters.P_ADJ_INIT,
             AdvancedParameters.P_ADJ_HELP, AdvancedParameters.P_ADJ_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['baseMean', forms.FloatField, False, AdvancedParameters.BASEMEAN_INIT,
             AdvancedParameters.BASEMEAN_HELP, AdvancedParameters.BASEMEAN_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
            # ['UMI_barcode_length', forms.IntegerField, False,
            #  TranscriptomeConst.UMI_LENGTH_INIT,
            #  TranscriptomeConst.UMI_LENGTH_HELP,
            #  TranscriptomeConst.UMI_LENGTH_LABLE,
            #  forms.NumberInput(
            #      attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])


class MarsSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(MarsSeqFields, self).__init__()
        self.char_fields.extend([
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE,
             forms.TextInput(
                 attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT,
                        'size': 80})]
        ])

        self.choise_fields.extend([
            ['correct_with_FDR_tool', forms.ChoiceField, True, AdvancedParameters.FDR_CORRECTION_INIT, AdvancedParameters.FDR_CORRECTION_CHOICES,
             AdvancedParameters.FDR_CORRECTION_HELP, AdvancedParameters.FDR_CORRECTION_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'command': Differential_gene_expression_analysis,
                                 'form_id': ADVANCED_ID})],
            ['deseq_run', forms.ChoiceField, True, TranscriptomeConst.DESEQRUN_INIT, TranscriptomeConst.DESEQRUN_CHOICES,
             TranscriptomeConst.DESEQRUN_HELP, TranscriptomeConst.DESEQRUN_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['log2_Fold_Change', forms.FloatField, False, AdvancedParameters.LOG2FC_INIT,
             AdvancedParameters.LOG2FC_HELP, AdvancedParameters.LOG2FC_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['adjusted_P_value', forms.FloatField, False, AdvancedParameters.P_ADJ_INIT,
             AdvancedParameters.P_ADJ_HELP, AdvancedParameters.P_ADJ_LABEL, forms.NumberInput(
                attrs={'class': 'btn btn-default', 'step': '0.01',
                       'command': Differential_gene_expression_analysis, 'form_id': ADVANCED_ID})],
            ['baseMean', forms.FloatField, False, AdvancedParameters.BASEMEAN_INIT,
             AdvancedParameters.BASEMEAN_HELP, AdvancedParameters.BASEMEAN_LABEL, forms.NumberInput(
                attrs={'class': 'btn btn-default', 'step': '0.01',
                       'command': Differential_gene_expression_analysis, 'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_MARSEQ_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])

class ScrbSeqFields(MarsSeqFields):
    def __init__(self):
        super(ScrbSeqFields, self).__init__()
        self.integer_fields.extend([
            ['UMI_barcode_length', forms.IntegerField, False,
             TranscriptomeConst.UMI_LENGTH_INIT,
             TranscriptomeConst.UMI_LENGTH_HELP,
             TranscriptomeConst.UMI_LENGTH_LABLE,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]])



class ChipSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(ChipSeqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, False, CHIPseqConst.ADAPTER1_INIT, CHIPseqConst.ADAPTER1_HELP,
             CHIPseqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, CHIPseqConst.ADAPTER2_INIT, CHIPseqConst.ADAPTER2_HELP,
             CHIPseqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        self.choise_fields.extend([
            ['treat_vs_control', forms.ChoiceField, True, CHIPseqConst.TRT_CONT_INIT, CHIPseqConst.TRT_CONT_CHOICES,
             CHIPseqConst.TRT_CONT_HELP, CHIPseqConst.TRT_CONT_LABEL, forms.Select(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['macs_qvalue', forms.FloatField, False, AdvancedParameters.MACS_QVALUE_INIT,
             AdvancedParameters.MACS_QVALUE_HELP, AdvancedParameters.MACS_QVALUE_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'step': '0.01', 'command': MACS2,
                        'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['macs_bandwidth', forms.IntegerField, False, AdvancedParameters.MACS_BW_CHIP_INIT,
             AdvancedParameters.MACS_BW_HELP, AdvancedParameters.MACS_BW_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': MACS2,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])


class AtacSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(AtacSeqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, True, ATACseqConst.ADAPTER1_INIT, ATACseqConst.ADAPTER1_HELP,
             ATACseqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, ATACseqConst.ADAPTER2_INIT, ATACseqConst.ADAPTER2_HELP,
             ATACseqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        self.choise_fields.extend([
            ['treat_vs_control', forms.ChoiceField, True, ATACseqConst.TRT_CONT_INIT, ATACseqConst.TRT_CONT_CHOICES,
             ATACseqConst.TRT_CONT_HELP, ATACseqConst.TRT_CONT_LABEL, forms.Select(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['macs_qvalue', forms.FloatField, False, AdvancedParameters.MACS_QVALUE_INIT,
             AdvancedParameters.MACS_QVALUE_HELP, AdvancedParameters.MACS_QVALUE_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'step': '0.01', 'command': MACS2,
                        'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['macs_bandwidth', forms.IntegerField, False, AdvancedParameters.MACS_BW_ATAC_INIT,
             AdvancedParameters.MACS_BW_HELP, AdvancedParameters.MACS_BW_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': MACS2,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_qvalue', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_QVALUE_INIT_ATAC, AdvancedParameters.CUTADAPT_QVALUE_HELP,
             AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT_ATAC,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])



class RiboSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(RiboSeqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, False, RiboSeqConst.ADAPTER1_INIT, RiboSeqConst.ADAPTER1_HELP,
             RiboSeqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, RiboSeqConst.ADAPTER2_INIT, RiboSeqConst.ADAPTER2_HELP,
             RiboSeqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        # self.choise_fields.extend([
        #     ['treat_vs_control', forms.ChoiceField, True, RiboSeqConst.TRT_CONT_INIT, RiboSeqConst.TRT_CONT_CHOICES,
        #      RiboSeqConst.TRT_CONT_HELP, RiboSeqConst.TRT_CONT_LABEL, forms.Select(
        #         attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        # ])

        self.float_fields.extend([
            ['macs_qvalue', forms.FloatField, False, AdvancedParameters.MACS_QVALUE_INIT,
             AdvancedParameters.MACS_QVALUE_HELP, AdvancedParameters.MACS_QVALUE_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'step': '0.01', 'command': MACS2,
                        'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['macs_bandwidth', forms.IntegerField, False, AdvancedParameters.MACS_BW_CHIP_INIT,
             AdvancedParameters.MACS_BW_HELP, AdvancedParameters.MACS_BW_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': MACS2,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_max_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MAXIMUM_LENGTH_INIT_RIBO,
             AdvancedParameters.CUTADAPT_MAXIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MAXIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])









class DESeqFromCountsMatrixFields(ReturnAllClassFields):
    def __init__(self):
        super(DESeqFromCountsMatrixFields, self).__init__()

        self.choise_fields.extend([
            ['correct_with_FDR_tool', forms.ChoiceField, True, AdvancedParameters.FDR_CORRECTION_INIT,
             AdvancedParameters.FDR_CORRECTION_CHOICES,
             AdvancedParameters.FDR_CORRECTION_HELP, AdvancedParameters.FDR_CORRECTION_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'command': Differential_gene_expression_analysis,
                                 'form_id': ADVANCED_ID})],
            ['deseq_run', forms.ChoiceField, True, TranscriptomeConst.DESEQRUN_INIT, TranscriptomeConst.DESEQRUN_CHOICES,
             TranscriptomeConst.DESEQRUN_HELP, TranscriptomeConst.DESEQRUN_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['log2_Fold_Change', forms.FloatField, False, AdvancedParameters.LOG2FC_INIT,
             AdvancedParameters.LOG2FC_HELP, AdvancedParameters.LOG2FC_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['adjusted_P_value', forms.FloatField, False, AdvancedParameters.P_ADJ_INIT,
             AdvancedParameters.P_ADJ_HELP, AdvancedParameters.P_ADJ_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['baseMean', forms.FloatField, False, AdvancedParameters.BASEMEAN_INIT,
             AdvancedParameters.BASEMEAN_HELP, AdvancedParameters.BASEMEAN_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})]
        ])


        # CATEGORY1_INIT = ''
        # CATEGORY1_HELP = 'fill the categories of the samples (for example: knockout vs. control). Use + button if there are more than 2 categories. Please use with a-z,A-Z,0-9 characters only (other characters will be converted to "_" character).'
        # CATEGORY1_LABLE = 'Categories'
        #
        # CATEGORY2_INIT = ''
        # CATEGORY2_LABEL = ''

        # class ReseqConst(object):
        #     ReseqAPPS =  [ CONSENSUS_APP,CORE_APP,ALIGNMENT_APP,REPORTS_APP ]
        #
        #     NO_SPLIT_SUBREADS_INIT = "False"
        #     NO_SPLIT_SUBREADS_CHOICES = ((False, _("No")), (True, _("Yes")))
        #     NO_SPLIT_SUBREADS_HELP = "Do not split reads into subreads even if subread "
        #     NO_SPLIT_SUBREADS_LABEL = "Align unsplit polymerase reads"
        #     ALGORITHM_CHOICES = (('best',_('best')), ('plurality', _('plurality')), ('arrow', _('arrow')))
        #     MIN_ACCURACY_INIT = 70.0
        #     MIN_ACCURACY_MIN = 0.0
        #
        #

        #####################  Import ######################

        # class BarcodingConst(object):
        #     Barcoding_APPS = [CORE_APP]
        #     BARCODES_TYPES = (('symmetric', _('symmetric')), ('asymmetric', _('asymmetric')))
        #
        #     SCORE_MODE_INIT = 'symmetric'
        #     SCORE_MODE_CHOICES = BARCODES_TYPES
        #     SCORE_MODE_HELP = 'The type of barcode sequence to use. asymmetric: Barcode sequences that are different on either end of an insert present in a SMRTbell template.'
        #     SCORE_MODE_LABEL = 'Option Score Mode'
        #
        #
