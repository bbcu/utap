import os

PACKAGE_ROOT = os.path.dirname(os.path.dirname(__file__))  # utap/analysis
PROJECT_ROOT = os.path.abspath(os.path.join(PACKAGE_ROOT, os.pardir))  # utap
PROJECT_ENV = os.path.abspath(os.path.join(PROJECT_ROOT, os.pardir))  # ../utap
PROJECT_NAME = os.path.basename(PACKAGE_ROOT)

PIPELINE_OUTPUT_FOLDER = 'utap-output'

########################################################################################################################
# Don't change it, it is template for sed command (include the ' charcters)
########################################################################################################################
HOST_MOUNT='/home/labs/bioservices/services/UTAP-data-staging'  #Must be real value, else the code couldn't compiled
PIPELINE_SERVER='bio.wexac.weizmann.ac.il'
PROXY_URL='None'
USER_CLUSTER = 'bioinfo'
NOTIFICATION_INTERNAL_MAILING_LIST = ['utap@weizmann.ac.il']
INSTITUTE_NAME = 'Weizmann' 
QUEUE_USERS='bio-pipe'
CLUSTER_RESOURCES_PARAMS='-n {threads} -R "rusage[mem={resources.mem_mb_per_thread}]" -R "span[hosts=1]"'
SQLITE_DB='/data/refael/staging/db.sqlite3'
SMTP_SERVER_NAME='mg.weizmann.ac.il' 
PIPELINE_SERVER_PORT=8000
BIOUTILS_PACKAGE='/home/labs/bioservices/services/miniconda2/envs/utap-snakemake-testenv'
SNAKEFILE_PACKAGE='/home/labs/bioservices/services/miniconda2/envs/utap-snakemake-testenv'
CONDA_ROOT='/home/labs/bioservices/services/miniconda2'
RSCRIPT='/home/labs/bioservices/services/miniconda2/envs/utap/bin/Rscript'
R_LIB_PATHS='/home/labs/bioservices/services/miniconda2/envs/utap/lib/R/library' 
MAX_UPLOAD_SIZE=3145728000  # In Mb. for example : 2* 1024 *1024 = 2Gb
CLUSTER_TYPE='lsf'  #lsf, pbs, local

SNAKEMAKE_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-py35/bin/snakemake'
GS_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/gs'
CLUSTER_EXE='bsub' 
CUTADAPT_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/cutadapt' 
FASTQC_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/fastqc' 
STAR_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/STAR' 
SAMTOOLS_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/samtools'
TOPHAT_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-staging/bin/tophat'
NGS_PLOT_EXE='' 
HTSEQ_COUNT_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/htseq-count' 
BCL2FASTQ_EXE='/home/labs/bioservices/services/miniconda2/envs/utap/bin/bcl2fastq'
#CELLRANGER_EXE='/home/labs/bioservices/services/cellranger-4.0.0/cellranger'
CELLRANGER_EXE='/home/labs/bioservices/services/cellranger-5.0.0/cellranger'
#For chromatin pipelines (ATAC-Seq and CHIP-seq) - for now not installed inside the docker:
JAVA_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/bin/java'
MULTIQC_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/bin/multiqc'
BOWTIE2_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/bin/bowtie2'
BOWTIE1_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-staging/bin/bowtie'
PICARD_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/share/picard-2.18.22-0/picard.jar'
IGVTOOLS_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/bin/igvtools'
MACS2_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/bin/macs2'
BEDTOOLS_EXE='/home/labs/bioservices/services/miniconda2/envs/utap-chromatin/bin/bedtools'
MAX_CORES='100' # Maximum cores on the local host or on the nodes of the cluster
#==ATAC-Seq pipeline
RUN_LOCAL=False #Run inside the docker
RUN_LOCAL_HOST_CONDA=False #Run commands without cluster, but by ssh on the host.
DEMO_SITE=False
#==CHIP-seq pipeline # run insdie the docker
NGS_PLOT_EXE='module load R/3.4.2;module load ngsplot/2.61;ngs.plot.r'
KENTUTILS_EXE='module load kentUtils;'
HOMER_EXE='module load homer;'
PEAKSPLITTER_EXE='module load PeakSplitter;'
#==Ribo-seq pipeline
PYTHON_EXE = 'module load python/bio-2.7.13;'
JRE_EXE= 'module load jre;'
MACS_EXE= '/home/labs/bioservices/services/miniconda2/envs/utap-staging/bin/macs'
IGVTOOLS_EXE_FOR_RIBOSEQ='module load IGVTools/2.3.26;'
########################################################################################################################

if PROXY_URL != 'None':
    PIPELINE_URL = PROXY_URL
else:
    PIPELINE_URL = PIPELINE_SERVER + ":" + str(PIPELINE_SERVER_PORT)

PIPELINE_URL_HTTP = PIPELINE_URL if PIPELINE_URL.startswith('http') else 'http://'+PIPELINE_URL
RESULTS_URL = PIPELINE_URL_HTTP + '/reports'



################################################
# RSCRIPT = '/apps/RH7U2/gnu/R/3.4.2/bin/Rscript'
# R_LIB_PATHS = '/apps/RH7U2/scripts/bbcu-R-packages'
# SNAKEFILE_PACKAGE = '/home/labs/bioservices/services/test_env/python27-ve-ngs-snakemakeTest'
# BIOUTILS_PACKAGE = '/apps/RH7U2/scripts/bbcu-python-packages/ve-2.7-bioutils/'
# HOST_MOUNT = '/home/labs/bioservices/services/test_env/'
# PIPELINE_SERVER = 'ngspipe.wexac.weizmann.ac.il'
# NOTIFICATION_INTERNAL_MAILING_LIST = ['refael.kohen@weizmann.ac.il']
# INSTITUTE_NAME = 'BBCU'
# QUEUE_USERS = 'bio'
# SQLITE_DB = os.path.join(HOST_MOUNT, 'db.sqlite3')
# PIPELINE_SERVER_PORT = 8000
# SMTP_SERVER_NAME = 'localhost'
# RESULTS_URL = 'http://' + PIPELINE_SERVER + '/\runs'
##############################################

LAB_DIR = os.path.join(HOST_MOUNT, PIPELINE_OUTPUT_FOLDER) # Output of the pipeline

SMTP_SERVER_PORT = 25

DEFAULT_LOG_DIR = os.path.join(HOST_MOUNT, 'logs-utap')
PARAMETERS_DIR = os.path.join(HOST_MOUNT, 'parameters_files')
JOBS_STATUS_FILE = os.path.join(HOST_MOUNT, 'jobs_status.txt')
RESULTS_DIR = os.path.join(HOST_MOUNT, 'reports') # Links to reports - DocumentsDir of apache




DEMULTIPLEXING_SCRIPT = os.path.join(BIOUTILS_PACKAGE, 'bin/demultiplexing-fastq.py')
FASTQ_MULTX = os.path.join('/home/labs/bioservices/services/miniconda2/envs/utap-staging/bin/fastq-multx')
SNAKEFILE_SCRIPTS = os.path.join(SNAKEFILE_PACKAGE, 'bin')
SNAKEFILE_TAMPLATES = os.path.join(SNAKEFILE_PACKAGE, 'lib/python2.7/site-packages/ngs-snakemake')
SNAKEFILE_MARSSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-marseq.txt')
SNAKEFILE_RNASEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-rnaseq.txt')
SNAKEFILE_SINGLECELL = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-singlecell.txt')
SNAKEFILE_ATACSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-atacseq.txt')
SNAKEFILE_CHIPSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-chipseq.txt')
SNAKEFILE_RIBOSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-riboseq.txt')
SNAKEFILE_DESEQ_FROM_COUNTS_MATRIX = os.path.join(SNAKEFILE_TAMPLATES,'snakefiles', 'snakefile-deseq-from-counts-matrix.txt')
SNAKEFILE_PYTHON = os.path.join(SNAKEFILE_SCRIPTS, 'python')
SNAKEFILE_VERSION = os.path.join(SNAKEFILE_TAMPLATES, 'VERSION.txt')
with open(os.path.join(PROJECT_ROOT, "VERSION.txt"),"r") as f: UTAP_VERSION = f.read()


DEMULTIPLEXING_MEMORY = '3000'  # 3G per thread in snakemake in the parameter rusage[mem=3000] - don't include bare genome

# Snakemake repeats on run of jobs if it is failed (for example suspend by the cluster)
JOB_REPEATS_NUMBER = 3

#Mininum GB on disk for running the analysis. Under this value, the user will get warning.
MIN_DISK_FREE = 300 #300GB

