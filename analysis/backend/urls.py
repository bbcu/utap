"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.conf.urls import url

from analysis.backend.views import views_general as general_views
from analysis.backend.views.views_analysis_atacseq import AnalysisAtacSeqView
from analysis.backend.views.views_analysis_chipseq import AnalysisChipSeqView
from analysis.backend.views.views_analysis_riboseq import AnalysisRiboSeqView
from analysis.backend.views.views_analysis_deseq_to_counts_matrix import AnalysisDESeqFromCountsMatrixView
from analysis.backend.views.views_analysis_demultiplexing import AnalysisDemultiplexingView
from analysis.backend.views.views_analysis_singlecell import AnalysisSinglecellView
from analysis.backend.views.views_analysis_submit import AnalysisSubmitView
from analysis.backend.views.views_analysis_transcriptome import AnalysisTranscriptomeView, AnalysisDeseqView

app_name = 'analysis'
from analysis.backend.views.views_general import AnalysisChoice, LoginAsUser, samples_list_ajax, AnalysisDetailsView, \
    remote_samples_list_ajax, UserDatasets, no_results, run_failed, demultiplexing_results, temp, test, \
    stest, folder_exists_ajax, has_space_on_disc_ajax, signup, activate_user, \
    UploadFolder, upload_progress, upload_help, DeleteAnalysisView, analysis_progress_ajax, Elearning, atacseq_results, chipseq_results, check_samplesheet_ajax

urlpatterns = [
    url(r'^signup/$', signup, name='signup'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', activate_user,
        name='activate'),
    url(r'^login_as/$', LoginAsUser.as_view(), name='login_as'),
    url(r'^user_logout/$', general_views.logout_view, name='user_logout'),
    url(r'^ajax/samples_list/$', samples_list_ajax, name='samples_list'),
    url(r'^ajax/folder_exists/$', folder_exists_ajax, name='folder_exists'),
    url(r'^ajax/free_space/$', has_space_on_disc_ajax, name='free_space'),
    url(r'^ajax/remote_samples_list/$', remote_samples_list_ajax, name='remote_samples_list'),
    url(r'^ajax/check_samplesheet/$', check_samplesheet_ajax, name='check_samplesheet'),
    url(r'^run_deseq/(?P<pipeline>.*)/$', AnalysisDeseqView.as_view(), name='run_deseq'),
    url(r'^ajax/delete_analysis/$', DeleteAnalysisView.as_view(), name='delete_analysis'),
    url(r'^analysis_details/(?P<pk>[0-9]+)/$', AnalysisDetailsView.as_view(), name='analysis_details'),
    url(r'^e_learning/$', Elearning.as_view(), name='e_learning'),
    url(r'^user_datasets/$', UserDatasets.as_view(), name='user_datasets'),
    url(r'^chanalysis/$', AnalysisChoice.as_view(), name='choice_analysis'),
    url(r'^analysis/(?P<pipeline>Transcriptome.*)/$', AnalysisTranscriptomeView.as_view(), name='analysis'),
    url(r'^analysis/(?P<pipeline>Demultiplexing.*)/$', AnalysisDemultiplexingView.as_view(), name='analysis'),
    url(r'^analysis/(?P<pipeline>Single-cell RNA Cell Ranger.*)/$', AnalysisSinglecellView.as_view(), name='analysis'),
    url(r'^analysis/(?P<pipeline>ATAC-Seq.*)/$', AnalysisAtacSeqView.as_view(), name='analysis'),
    url(r'^analysis/(?P<pipeline>ChIP-Seq.*)/$', AnalysisChipSeqView.as_view(), name='analysis'),
    url(r'^analysis/(?P<pipeline>Ribo-Seq.*)/$', AnalysisRiboSeqView.as_view(), name='analysis'),
    url(r'^analysis/(?P<pipeline>DESeq2.*)/$', AnalysisDESeqFromCountsMatrixView.as_view(), name='analysis'),
    url(r'^submit_analysis/(?P<pipeline>.*)/$', AnalysisSubmitView.as_view(), name='submit_analysis'),
    url(r'^temp/$', temp, name='temp'),
    url(r'^test/$', test, name='test'),
    url(r'^stest/$', stest, name='stest'),
    url(r'^upload_help/$', upload_help, name='upload_help'),
    url(r'^no_results/$', no_results, name='no_resutls'),
    url(r'^run_failed/$', run_failed, name=''),
    url(r'^demultiplexing_results/$', demultiplexing_results, name='demultiplexing_results'),
    url(r'^atacseq_results/$', atacseq_results, name='atacseq_results'),
    url(r'^chipseq_results/$', chipseq_results, name='chipseq_results'),
    #url(r'^$', UserDatasets.as_view(), name='user_datasets'),
    url(r'^$', Elearning.as_view(), name='e_learning'),
    url(r'^upfolder/$', UploadFolder.as_view(), name='upfolder'),
    url(r'^upload_progress/$', upload_progress, name="upload-progress"),
    url(r'^ajax/analysis_progress/$', analysis_progress_ajax.as_view(), name="analysis_progress"),
]
