"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import logging
import os

from analysis.backend.settings import PROJECT_NAME, DEFAULT_LOG_DIR

FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

root_logger = logging.getLogger(PROJECT_NAME)
root_logger.setLevel(logging.DEBUG)


def add_run_log_handlers(logs_dir=DEFAULT_LOG_DIR):
    # create handler for outputting to console
    stderr_handler = logging.StreamHandler()
    stderr_handler.setLevel(logging.DEBUG)
    stderr_handler.setFormatter(FORMATTER)
    root_logger.addHandler(stderr_handler)

    # create application log file
    now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    log_filename = os.path.join(logs_dir, '%s.%s.log' % (PROJECT_NAME, now))
    file_handler = logging.FileHandler(log_filename)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(FORMATTER)
    root_logger.addHandler(file_handler)

    # create error log file
    error_log_filename = os.path.join(logs_dir, 'error.%s.log' % (now,))
    error_file_handler = logging.FileHandler(error_log_filename)
    error_file_handler.setLevel(logging.WARNING)
    error_file_handler.setFormatter(FORMATTER)
    root_logger.addHandler(error_file_handler)
