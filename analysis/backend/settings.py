# change name of this file to settings.py

from settings_base import *

BBCU_INTERNAL = True

COLLABORATIONS_DIR = '/home/labs'

QUEUE_STAFF = 'bio'
# You need copy the content of the file ~/.ssh/id_rsa.pub (of user bioinfo in bio server) to ~/.ssh/authorized_keys of user pmrefael in stefan server
NEXTSEQ_SERVER = 'stefan2.weizmann.ac.il'
NEXTSEQ_USER = 'stefan'
FASTQ_REMOTE_PATH = '/data/fastq'
FASTQ_REMOTE_PATH_INCPM = '/data1/fastq'
BCL_REMOTE_PATH = '/data/nextseq'
BCL_REMOTE_PATH_INCPM = '/data1/nextseq'
BCL_REMOTE_PATH_INCPM_NOVA = '/data1/novaseq'

DOCKER_USERS = ''  # Not relevant to internal usage

