"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import abc
import logging

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from analysis.backend.forms.forms_riboseq import RiboSeqForm
from analysis.backend.run.run_ngs_riboseq import RunRiboSeq
from analysis.backend.utils.consts import FUTURE_DATA_TYPE, RIBOSEQ_ANALYSIS_TYPE
from analysis.backend.views.views_analysis_base import AnalysisBaseView
from analysis.backend.views.views_general import LoginAsUser

logger = logging.getLogger(__name__)


# csrf_exempt cancel the need for csrf in the login. For enabling to login with python code
@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisRiboSeqView(AnalysisBaseView):
    @abc.abstractmethod
    def get_form_class(self, pipeline):
        if pipeline == RIBOSEQ_ANALYSIS_TYPE:
            return RiboSeqForm
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        if pipeline == RIBOSEQ_ANALYSIS_TYPE:
            return RunRiboSeq
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        self.check_validation(analysis_form, request, 'input_folder')

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        # if output_dir is empty, the output folder created in run function with name and date.
        self.output_dir_ = None

    def post(self, request, pipeline, *args, **kwargs):
        if 'user' in request.GET.keys():  # submit user post analyses with user argument
            username = request.GET['user']
            LoginAsUser().login_as_user(username, request)
        return super(AnalysisRiboSeqView, self).post(request, pipeline, *args, **kwargs)
