"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import abc
import json
import logging
import re

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from analysis.backend.forms.forms_transcriptome import RnaSeqForm, MarsSeqForm, RnaSeqDeseqForm, MarsSeqDeseqForm, ScrbSeqForm, ScrbSeqDeseqForm
from analysis.backend.run.run_ngs_transcriptome_rnaseq import RunRnaSeqDeseq, RunRnaSeq
from analysis.backend.run.run_ngs_transcriptome_marsseq import RunMarsSeqDeseq, RunMarsSeq
from analysis.backend.run.run_ngs_transcriptome_scrbseq import RunScrbSeqDeseq, RunScrbSeq
from analysis.backend.utils.consts import RNASEQ_ANALYSIS_TYPE, MARSSEQ_ANALYSIS_TYPE, SCRBSEQ_ANALYSIS_TYPE, FUTURE_DATA_TYPE, \
    RNASEQ_DESEQ_ANALYSIS_TYPE, MARSSEQ_DESEQ_ANALYSIS_TYPE, SCRBSEQ_DESEQ_ANALYSIS_TYPE, SINGLECELL_CELRANGER_ANALYSIS_TYPE
from analysis.backend.views.views_analysis_base import AnalysisBaseView
from analysis.backend.views.views_general import LoginAsUser

logger = logging.getLogger(__name__)


# csrf_exempt cancel the need for csrf in the login. For enabling to login with python code
@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisTranscriptomeView(AnalysisBaseView):
    @abc.abstractmethod
    def get_form_class(self, pipeline):
        if pipeline == RNASEQ_ANALYSIS_TYPE:
            return RnaSeqForm
        if pipeline == MARSSEQ_ANALYSIS_TYPE:
            return MarsSeqForm
        if pipeline == SCRBSEQ_ANALYSIS_TYPE:
            return ScrbSeqForm
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        if pipeline == RNASEQ_ANALYSIS_TYPE:
            return RunRnaSeq
        if pipeline == MARSSEQ_ANALYSIS_TYPE:
            return RunMarsSeq
        if pipeline == SCRBSEQ_ANALYSIS_TYPE:
            return RunScrbSeq
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        self.check_validation(analysis_form, request, 'input_folder')

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        # if output_dir is empty, the output folder created in run function with name and date.
        self.output_dir_ = None

    def post(self, request, pipeline, *args, **kwargs):
        if 'user' in request.GET.keys():  # submit user post analyses with user argument
            username = request.GET['user']
            LoginAsUser().login_as_user(username, request)
        return super(AnalysisTranscriptomeView, self).post(request, pipeline, *args, **kwargs)


# csrf_exempt cancel the need for csrf in the login. For enabling to login with python code
@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisDeseqView(AnalysisBaseView):
    @abc.abstractmethod
    def get_form_class(self, pipeline):
        if pipeline == RNASEQ_DESEQ_ANALYSIS_TYPE:
            return RnaSeqDeseqForm
        if pipeline == MARSSEQ_DESEQ_ANALYSIS_TYPE:
            return MarsSeqDeseqForm
        if pipeline == SCRBSEQ_DESEQ_ANALYSIS_TYPE:
            return ScrbSeqDeseqForm

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        if pipeline == RNASEQ_DESEQ_ANALYSIS_TYPE:
            return RunRnaSeqDeseq
        if pipeline == MARSSEQ_DESEQ_ANALYSIS_TYPE:
            return RunMarsSeqDeseq
        if pipeline == SCRBSEQ_DESEQ_ANALYSIS_TYPE:
            return RunScrbSeqDeseq

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        self.check_validation(analysis_form, request, 'input_folder')

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        if  pipeline.endswith('DESeq2'): # Here the output_folder contains the output of the analysis, but in regular analysis the output_folder is the parent folder of the output
            deseq_run_nanem_stripped = re.sub('[^0-9a-zA-Z-_]+', '_', request.POST['deseq_run_name'].strip())
            self.model_saved_.name += '_' + deseq_run_nanem_stripped
            self.output_dir_ = analysis_form.cleaned_data['output_folder']

    def get(self, request, pipeline, *args, **kwargs):
        params_file = request.GET['params_file']
        with open(params_file) as f:
            params_raw_content = f.read()
        parameters = json.JSONDecoder().decode(params_raw_content)
        for field_name, value in parameters.items():
            if field_name != 'pipeline':  # get already have pipeline parameter
                kwargs.update({field_name: value})
        return super(AnalysisDeseqView, self).get(request, pipeline, *args, **kwargs)
