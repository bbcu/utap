import abc
import logging

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from analysis.backend.forms.forms_deseq_to_counts_matrix import DESeqFromCountsMatrixForm
from analysis.backend.run.run_deseq_from_counts_matrix import RunDESeqFromCountsMatrix
from analysis.backend.utils.consts import FUTURE_DATA_TYPE, DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE
from analysis.backend.views.views_analysis_base import AnalysisBaseView
from analysis.backend.views.views_general import LoginAsUser

logger = logging.getLogger(__name__)


# csrf_exempt cancel the need for csrf in the login. For enabling to login with python code
@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisDESeqFromCountsMatrixView(AnalysisBaseView):
    @abc.abstractmethod
    def get_form_class(self, pipeline):
        if pipeline == DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE:
            return DESeqFromCountsMatrixForm
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        if pipeline == DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE:
            return RunDESeqFromCountsMatrix
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        self.check_validation(analysis_form, request, 'input_folder',pipeline=DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE)

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        # if output_dir is empty, the output folder created in run function with name and date.
        self.output_dir_ = None

    def post(self, request, pipeline, *args, **kwargs):
        if 'user' in request.GET.keys():  # submit user post analyses with user argument
            username = request.GET['user']
            LoginAsUser().login_as_user(username, request)
        return super(AnalysisDESeqFromCountsMatrixView, self).post(request, pipeline, *args, **kwargs)




