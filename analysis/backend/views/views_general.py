"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import logging
import os
import re
import fnmatch
import urllib2

from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.cache import cache
from django.core.files.uploadedfile import TemporaryUploadedFile
from django.core.files.uploadhandler import TemporaryFileUploadHandler
from django.http import HttpResponse, Http404
from django.http import HttpResponseRedirect
from django.http import HttpResponseServerError
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_bytes
from django.utils.encoding import force_str
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.utils.http import urlsafe_base64_encode
from django.views import View
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django_auth_ldap.backend import LDAPBackend

from analysis.backend.forms.forms_base import AnalysisChoiceForm, TestForm, LoginAsUserForm, SignUpForm
from analysis.backend.forms.forms_base import UploadFileForm
from analysis.backend.settings import LAB_DIR, DOCKER_USERS, DEMO_SITE, PIPELINE_URL_HTTP, MAX_UPLOAD_SIZE, UTAP_VERSION
from analysis.backend.utils.consts import ELEARNING_PIPELINES_LIST,DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE, DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE
from analysis.backend.utils.delete_job import delete_job
from analysis.backend.utils.emails import ConfirmUserNotification, Notifier
from analysis.backend.utils.general import samples_in_fastq_dir, updated_user_dir, folder_exists, has_space_on_disc, get_samples_names, get_samples_dimultiplexing, check_samplesheet,check_pools
from analysis.backend.utils.tokens import account_activation_token
from analysis.backend.utils.users import get_user_dir
from analysis.models import Analysis, CustomUser, Profile

logger = logging.getLogger(__name__)


def logout_view(request):
    try:
        logout(request)
    except Exception:
        pass
    return HttpResponseRedirect('/user_login/')


def samples_list_ajax(request):
    root_dir = request.GET['root_dir']
    pipeline = request.GET['pipeline']
    if not root_dir:
        return Http404()
    user = request.user.username
    user_base_dir = get_user_dir(user)
    root_dir_path = os.path.join(user_base_dir, root_dir)
    if pipeline == DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE:
        samples, error_msg = get_samples_names(root_dir_path)
    elif pipeline == DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE:
        samples, error_msg= get_samples_dimultiplexing(root_dir_path)
    else:
        samples, error_msg = samples_in_fastq_dir(root_dir_path, pipeline)
    if not samples:
        return JsonResponse({'message': error_msg}, status=400)
        # return Http404(error)
    data = {
        'samples': samples
    }
    return JsonResponse(data)

def check_samplesheet_ajax(request):
    data_string = request.GET['json_data']
    data_dict = json.loads(data_string)
    samplesheet= data_dict['samplesheet_data']
    samplesheet_pools = data_dict['pool_data']
    clean_samplesheet, err_msg= check_samplesheet(samplesheet)
    if not clean_samplesheet:
        return JsonResponse({'message': err_msg}, status=400)
    if samplesheet_pools:
        logger.info("pool not empty")
        logger.info(samplesheet_pools)
        clean_samplesheet_pool, err_msg_pool = check_samplesheet(samplesheet_pools)
        valid_pools, err_msg_pools= check_pools(clean_samplesheet_pool,clean_samplesheet)
        if not clean_samplesheet_pool or not valid_pools:
            return JsonResponse({'message': err_msg_pool+"\n"+err_msg_pools}, status=400)
    data = {
        'valid': "Youre samples names and indexes are correct you can now run the analysis"
    }
    return JsonResponse(data)



def folder_exists_ajax(request):
    folder = request.GET['folder']
    if not folder:
        return Http404()
    exists, message = folder_exists(folder, input=False)
    if not exists:
        return JsonResponse({'message': message}, status=400)
    data = {
        'folder': exists
    }
    return JsonResponse(data)


def has_space_on_disc_ajax(request):
    folder = request.GET['folder']
    if not folder:
        return Http404()
    has_free_space, message = has_space_on_disc(folder)
    logger.info('User have free space in folder %s: %s' % (folder, has_free_space))
    logger.info(message)
    if not has_free_space:
        return JsonResponse({'message': message}, status=400)
    data = {
        'folder': has_free_space
    }
    return JsonResponse(data)


# samples list on Stefan server
def remote_samples_list(run_id, username, list_format):
    try:
        url = 'http://stefan.weizmann.ac.il/cgi-bin/getSamplesUrl?run_id=%s&user=%s' % (run_id, username)
        data_str = urllib2.urlopen(url).read()
    except Exception as e:
        return JsonResponse({'message': str(e)}, status=400)
    if not data_str:
        return JsonResponse({'message': "Cannot find files on remote server. "}, status=400)
    samples = data_str.split(' ')
    logger.info('Get remote samples of run_id %s: %s' % (run_id, samples))
    if list_format:
        return sorted(samples)
    data = {
        'samples': sorted(samples)
    }
    return JsonResponse(data)


# samples list on Stefan server by ajax
def remote_samples_list_ajax(request, list_format=False, post=False):
    if post:
        run_id = request.POST['run_id']
    else:
        run_id = request.GET['run_id']
    username = request.user.username
    if request.user.loggedas_username:
        username = CustomUser.objects.get(username=request.user.loggedas_username)
    return remote_samples_list(run_id, username, list_format)


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            domain = get_current_site(request).domain
            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = account_activation_token.make_token(user)
            activate_url = 'activate'
            notification = ConfirmUserNotification(user.username, user.first_name, user.last_name, domain, activate_url,
                                                   uid, token, user.password[:8])
            Notifier(notification, user.email)
            return render(request, 'analysis/account_activation_sent.html', {'user': user})
    else:
        form = SignUpForm()
    return render(request, 'analysis/signup.html', {'form': form})


def activate_user(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, CustomUser.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        profile = Profile.objects.get(user=user)
        profile.email_confirmed = True
        profile.save()
        user_dir = os.path.join(LAB_DIR, user.username)
        if not os.path.exists(user_dir):
            os.makedirs(user_dir, mode=0700)
        os.system("sudo deluser %s" % (user.username))
        os.system(
            "sudo useradd --groups %s --shell /bin/bash --home-dir %s %s" % (DOCKER_USERS, user_dir, user.username))
        os.system("echo \"%s:%s\" | sudo chpasswd" % (user.username, user.password[:8]))
        os.system("chmod 755 %s" % (os.path.dirname(user_dir)))
        os.system("chmod 777 %s" % (user_dir))
        # os.system("sudo setfacl -m u:%s:rx %s" %(user.username, os.path.dirname(user_dir)))
        # os.system("sudo setfacl -R -m u:%s:rwx %s" %(user.username, user_dir))

        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        return HttpResponseRedirect(reverse('analysis:user_datasets'))

    else:
        return render(request, 'analysis/account_activation_invalid.html')


@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class LoginAsUser(View):
    form_class = LoginAsUserForm
    template_name = 'analysis/login_as.html'

    def login_as_user(self, username, request):
        if not request.user.is_superuser:
            return False, 'You have not permission to login as another user.'
        # Create user if not exists
        if not CustomUser.objects.filter(username=username).exists():
            user_obj = LDAPBackend().populate_user(username)  # here user is created
            if user_obj is not None:
                user_obj.save()
            else:
                return False, 'Cannot login as user: %s, No such user in the database. Please check if the user has account on LDAP.' % username
        try:
            current_user_obj = CustomUser.objects.get(username=request.user.username)
            current_user_obj.loggedas_username = username
            current_user_obj.loggedas_lab_path = updated_user_dir(username)
            current_user_obj.save()
            logged_as_user = CustomUser.objects.get(username=username)
            logged_as_user.lab_path = current_user_obj.loggedas_lab_path
            logged_as_user.save()
        except Exception:
            return False, 'Cannot login as user: %s, No such user.' % username
        return True, 'You logged in as user: %s' % username

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            sucsses, message = self.login_as_user(username, self.request)
            if sucsses:
                messages.success(request, message)
            else:
                messages.error(request, message)
        return HttpResponseRedirect(reverse('analysis:user_datasets'))


#@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class Elearning(generic.ListView):
    template_name = 'analysis/e_learning.html'
    # , 'demosite': DEMO_SITE
    def get_queryset(self):
        return

    def get_context_data(self, *args, **kwargs):
        context = super(Elearning, self).get_context_data(**kwargs)
        context['demosite'] = DEMO_SITE
        context['version'] = UTAP_VERSION
        context['pipeliens']=ELEARNING_PIPELINES_LIST
        return context

@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class UserDatasets(generic.ListView):
    template_name = 'analysis/user_datasets.html'
    # , 'demosite': DEMO_SITE
    def get_queryset(self):
        return

    def group_by_output_folder(self, records):
        for i in xrange(len(records)):
            if i != len(records) - 1:
                for j in xrange(i + 1, len(records)):
                    if str(records[i].output_folder) == str(records[j].output_folder):
                        if j != len(records) - 1:
                            records = records[:i] + records[j:j + 1] + records[i:j] + records[j + 1:]
                        else:
                            records = records[:i] + [records[j]] + records[i:j]
                        break
        return list(reversed(records))

    def get_context_data(self, *args, **kwargs):
        user = self.request.user
        if self.request.user.loggedas_username:
            user = CustomUser.objects.get(username=self.request.user.loggedas_username)
        context = super(UserDatasets, self).get_context_data(**kwargs)
        records = Analysis.objects.filter(user=user).order_by('date')
        records = self.group_by_output_folder(records)
        context['analysis_list'] = records
        context['demosite'] = DEMO_SITE
        context['version'] = UTAP_VERSION
        return context


@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisDetailsView(generic.DetailView):
    model = Analysis
    template_name = 'analysis/analysis_details.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AnalysisDetailsView, self).get_context_data(**kwargs)
        context['demosite'] = DEMO_SITE
        context['version'] = UTAP_VERSION
        return context

@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisChoice(View):
    form_class = AnalysisChoiceForm
    template_name = 'analysis/analysis_choice.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AnalysisChoice, self).get_context_data(**kwargs)
        context['demosite'] = DEMO_SITE
        context['version'] = UTAP_VERSION
        return context

    def get(self, request, *args, **kwargs):
        choice_form = self.form_class(request, *args, **kwargs)
        return render(request, self.template_name, {'form_choice': choice_form})

    def post(self, request, *args, **kwargs):
        choice_form = self.form_class(request, request.POST, *args, **kwargs)
        if choice_form.is_valid():
            return HttpResponseRedirect(reverse('analysis:analysis', args=(request.POST['pipeline'],)))
        else:
            error = "Error, the pipeline does not work for now. Please choose another pipeline"
            return render(request, self.template_name, {'form_choice': choice_form, 'error_message': error})


@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class DeleteAnalysisView(View):
    def get(self, request, *args, **kwargs):
        job_name = request.GET['job_name']
        pipeline = request.GET['pipeline']
        deleted = delete_job(job_name, pipeline, dry_run=True)
        if type(deleted) is not Exception:
            data = {
                'deleted': deleted
            }
            return JsonResponse(data)
        else:
            return JsonResponse({'message': str(deleted)}, status=400)

    def post(self, request, *args, **kwargs):
        job_name = request.POST['job_name']
        pipeline = request.POST['pipeline']
        deleted = delete_job(job_name, pipeline, dry_run=False)
        if type(deleted) is not Exception:
            data = {
                'deleted': deleted,
                'redirect_url': PIPELINE_URL_HTTP
            }
            return JsonResponse(data)
        else:
            return JsonResponse({'message': str(deleted)}, status=400)


def no_results(request):
    return render(request, 'analysis/no_results_yet.html')


def upload_help(request):
    return render(request, 'analysis/upload_help.html')


def demultiplexing_results(request):
    return render(request, 'analysis/demultiplexing_results.html')


def atacseq_results(request):
    return render(request, 'analysis/atacseq_results.html')

def chipseq_results(request):
    return render(request, 'analysis/chipseq_results.html')


def run_failed(request):
    return render(request, 'analysis/run_failed.html')


class FullPathTemporaryUploadedFile(TemporaryUploadedFile):
    def __repr__(self):
        return force_str("<%s: %s (%s)>" % (
            self.__class__.__name__, self.name, self.content_type))

    def _set_name(self, name):
        # Sanitize the file name so that it can't be dangerous.
        if name is not None:
            pass
            # Just use the basename of the file -- anything else is dangerous.
            # name = os.path.basename(name)

            # File names longer than 255 characters can cause problems on older OSes.
            # if len(name) > 255:
            #     name, ext = os.path.splitext(name)
            #     ext = ext[:255]
            #     name = name[:255 - len(ext)] + ext
        self._name = name

    def _get_name(self):
        return self._name

    name = property(_get_name, _set_name)


class ProgressBarUploadHandler(TemporaryFileUploadHandler):
    """
    Tracks progress for file uploads.
    The http post request must contain a header or query parameter, 'X-Progress-ID'
    which should contain a unique string to identify the upload to be tracked.

    Copied from:
    http://djangosnippets.org/snippets/678/

    See views.py for upload_progress function...
    """

    def __init__(self, request=None):
        super(ProgressBarUploadHandler, self).__init__(request)
        self.progress_id = None
        self.cache_key = None

    def handle_raw_input(self, input_data, META, content_length, boundary, encoding=None):
        self.content_length = content_length
        if 'X-Progress-ID' in self.request.GET:
            self.progress_id = self.request.GET['X-Progress-ID']
        elif 'X-Progress-ID' in self.request.META:
            self.progress_id = self.request.META['X-Progress-ID']
        if self.progress_id:
            self.cache_key = "%s_%s" % (self.request.META['REMOTE_ADDR'], self.progress_id)
            cache.set(self.cache_key, {
                'length': self.content_length,
                'uploaded': 0
            })

    def new_file(self, *args, **kwargs):
        """
        Create the file object to append to as data is coming in.
        """
        super(TemporaryFileUploadHandler, self).new_file(*args, **kwargs)
        self.file = FullPathTemporaryUploadedFile(self.file_name, self.content_type, 0, self.charset,
                                                  self.content_type_extra)

    def receive_data_chunk(self, raw_data, start):
        self.file.write(raw_data)  # save to temporary location
        # save the progressing
        if self.cache_key:
            data = cache.get(self.cache_key)
            if data:
                data['uploaded'] += self.chunk_size
            else:
                data = {'uploaded': start}
                #            data['uploaded'] += self.chunk_size
            cache.set(self.cache_key, data)
        return raw_data

    # complete to write file to temporary location
    def file_complete(self, file_size):
        self.file.seek(0)
        self.file.size = file_size
        return self.file

    # complete to upload file to server (not wrriten to file - only in the memory
    def upload_complete(self):
        if self.cache_key:
            cache.delete(self.cache_key)


def upload_progress(request):
    """
    A view to report back on upload progress.
    Return JSON object with information about the progress of an upload.

    Copied from:
    http://djangosnippets.org/snippets/678/

    See upload.py for file upload handler.
    """
    progress_id = ''
    if 'X-Progress-ID' in request.GET:
        progress_id = request.GET['X-Progress-ID']
    elif 'X-Progress-ID' in request.META:
        progress_id = request.META['X-Progress-ID']
    if progress_id:
        # from django.utils import simplejson
        cache_key = "%s_%s" % (request.META['REMOTE_ADDR'], progress_id)
        data = cache.get(cache_key)
        return HttpResponse(json.dumps(data))
    else:
        return HttpResponseServerError(
            'Server Error: You must provide X-Progress-ID header or query param.')


def check_if_exists(f, user_base_dir):
    fixed_path = re.sub(r"[\s]", '_', f.name)
    root_folder = os.path.join(fixed_path.split('/')[0])
    if os.path.exists(os.path.join(user_base_dir, root_folder)):
        e = "The folder %s already exists on the server. Please delete it and then upload again" % (os.path.join(user_base_dir, root_folder))
        logger.error(e)
        raise IOError(e)
    return root_folder


def write_file(f, user_base_dir):
    fixed_path = re.sub(r"[\s]", '_', f.name)
    file_path = os.path.join(user_base_dir, fixed_path)
    folders_path = '/'.join(file_path.split('/')[:-1])
    os.system("mkdir -p %s" % (folders_path))
    with open(os.path.join(file_path), 'wb+') as destination:
        logger.info('Write file to disk: %s' % file_path)
        for chunk in f.chunks():
            destination.write(chunk)


@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class UploadFolder(View):
    form_class = UploadFileForm
    template_name = 'analysis/upload_folder.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'form': self.form_class(), 'demosite': DEMO_SITE, 'max_upload_size': MAX_UPLOAD_SIZE, 'version': UTAP_VERSION})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        files = request.FILES.getlist('file_field')
        username = request.user.username
        if request.user.loggedas_username:
            username = CustomUser.objects.get(username=request.user.loggedas_username)
        user_base_dir = get_user_dir(username)
        if form.is_valid():
            try:
                root_folder = check_if_exists(files[0], user_base_dir)
            except IOError, e:
                logger.error(e.message)
                messages.error(request, e.message)
                return HttpResponseRedirect(reverse('analysis:upfolder'))
            for f in files:
                write_file(f, user_base_dir)
            messages.success(request,
                             'The folder %s uploaded to server successfully. Now you can select this folder in Run Analysis section' % root_folder)
            return HttpResponseRedirect(reverse('analysis:upfolder'))
        else:
            logger.error('The errors in post are %s' % str(form.errors))
            messages.error(request,
                           'The uploading failed because: %s. Please contact the administrator: utap@weizmann.ac.il' % str(
                               form.errors))
            return HttpResponseRedirect(reverse('analysis:upfolder'))


##############################################################################################
# examples:
##############################################################################################

def stest(request):
    return render(request, 'analysis/test_specific.html')


@method_decorator(csrf_exempt, name='dispatch')
def test(request):
    if request.method == 'POST':
        form = TestForm(request.POST)
        if form.is_valid():
            return HttpResponse('your choice is: ' + request.POST.get('favorite_colors'))
    else:
        form = TestForm()
    return render(request, 'analysis/test.html', {'form': form})


def temp(request):
    if request.method == 'POST':
        form = TestForm(request.POST)
        if form.is_valid():
            return HttpResponse('your choice is: ' + request.POST.get('favorite_colors'))
    else:
        form = TestForm()
    return render(request, 'analysis/temp.html', {'form': form})

class analysis_progress_ajax(View):
    def get(self, request, *args, **kwargs):
        analysis_ID = request.GET['analysis_ID']
        logger.info(analysis_ID)
        logger.info(request.GET.keys())
        logger.info('Get output folder of ' + analysis_ID)
        analysis_name= Analysis.objects.get(name=analysis_ID)
        outdir= analysis_name.output_folder
        logger.info(analysis_name.pipeline)
        logger.info(analysis_name)
        logger.info(request)
        snakefile = fnmatch.filter(os.listdir(str(outdir)), 'snakemake_stdout_*')
        matches=[]
        # if not "DESeq_from_counts_matrix" in outdir:
        if not analysis_name.pipeline == 'DESeq from counts matrix':
            snakemake_stdout=os.path.join(str(outdir),snakefile[0])
            if os.path.isfile(snakemake_stdout):
                try:
                    with open(snakemake_stdout, 'r') as f:
                            for l in f.readlines():
                                pattern = re.compile('(\d{1,3}%)')
                                matches += pattern.findall(l)
                            if matches:
                                percent = {
                                    'percent': matches[-1]
                                }
                                return JsonResponse(percent)
                            else:
                                percent = {
                                    'percent': '0%'
                                }
                                return JsonResponse(percent)

                except FileNotFoundError as message:
                    return JsonResponse({ 'message': message }, status=400)
            
        else:
             try:
                 log_name = fnmatch.filter(os.listdir(str(outdir)), 'report_log*')
                 log_file = os.path.join(str(outdir), log_name[0])
                 if os.path.isfile(log_file):
                     with open(log_file, 'r') as f:
                            for l in f.readlines():
                              pattern = re.compile('step_\d{1,2}')
                              matches += pattern.findall(l)
                            if matches:
                                match_percent = int(matches[-1].split('_')[1]) * 100 / 12
                                percent = {
                                     'percent': str(match_percent) + '%'
                                 }
                                return JsonResponse(percent)
                            else:
                                 percent = {
                                     'percent': '0%'
                                 }
                                 return JsonResponse(percent)
             except FileNotFoundError as message:
                return JsonResponse({'message': message}, status=400)



             # log=os.path.join(str(outdir),output[0])
             # logger.info(log)
             # if os.path.isfile(log):
             #    logger.info(log)
             #    try:
             #        with open(log, 'r') as f:
             #            for l in f.readlines():
             #                pattern = re.compile('label:')
             #                matches += pattern.findall(l)
             #            count = matches.count('label:')
             #            logger.info(count)
             #            percent = (count *100)/29
             #            logger.info(percent)
             #            if matches:
             #                percent = {
             #                    'percent': str(percent) + '%'
             #                }
             #                return JsonResponse(percent)
             #            else:
             #                percent = {
             #                    'percent': '0%'
             #                }
             #                return JsonResponse(percent)
             #
             #    except FileNotFoundError as message:
             #        return JsonResponse({'message': message}, status=400)


    #else: None

# from django.views.decorators.csrf import ensure_csrf_cookie
# from django.views.generic.base import TemplateView
# from django.utils.decorators import method_decorator

# def import_data(request):
#     return render(request, 'analysis/import_data.html')

# class import_data(TemplateView):
#     template_name = 'analysis/import_data.html'
#
#     @method_decorator(ensure_csrf_cookie)
#     def dispatch(self, *args, **kwargs):
#         return super(import_data, self).dispatch(*args, **kwargs)
