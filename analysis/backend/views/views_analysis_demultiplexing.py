"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import abc
import logging

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from analysis.backend.forms.forms_demultiplexing import DemultiplexingFastqForm, DemultiplexingRunidForm, \
    DemultiplexingBclForm
from analysis.backend.run.run_demultiplexing import RunDemultiplexingRunid, RunDemultiplexingBcl, RunDemultiplexingFastq
from analysis.backend.utils.consts import DEMULTIPLEXING_RUNID_ANALYSIS_TYPE, DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE, \
    DEMULTIPLEXING_BCL_ANALYSIS_TYPE, FUTURE_DATA_TYPE
from analysis.backend.views.views_analysis_base import AnalysisBaseView

logger = logging.getLogger(__name__)


# csrf_exempt cancel the need for csrf in the login. For enabling to login with python code
@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(login_required(login_url="/user_login/"), name='dispatch')
class AnalysisDemultiplexingView(AnalysisBaseView):
    @abc.abstractmethod
    def get_form_class(self, pipeline):
        if pipeline == DEMULTIPLEXING_RUNID_ANALYSIS_TYPE:
            return DemultiplexingRunidForm
        if pipeline == DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE:
            return DemultiplexingFastqForm
        if pipeline == DEMULTIPLEXING_BCL_ANALYSIS_TYPE:
            return DemultiplexingBclForm
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        if pipeline == DEMULTIPLEXING_RUNID_ANALYSIS_TYPE:
            return RunDemultiplexingRunid
        if pipeline == DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE:
            return RunDemultiplexingFastq
        if pipeline == DEMULTIPLEXING_BCL_ANALYSIS_TYPE:
            return RunDemultiplexingBcl
        elif pipeline == FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureForm'

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        # if output_dir is empty, the output folder created in run function with name and date.
        self.output_dir_ = None

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        if 'bcl_files' in analysis_form.cleaned_data:
            self.check_validation(analysis_form, request, 'bcl_files')

    def post(self, request, pipeline, *args, **kwargs):
        # FileBrowseField makes the fastq_file_R2 to required. So we set "none" value before is_valid function
        request_updated = request
        if "fastq_file_R2" in request.POST and not request.POST["fastq_file_R2"]:
            request.POST._mutable = True
            request.POST.update({"fastq_file_R2": "none"})
        return super(AnalysisDemultiplexingView, self).post(request, pipeline, *args, **kwargs)
