"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import abc
import datetime
import logging
import os
import re



from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from analysis.backend.settings import PIPELINE_URL, RESULTS_DIR, RESULTS_URL, DEMO_SITE, UTAP_VERSION, PIPELINE_SERVER_PORT
from analysis.backend.utils.consts import RunStatus, RNASEQ_DESEQ_ANALYSIS_TYPE, MARSSEQ_DESEQ_ANALYSIS_TYPE
from analysis.backend.utils.general import get_cluster_queue
from analysis.models import CustomUser

logger = logging.getLogger(__name__)


class AnalysisBaseView(View):
    # __metaclass__ = abc.ABCMeta #Cause to bug, becuause the childs of this class don't called to super.__init__

    template_name = 'analysis/analysis.html'

    @abc.abstractmethod
    def get_form_class(self, pipeline):
        pass

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        pass

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        pass

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        pass

    def get(self, request, pipeline, *args, **kwargs):
        form = self.get_form_class(pipeline)
        if not form:
            error_message = "Error, the pipeline doesn\'t work for now. Please choose another pipeline"
            logger.error(error_message)
            messages.error(request, error_message)
            return HttpResponseRedirect(reverse('analysis:choice_analysis'))
        analysis_form = form(request.user, *args, **kwargs)
        advanced_form = [analysis_form.fields[key].get_bound_field(analysis_form,key)for key in analysis_form.fields\
                         if 'form_id' in analysis_form.fields[key].widget.attrs\
                         and analysis_form.fields[key].widget.attrs['form_id'] == 'advanced']
        return render(request, self.template_name, {'form': analysis_form, 'advanced_form': advanced_form, 'pipeline': pipeline, 'demosite': DEMO_SITE, 'version': UTAP_VERSION})


    def clean_cleaned_data(self, cleaned_data, key):
        '''
        :param cleaned_data: hash. for example:  analysis_form.cleaned_data['name']
        :return: None. Updating the value
        '''
        cleaned_data[key] = re.sub('[^0-9a-zA-Z-_]+', '_', cleaned_data[key].strip())

    def save_to_model_shared1(self, request, analysis_form, pipeline, date):
        self.clean_cleaned_data(analysis_form.cleaned_data, 'name')
        self.model_saved_.name = date + '_' + analysis_form.cleaned_data['name']
        self.analysis_class_ = self.get_analysis_class(pipeline)
        self.model_saved_.status = RunStatus.RUNNING
        self.model_saved_.pipeline = pipeline
        self.model_saved_.user_run = request.user
        self.model_saved_.user = request.user
        self.run_by_user_ = request.user
        if request.user.loggedas_username:
            self.model_saved_.user = CustomUser.objects.get(username=request.user.loggedas_username)
        self.model_saved_.results = "%s/no_results/" % (PIPELINE_URL)

    def save_to_model_shared2(self):
        params_yaml_file_name = self.model_saved_.name + "_analysis_parameters" + ".yaml"
        params_json_file_name = self.model_saved_.name + "_analysis_parameters" + ".json"
        self.parameters_yaml_path_ = os.path.join(RESULTS_DIR, params_yaml_file_name)
        self.parameters_json_path_ = os.path.join(RESULTS_DIR, params_json_file_name)
        params_link = '%s/%s' % (RESULTS_URL, params_yaml_file_name)
        self.model_saved_.parameters = params_link
        self.model_saved_.parameters_json = self.parameters_json_path_

    def save_to_model(self, request, analysis_form, pipeline, date):
        self.save_to_model_shared1(request, analysis_form, pipeline, date)
        self.save_to_model_specific_to_pipeline(pipeline, analysis_form, request)
        self.save_to_model_shared2()

    def check_output_folder_valiation(self, analysis_form, request):
        if not os.path.isdir(analysis_form.cleaned_data['output_folder']) and str(request.user) != 'submit':
            messages.error(request,
                           'Your analysis cannot run. The output directory %s doesn\'t exist (is it the full path to the input folder ?). Verify that you use a supported browser.' %
                           analysis_form.cleaned_data['output_folder'])
            return HttpResponseRedirect(reverse('analysis:user_datasets'))
        return None

    def check_validation(self, analysis_form, request, input_folder,pipeline=None):
        logger.info(pipeline)
        if input_folder:
            logger.info(analysis_form.cleaned_data[input_folder])
            if pipeline == 'DESeq2 from counts matrix':
                if not os.path.isfile(analysis_form.cleaned_data[input_folder]) :
                    messages.error(request,
                                   'Your analysis cannot run. The input file %s doesn\'t exist (is it the full path to the input folder ?). Verify that you use a supported browser.' % analysis_form.cleaned_data[input_folder])
                else:
                    if os.path.splitext(analysis_form.cleaned_data[input_folder])[1] != '.csv' and\
                        os.path.splitext(analysis_form.cleaned_data[input_folder])[1] != '.txt':
                        logger.info(analysis_form.cleaned_data[input_folder])
                        messages.error(request,'Please choose a csv or txt file format as input file')
            else:
                if analysis_form.cleaned_data['output_folder'] == analysis_form.cleaned_data[input_folder]:
                    messages.error(request,
                                   'Your analysis cannot run. The output directory should be different from the input directory. Please run again with another output directory.')
                    return HttpResponseRedirect(reverse('analysis:user_datasets'))
                if not os.path.isdir(analysis_form.cleaned_data[input_folder]) and str(request.user) != 'submit':
                    messages.error(request,
                                   'Your analysis cannot run. The input directory %s doesn\'t exist (is it the full path to the input folder ?). Verify that you use a supported browser.' %
                                   analysis_form.cleaned_data[input_folder])
                    return HttpResponseRedirect(reverse('analysis:user_datasets'))
                else:
                    if not os.access(analysis_form.cleaned_data[input_folder], os.R_OK) or not os.access(analysis_form.cleaned_data[input_folder], os.W_OK):
                        messages.error(request,
                                       'Your analysis cannot run. You don\'t have permissions to the chosen input directory: %s. Please verify your input directory permissions or choose another directory before running the pipeline.' %
                                       analysis_form.cleaned_data[input_folder])
        return self.check_output_folder_valiation(analysis_form, request)

    def post(self, request, pipeline, *args, **kwargs):
        date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        form = self.get_form_class(pipeline)
        analysis_form = form(request.user, request.POST, *args, **kwargs)
        if DEMO_SITE and pipeline != RNASEQ_DESEQ_ANALYSIS_TYPE and pipeline != MARSSEQ_DESEQ_ANALYSIS_TYPE:
            error_message = 'Note: This is a demonstration site for UTAP, showcasing the user interface and examples of run results.It provides partial functionality, including the capability to rerun the DESeq2 step on existing analyses, but does not support running new analyses by selecting \"run DESeq2 again with other parameters\" button'
            logger.error(error_message)
            messages.error(request, error_message)
            return HttpResponseRedirect(reverse('analysis:user_datasets'))
        if analysis_form.is_valid():
            self.model_saved_ = analysis_form.save(commit=False)
            self.output_dir_ = None
            self.save_to_model(request, analysis_form, pipeline, date)
            not_validated = self.check_validation_specific_to_pipeline(analysis_form, request)
            if not_validated:
                return not_validated
            response, results_link, full_output_folder = self.analysis_class_(self.model_saved_.user.username,
                                                                              self.run_by_user_.username,
                                                                              self.model_saved_.name, pipeline, request,
                                                                              analysis_form.cleaned_data,
                                                                              get_cluster_queue(request),
                                                                              self.parameters_yaml_path_,
                                                                              self.parameters_json_path_,
                                                                              request.POST, date,
                                                                              self.output_dir_).send_analysis()
            if not response or type(response) is not Exception:
                self.model_saved_.results_temp = results_link
                self.model_saved_.output_folder.path = full_output_folder
                self.model_saved_.save()
                messages.success(request, 'Your analysis was sent, you will get an email in the end of the run')
                return HttpResponseRedirect(reverse('analysis:user_datasets'))
            else:  # response of type Exception
                error_message = 'Error message is: ' + str(response)
                messages.error(request,
                               'Your analysis cannot run %s. Please contact the administrator: utap@weizmann.ac.il' % error_message)
                return HttpResponseRedirect(reverse('analysis:user_datasets'))
        else:
            error_message = 'The errors in post are %s' % str(analysis_form.errors)
            logger.error(error_message)
            messages.error(request,
                           "Error, The parameters are not correct: %s. The analysis didn't run. Please choose the parameters again." % error_message)
            return HttpResponseRedirect(reverse('analysis:user_datasets'))
