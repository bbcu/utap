"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import re
import logging
from analysis.backend.run.run_ngs_transcriptome_marsseq import RunMarsSeqDeseq, RunMarsSeq
from analysis.backend.utils.consts import SCRBSEQ_ANALYSIS_TYPE
from analysis.backend.settings import SNAKEFILE_MARSSEQ, SNAKEFILE_SCRIPTS, SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, \
    RSCRIPT, RESULTS_URL, R_LIB_PATHS, CUTADAPT_EXE, FASTQC_EXE, STAR_EXE, SAMTOOLS_EXE, NGS_PLOT_EXE, HTSEQ_COUNT_EXE, \
    PIPELINE_URL, CONDA_ROOT, GS_EXE, MAX_CORES

logger = logging.getLogger(__name__)


class RunScrbSeq(RunMarsSeq):
    pipeline_name_ = SCRBSEQ_ANALYSIS_TYPE
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunScrbSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.config_file = os.path.join(self.output_dir, 'config-scrb-%s.yaml' % self.date)
        self.umi_barcode_length = self.clean_data_form.UMI_barcode_length if self.clean_data_form.UMI_barcode_length else ""


    def create_config_file(self):
        super(RunScrbSeq, self).create_config_file()
        with open(self.config_file, "a") as conf_f:
            conf_f.write('UMI_barcode_length: "' + str(self.clean_data_form.UMI_barcode_length) + '"\n')




class RunScrbSeqDeseq(RunMarsSeqDeseq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunScrbSeqDeseq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                              params_yaml_file, params_json_file, post_params, date, output_dir,
                                              **kwargs)
