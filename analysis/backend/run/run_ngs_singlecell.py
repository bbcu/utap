"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging

from analysis.backend.forms.forms_singlecell import SinglecellForm
from analysis.backend.run.run_ngs import RunAnalysisNGS
from analysis.backend.utils.consts import SINGLECELL_CELRANGER_ANALYSIS_TYPE
from analysis.backend.settings import SNAKEFILE_SINGLECELL, SNAKEFILE_SCRIPTS, \
    SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, CELLRANGER_EXE, PIPELINE_URL_HTTP, RESULTS_URL, MAX_CORES

logger = logging.getLogger(__name__)

class RunSinglecell(RunAnalysisNGS):
    pipeline_name_ = SINGLECELL_CELRANGER_ANALYSIS_TYPE

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunSinglecell, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                            params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.results_link_ = '%s/%s/report.html' % (RESULTS_URL, self.job_name)
        if self.run_by_user == 'submit' and 'Submit' not in self.pipeline:  # run submitted
            if not os.path.isdir(self.input_dir):
                raise Exception('The copy of the run %s from stefan don\'t exists.' % self.clean_data_form.run_id)
        # self.copy_fastq_directory()
        if not self.output_dir:  # In case of Submitted analyses the class get output_dir as input
            self.output_dir = os.path.join(self.prj_dir, self.job_name + '_' + '_'.join(self.pipeline_name_.split(' ')))
        self.username = username
        self.genome = self.clean_data_form.cellranger_genome.path
        self.snakefile_source = SNAKEFILE_SINGLECELL
        self.config_file = os.path.join(self.output_dir, 'config-singlecell-%s.yaml' % self.date)
        self.results_dir = os.path.join(self.output_dir, '3_reports')



    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in SinglecellForm(self.username).fields:
            if 'form_id' in SinglecellForm(self.username).fields[key].widget.attrs and \
                    SinglecellForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in SinglecellForm(self.username).fields[key].widget.attrs and \
                    SinglecellForm(self.username).fields[key].widget.attrs['form_id'] == 'basic' and key != "genome":
                self.basic[key] = self.post_params[key]

        self.basic["genome"] = self.genome
        self.relevant_parameters['advanced_parameters'] = self.advanced
        self.relevant_parameters['basic_parameters'] = self.basic



    def create_config_file(self):
        with open(self.config_file, 'w') as conf_f:
            conf_f.writelines('---\n')
            conf_f.writelines('pipeline: "' + self.pipeline_name_ + '"\n')
            conf_f.writelines('run_id: "' + self.date + '"\n')
            conf_f.writelines('job_name: "' + self.job_name + '"\n')
            conf_f.writelines('fastq_dir: ' + self.input_dir + '\n')
            conf_f.writelines('output_dir: ' + self.output_dir + '\n')
            conf_f.writelines('cellranger_transcriptom: ' + self.genome + '\n')  #######
            conf_f.writelines('cellranger_exe: ' + CELLRANGER_EXE + '\n')  ########
            conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
            conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
            conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
            conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')

    def specific_steps_to_run(self):
        return ''


class SubmitSinglecell(RunSinglecell):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(SubmitSinglecell, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                               params_yaml_file, params_json_file, post_params, date, output_dir,
                                               **kwargs)
        self.results_link_ = "%s/no_results/" % (PIPELINE_URL_HTTP)

    # Overide the function of RunAnalysis class
    def send_analysis(self):
        try:
            self.save_relevant_parameters()
            self.create_yaml_parameters_file(self.params_yaml_file)
            json = create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params, self.user,
                                                       self.output_dir)
            logger.info("Parameters of the analysis are: " + json)
            return None, self.results_link_, self.output_dir
        except Exception as e:
            logger.error('Cannot submit analysis because Error: %s' % str(e))
            return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir
