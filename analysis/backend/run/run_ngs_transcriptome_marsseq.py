"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import re
import logging
from analysis.backend.forms.forms_transcriptome import TranscriptomeBaseForm
from analysis.backend.forms.forms_transcriptome import MarsSeqForm
from analysis.backend.run.run_ngs_transcriptome import RunTranscriptome
from analysis.backend.utils.consts import MARSSEQ_ANALYSIS_TYPE
from analysis.backend.settings import SNAKEFILE_MARSSEQ, SNAKEFILE_SCRIPTS, SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, \
    RSCRIPT, RESULTS_URL, R_LIB_PATHS, CUTADAPT_EXE, FASTQC_EXE, MULTIQC_EXE, STAR_EXE, SAMTOOLS_EXE, NGS_PLOT_EXE, HTSEQ_COUNT_EXE, \
    PIPELINE_URL, CONDA_ROOT, GS_EXE, MAX_CORES

logger = logging.getLogger(__name__)


class RunMarsSeq(RunTranscriptome):
    pipeline_name_ = MARSSEQ_ANALYSIS_TYPE

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunMarsSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.username = username
        #self.report_dir_name = self.parsing_post_obj.get_report_dir_name()
        self.results_link_ = '%s/%s/%s_umi_counts_%s/report.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        self.snakefile_source = SNAKEFILE_MARSSEQ
        self.config_file = os.path.join(self.output_dir, 'config-marseq-%s.yaml' % self.date)
        self.gtf = self.clean_data_form.annotation.path3p
        self.results_dir = os.path.join(self.output_dir, '10_reports')
        self.umi_barcode_length= "8"
        self.UMI="yes"
        self.paired_end="no"
        # advanced parameters:
        # DEseq2
        self.correct_with_FDR_tool = self.clean_data_form.correct_with_FDR_tool
        self.log2_Fold_Change = self.clean_data_form.log2_Fold_Change
        self.adjusted_P_value = self.clean_data_form.adjusted_P_value
        self.baseMean = self.clean_data_form.baseMean
        self.log2FC = self.clean_data_form.log2_Fold_Change
        #CUTADAPT
        self.additional_Adapter_R1 = self.clean_data_form.aditional_adapter_on_R1
        self.cutadapt_qvalue = self.clean_data_form.cutadapt_qvalue
        self.cutadapt_min_len = self.clean_data_form.cutadapt_min_len
        self.cutadapt_cut_start = "" if str(self.clean_data_form.cutadapt_cut_start) == 'None' else self.clean_data_form.cutadapt_cut_start
        self.cutadapt_cut_end = "" if str(self.clean_data_form.cutadapt_cut_end) == 'None' else self.clean_data_form.cutadapt_cut_end

    # def save_relevant_parameters(self):
    #     self.relevant_parameters['Input dir'] = self.input_dir
    #     self.relevant_parameters['Output dir'] = self.output_dir
    #     self.relevant_parameters['Genome'] = self.genome
    #     self.relevant_parameters['Gtf file'] = self.gtf
    #     self.relevant_parameters['Samples'] = self.samples
    #     self.relevant_parameters['Deseq run'] = self.clean_data_form.deseq_run
    #     if self.parsing_post_obj.factors:
    #         self.relevant_parameters['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
    #     if self.parsing_post_obj.batches:
    #         self.relevant_parameters['Batches'] = self.parsing_post_obj.batches

    # def save_relevant_parameters(self):
    #     self.basic = {}
    #     self.advanced = {}
    #     for key in MarsSeqForm(self.username).fields:
    #         if 'form_id' in MarsSeqForm(self.username).fields[key].widget.attrs and \
    #                 MarsSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
    #             self.advanced[key] = self.post_params[key]
    #         elif 'form_id' in MarsSeqForm(self.username).fields[key].widget.attrs and \
    #                 MarsSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'basic':
    #             self.basic[key] = self.post_params[key]
    #
    #     self.relevant_parameters['advanced_parameters'] = self.advanced
    #     self.relevant_parameters['basic_parameters'] = self.basic


    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in  MarsSeqForm(self.username).fields:
            if 'form_id' in MarsSeqForm(self.username).fields[key].widget.attrs and \
                    MarsSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in MarsSeqForm(self.username).fields[key].widget.attrs and \
                    MarsSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'basic' and key != "genome" and key != "annotation":
                self.basic[key] = self.post_params[key]
        self.basic["genome"] = self.genome
        self.basic["annotation"] = self.gtf
        self.basic['Samples'] = self.samples
        if self.parsing_post_obj.factors:
             self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
        if self.parsing_post_obj.batches:
             self.basic['Batches'] = self.parsing_post_obj.batches
        self.relevant_parameters['advanced_parameters'] = self.advanced
        self.relevant_parameters['basic_parameters'] = self.basic


    def create_config_file(self):
        with open(self.config_file, 'w') as conf_f:
            conf_f.writelines('---\n')
            conf_f.writelines('pipeline: "' + self.pipeline_name_ + '"\n')
            conf_f.writelines('run_id: "' + self.date + '"\n')
            conf_f.writelines('job_name: "' + self.job_name + '"\n')
            conf_f.writelines('fastq_dir: ' + self.input_dir + '\n')
            conf_f.writelines('report_dir_name: ' + self.report_dir_name + '\n')
            conf_f.writelines('gtf: ' + self.gtf + '\n')
            conf_f.writelines('my_star_index: ' + self.genome + '\n')
            conf_f.writelines('output_dir: ' + self.output_dir + '\n')
            conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
            conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
            conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
            conf_f.writelines('Rscript: ' + RSCRIPT + '\n')
            conf_f.writelines('gs_exe: ' + GS_EXE + '\n')
            conf_f.writelines('R_lib_paths: ' + R_LIB_PATHS + '\n')
            conf_f.writelines('conda_root: ' + CONDA_ROOT + '\n')
            conf_f.writelines('cutadapt_exe: ' + CUTADAPT_EXE + '\n')
            conf_f.writelines('fastqc_exe: ' + FASTQC_EXE + '\n')
            conf_f.writelines('multiqc_exe: ' + MULTIQC_EXE + '\n')
            conf_f.writelines('star_exe: ' + STAR_EXE + '\n')
            conf_f.writelines('samtools_exe: ' + SAMTOOLS_EXE + '\n')
            conf_f.writelines('ngs_plot_exe: ' + NGS_PLOT_EXE + '\n')
            conf_f.writelines('htseq_count_exe: ' + HTSEQ_COUNT_EXE + '\n')
            conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')
            conf_f.writelines('correct_with_FDR_tool: "' + str(self.correct_with_FDR_tool) + '"\n')
            conf_f.writelines('adjusted_P_value: "' + str(self.adjusted_P_value) + '"\n')
            conf_f.writelines('baseMean: "' + str(self.baseMean) + '"\n')
            conf_f.writelines('log2_fold_change: "' + str(self.log2FC) + '"\n')
            conf_f.writelines('additional_Adapter_R1: ' + self.additional_Adapter_R1 + '\n')
            conf_f.writelines('cutadapt_qvalue: "' + str(self.cutadapt_qvalue) + '"\n')
            conf_f.writelines('cutadapt_min_len: "' + str(self.cutadapt_min_len) + '"\n')
            conf_f.writelines('cutadapt_cut_start: "' + str(self.cutadapt_cut_start) + '"\n')
            conf_f.writelines('cutadapt_cut_end: "' + str(self.cutadapt_cut_end) + '"\n')
            conf_f.writelines('UMI_barcode_length: "' + str(self.umi_barcode_length) + '"\n')
            conf_f.writelines('UMI: "' + str(self.UMI) + '"\n')
            conf_f.writelines('paired_end: "' + str(self.paired_end) + '"\n')
            #conf_f.writelines('gene_db: ' + self.clean_data_form.intermine_genome.gene_db_url + '\n')
            #conf_f.writelines('intermine_web_query: ' + self.clean_data_form.intermine_genome.interMine_web_query + '\n')
            #conf_f.writelines('intermine_web_base: ' + self.clean_data_form.intermine_genome.intermine_web_base + '\n')
            #conf_f.writelines('intermine_web_creature: ' + self.clean_data_form.intermine_genome.interMine_creature + '\n') need to enter these in the DB so we can use them
            conf_f.writelines('yaml_file: ' + self.params_yaml_file + '\n')
            if self.parsing_post_obj.factors:
                conf_f.writelines('factors_file: ' + self.factors_file + '\n')
                self.parsing_post_obj.create_factors_file()

    def specific_steps_to_run(self):
        return ''


class SubmitMarsSeq(RunMarsSeq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(SubmitMarsSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                            params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.results_link_ = "%s/no_results/" % (PIPELINE_URL)

    # Overide the function of RunAnalysis class
    def send_analysis(self):
        try:
            self.save_relevant_parameters()
            self.create_yaml_parameters_file(self.params_yaml_file)
            json = self.create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params,
                                                            self.user,
                                                            self.output_dir)
            logger.info("Parameters of the analysis are: " + json)
            return None, self.results_link_, self.output_dir
        except Exception as e:
            logger.error('Cannot submit analysis because Error: %s' % str(e))
            return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir


class RunMarsSeqDeseq(RunMarsSeq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunMarsSeqDeseq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                              params_yaml_file, params_json_file, post_params, date, output_dir,
                                              **kwargs)
        self.report_dir_name = re.sub('[^0-9a-zA-Z-_]+', '_', self.clean_data_form.deseq_run_name.strip())
        self.results_link_ = '%s/%s/%s_umi_counts_%s/report.html' % (
        RESULTS_URL, self.job_name, self.report_dir_name, self.date)
    def specific_steps_to_run(self):
        return 'rule_10_reports'
