"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""


import os
import logging
from analysis.backend.forms.forms_atacseq import AtacSeqForm
from analysis.backend.run.parsing_post import ParsingPost
from analysis.backend.run.run_ngs import RunAnalysisNGS
from analysis.backend.utils.consts import ATACSEQ_ANALYSIS_TYPE
from analysis.backend.settings import SNAKEFILE_ATACSEQ, PIPELINE_URL_HTTP, RESULTS_URL, JAVA_EXE, MULTIQC_EXE, BOWTIE2_EXE, \
    PICARD_EXE, IGVTOOLS_EXE, MACS2_EXE, BEDTOOLS_EXE, SNAKEFILE_SCRIPTS, SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, \
    RSCRIPT, R_LIB_PATHS, CUTADAPT_EXE, FASTQC_EXE, SAMTOOLS_EXE, NGS_PLOT_EXE, CONDA_ROOT, GS_EXE, MAX_CORES, PIPELINE_SERVER_PORT

logger = logging.getLogger(__name__)

class RunAtacSeq(RunAnalysisNGS):
    pipeline_name_ = ATACSEQ_ANALYSIS_TYPE

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunAtacSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)

        self.snakefile_source = SNAKEFILE_ATACSEQ
        if not self.output_dir:  # In case of Submitted analyses the class get output_dir as input
            self.output_dir = os.path.join(self.prj_dir, self.job_name + '_' + self.pipeline_name_)
        self.config_file = os.path.join(self.output_dir, 'config-atacseq-%s.yaml' % self.date)
        self.factors_file = None if self.clean_data_form.treat_vs_control == 'no' else os.path.join(
            self.output_dir, 'pheno_data-%s.tsv' % self.date)
        logger.info("Run ATAC-Seq pipeline with treat_vs_control ? %s" %self.clean_data_form.treat_vs_control)
        self.parsing_post_obj = ParsingPost(self.samples, self.factors_file, self.post_params_obj, self.pipeline)
        self.report_dir_name = self.parsing_post_obj.get_report_dir_name() if self.parsing_post_obj.get_report_dir_name() else "_".join(
            self.job_name.split("_", 2)[2:])
        self.results_dir = os.path.join(self.output_dir, '11_reports')
        if PIPELINE_SERVER_PORT != 7000:
            self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        else:
            self.results_link_ = "%s/atacseq_results/" % (PIPELINE_URL_HTTP)
        #self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        #self.results_link_ = "%s/atacseq_results/" % (PIPELINE_URL_HTTP)
        if self.run_by_user == 'submit' and 'Submit' not in self.pipeline:  # run submitted
            if not os.path.isdir(self.input_dir):
                raise Exception('The copy of the run %s from stefan don\'t exists.' % self.clean_data_form.run_id)
        # self.copy_fastq_directory()
        self.username = username
        self.genome = self.clean_data_form.genome.path
        if self.clean_data_form.tss_file:
            self.tss_file= self.clean_data_form.tss_file.path
        else:
            self.tss_file =""
        self.alias = self.clean_data_form.genome.alias
        self.adaptor1 = self.clean_data_form.adapter_on_R1.lstrip()
        self.adaptor2 = self.clean_data_form.adapter_on_R2.lstrip()
        #advanced parameters:
        #CUTADAPT
        self.additional_Adapter_R1 = self.clean_data_form.aditional_adapter_on_R1
        self.additional_Adapter_R2 = self.clean_data_form.aditional_adapter_on_R2
        self.cutadapt_qvalue = self.clean_data_form.cutadapt_qvalue
        self.cutadapt_min_len =self.clean_data_form.cutadapt_min_len
        self.cutadapt_cut_start = "" if str(self.clean_data_form.cutadapt_cut_start) == 'None' else self.clean_data_form.cutadapt_cut_start
        self.cutadapt_cut_end = "" if str(self.clean_data_form.cutadapt_cut_end) == 'None' else self.clean_data_form.cutadapt_cut_end
        #MACS2
        self.macs_qvalue = self.clean_data_form.macs_qvalue
        self.macs_bandwidth = self.clean_data_form.macs_bandwidth


    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in AtacSeqForm(self.username).fields:
            if 'form_id' in AtacSeqForm(self.username).fields[key].widget.attrs and \
                    AtacSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in AtacSeqForm(self.username).fields[key].widget.attrs and \
                    AtacSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'basic' and key != "genome":
                self.basic[key] = self.post_params[key]
        self.basic["genome"] = self.genome
        if self.parsing_post_obj.factors:
            self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
        self.relevant_parameters['advanced_parameters'] = self.advanced
        self.relevant_parameters['basic_parameters'] = self.basic

    def create_config_file(self):
        with open(self.config_file, 'w') as conf_f:
            conf_f.writelines('---\n')
            conf_f.writelines('pipeline: "' + self.pipeline_name_ + '"\n')
            conf_f.writelines('run_id: "' + self.date + '"\n')
            conf_f.writelines('job_name: "' + self.job_name + '"\n')
            conf_f.writelines('fastq_dir: ' + self.input_dir + '\n')
            conf_f.writelines('output_dir: ' + self.output_dir + '\n')
            conf_f.writelines('report_link: ' + RESULTS_URL + '/' + self.job_name + '\n')
            conf_f.writelines('yaml_file: ' + self.params_yaml_file + '\n')
            conf_f.writelines('adaptor1: ' + self.adaptor1 + '\n')
            conf_f.writelines('adaptor2: ' + self.adaptor2 + '\n')
            conf_f.writelines('genome: ' + self.genome + '\n')
            conf_f.writelines('tss_file: ' + self.tss_file + '\n')
            conf_f.writelines('genome_alias: ' + self.alias + '\n')
            conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
            conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
            conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
            conf_f.writelines('Rscript: ' + RSCRIPT + '\n')
            conf_f.writelines('R_lib_paths: ' + R_LIB_PATHS + '\n')
            conf_f.writelines('conda_root: ' + CONDA_ROOT + '\n')
            conf_f.writelines('java: ' + JAVA_EXE + '\n')
            conf_f.writelines('cutadapt_exe: ' + CUTADAPT_EXE + '\n')
            conf_f.writelines('fastqc_exe: ' + FASTQC_EXE + '\n')
            conf_f.writelines('multiqc_exe: ' + MULTIQC_EXE + '\n')
            conf_f.writelines('bowtie2_exe: ' + BOWTIE2_EXE + '\n')
            conf_f.writelines('samtools_exe: ' + SAMTOOLS_EXE + '\n')
            conf_f.writelines('picard_exe: ' + PICARD_EXE + '\n')
            conf_f.writelines('igvtools_exe: ' + IGVTOOLS_EXE + '\n')
            conf_f.writelines('ngs_plot_exe: ' + NGS_PLOT_EXE + '\n')
            conf_f.writelines('gs_exe: ' + GS_EXE + '\n')
            conf_f.writelines('macs2_exe: ' + MACS2_EXE + '\n')
            conf_f.writelines('bedtools_exe: ' + BEDTOOLS_EXE + '\n')
            conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')
            conf_f.writelines('additional_Adapter_R1: ' + self.additional_Adapter_R1 + '\n')
            conf_f.writelines('additional_Adapter_R2: ' + self.additional_Adapter_R2 + '\n')
            conf_f.writelines('cutadapt_qvalue: "' + str(self.cutadapt_qvalue) + '"\n')
            conf_f.writelines('cutadapt_min_len: "' + str(self.cutadapt_min_len) + '"\n')
            conf_f.writelines('cutadapt_cut_start: "' + str(self.cutadapt_cut_start) + '"\n')
            conf_f.writelines('cutadapt_cut_end: "' + str(self.cutadapt_cut_end) + '"\n')
            conf_f.writelines('macs2_qvalue: "' + str(self.macs_qvalue) + '"\n')
            conf_f.writelines('macs2_bandwidth: "' + str(self.macs_bandwidth) + '"\n')
            if self.parsing_post_obj.factors:
                conf_f.writelines('treat_vs_control: ' + self.factors_file + '\n')
                self.parsing_post_obj.create_factors_file()

    def specific_steps_to_run(self):
        return ''


class SubmitAtacSeq(RunAtacSeq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunAtacSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir,
                                         **kwargs)
        #self.results_link_ = "%s/no_results/" % (PIPELINE_URL_HTTP)
        #self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        if PIPELINE_SERVER_PORT != 7000:
            self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        else:
            self.results_link_ = "%s/chipseq_results/" % (PIPELINE_URL_HTTP)

    # Overide the function of RunAnalysis class
    def send_analysis(self):
        try:
            self.save_relevant_parameters()
            self.create_yaml_parameters_file(self.params_yaml_file)
            json = create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params, self.user,
                                                       self.output_dir)
            logger.info("Parameters of the analysis are: " + json)
            return None, self.results_link_, self.output_dir
        except Exception as e:
            logger.error('Cannot submit analysis because Error: %s' % str(e))
            return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir
