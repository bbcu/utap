"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging
from abc import ABCMeta, abstractmethod

from analysis.backend.run.run_ngs import RunAnalysisNGS
from analysis.backend.run.parsing_post import ParsingPost

logger = logging.getLogger(__name__)

class RunTranscriptome(RunAnalysisNGS):
    __metaclass__ = ABCMeta

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunTranscriptome, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                               params_yaml_file, params_json_file, post_params, date, output_dir,
                                               **kwargs)
        if self.run_by_user == 'submit' and 'Submit' not in self.pipeline:  # run submitted
            if not os.path.isdir(self.input_dir):
                raise Exception('The copy of the run %s from stefan don\'t exists.' % self.clean_data_form.run_id)
        # self.copy_fastq_directory()
        if not self.output_dir:  # In case of run Deseq2 again and Submitted analyses the class get output_dir as input
            self.output_dir = os.path.join(self.prj_dir, self.job_name + '_' + '_'.join(self.pipeline_name_.split(' ')))
        self.genome = self.clean_data_form.genome.path
        self.factors_file = None if self.clean_data_form.deseq_run == 'no' else os.path.join(
            self.output_dir, 'pheno_data-%s.tsv' % self.date)
        self.parsing_post_obj = ParsingPost(self.samples, self.factors_file, self.post_params_obj, self.pipeline)
        self.report_dir_name = self.parsing_post_obj.get_report_dir_name() if self.parsing_post_obj.get_report_dir_name() else  "_".join(self.job_name.split("_", 2)[2:])

    @abstractmethod
    def save_relevant_parameters(self):
        pass

    @abstractmethod
    def create_config_file(self):
        pass

    @abstractmethod
    def specific_steps_to_run(self):
        pass

