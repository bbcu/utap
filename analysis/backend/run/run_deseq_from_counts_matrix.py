"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging
import subprocess
import itertools

from analysis.backend.run.run_base import RunAnalysis
from analysis.backend.forms.forms_deseq_to_counts_matrix import DESeqFromCountsMatrixForm
from analysis.backend.run.parsing_post import ParsingPost
from analysis.backend.run.run_ngs import RunAnalysisNGS
from analysis.backend.utils.users import get_user_dir
from analysis.backend.views.views_general import remote_samples_list_ajax
from abc import ABCMeta, abstractmethod
from analysis.backend.utils.general import  get_samples_names
from analysis.backend.utils.consts import DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE
from analysis.backend.settings import SNAKEFILE_DESEQ_FROM_COUNTS_MATRIX, PIPELINE_URL_HTTP, SNAKEFILE_SCRIPTS, SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, RSCRIPT, R_LIB_PATHS, CONDA_ROOT, GS_EXE, MAX_CORES, RESULTS_DIR
from analysis.backend.settings import JOBS_STATUS_FILE, JOB_REPEATS_NUMBER, CLUSTER_EXE, SNAKEMAKE_EXE, PIPELINE_SERVER, \
    USER_CLUSTER, CLUSTER_TYPE, DEMULTIPLEXING_MEMORY, SNAKEFILE_VERSION, UTAP_VERSION, CLUSTER_RESOURCES_PARAMS, RUN_LOCAL, RUN_LOCAL_HOST_CONDA,MAX_CORES,RESULTS_URL



# do we need GS_EXE and MAX_CORES
logger = logging.getLogger(__name__)



class RunDESeqFromCountsMatrix(RunAnalysisNGS):
    __metaclass__ = ABCMeta
    pipeline_name_ = DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunDESeqFromCountsMatrix, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)

        if not self.output_dir:  # In case of Submitted analyses the class get output_dir as input
            self.output_dir = os.path.join(self.prj_dir, self.job_name +'_'+ '_'.join(self.pipeline_name_.split(' ')))
        self.username = username
        self.samples = self.get_samples_list(self.clean_data_form.input_folder)[0]
        self.input_dir = self.clean_data_form.input_folder
        self.snakefile_source = SNAKEFILE_DESEQ_FROM_COUNTS_MATRIX
        self.config_file = os.path.join(self.output_dir, 'config-deseq-from-counts-matrix-%s.yaml' % self.date)
        self.factors_file = None if self.clean_data_form.deseq_run == 'no' else os.path.join(
            self.output_dir, 'pheno_data-%s.tsv' % self.date)
        self.parsing_post_obj = ParsingPost(self.samples, self.factors_file, self.post_params_obj, self.pipeline)
        self.report_dir_name= self.parsing_post_obj.get_report_dir_name() if self.parsing_post_obj.get_report_dir_name() else  "_".join(self.job_name.split("_", 2)[2:])
        self.results_dir = self.output_dir
        self.results_link_ = '%s/%s/%s_%s/report_counts_matrix.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        #self.unique_factors= list(sorted(set(self.parsing_post_obj.factors.values()), key=self.parsing_post_obj.factors.values().index)) #get all unique categories from boxes
        #self.factors= ['no-deseq'] if not self.parsing_post_obj.factors else []
        #self.deseq_samples=[]
        #for sample_name, category_name in self.parsing_post_obj.factors.items():
        #    self.deseq_samples.append(sample_name)
        #   self.factors.append(category_name)
        #self.batches= self.parsing_post_obj.batches.values()

# Overiding the method get_samples_list in run_ngs
    def get_samples_list(self, path): #calling the function in general
            return  get_samples_names(path)


    def create_config_file(self):
        with open(self.config_file, 'w') as conf_f:
            conf_f.writelines('---\n')
            conf_f.writelines('pipeline: "' + self.pipeline_name_ + '"\n')
            conf_f.writelines('run_id: "' + self.date + '"\n')
            conf_f.writelines('job_name: "' + self.job_name + '"\n')
            conf_f.writelines('input_file: ' + self.input_dir + '\n')
            conf_f.writelines('output_dir: ' + self.output_dir + '\n')
            conf_f.writelines('report_dir_name: ' + self.report_dir_name + '\n')
            conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
            conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
            conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
            conf_f.writelines('Rscript: ' + RSCRIPT + '\n')
            conf_f.writelines('R_lib_paths: ' + R_LIB_PATHS + '\n')
            conf_f.writelines('conda_root: ' + CONDA_ROOT + '\n')
            #conf_f.writelines('factors: ' + self.factors + '\n')
            #conf_f.writelines('deseq_samples: ' + self.deseq_samples + '\n')
            conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')
            conf_f.writelines('correct_with_FDR_tool: "' + str(self.clean_data_form.correct_with_FDR_tool) + '"\n')
            conf_f.writelines('adjusted_P_value: "' + str(self.clean_data_form.adjusted_P_value) + '"\n')
            conf_f.writelines('baseMean: "' + str(self.clean_data_form.baseMean) + '"\n')
            conf_f.writelines('log2_fold_change: "' + str(self.clean_data_form.log2_Fold_Change) + '"\n')
            if self.clean_data_form.intermine_genome:
                conf_f.writelines('gene_db: ' + self.clean_data_form.intermine_genome.gene_db_url + '\n')
                conf_f.writelines('intermine_web_query: ' + self.clean_data_form.intermine_genome.interMine_web_query + '\n')
                conf_f.writelines('intermine_web_base: ' + self.clean_data_form.intermine_genome.intermine_web_base + '\n')
                conf_f.writelines('intermine_web_creature: ' + self.clean_data_form.intermine_genome.interMine_creature + '\n')
            conf_f.writelines('yaml_file: ' + self.params_yaml_file + '\n')
            if self.parsing_post_obj.factors:
                conf_f.writelines('factors_file: ' + self.factors_file + '\n')
                self.parsing_post_obj.create_factors_file()
                # self.create_all_samples_info()



    # def create_all_samples_info(self):
    #     all_samples = open("all_samples_file.txt", 'w')
    #     all_samples.writelines('\t'.join(['All samples','DESeq_sample', 'condition', 'batches']) + '\n')
    #     sample = '\t'+'\n'.join(self.samples)
    #     deseq_samples= '\t'+'\n'.join(self.parsing_post_obj.factors.keys())
    #     factors =  '\t'+'\n'.join(self.parsing_post_obj.factors.values()
    #     all_samples.append(sample)
    #     all_samples.append(deseq_samples)
    #     all_samples.append(factors)
    #
    #     if self.parsing_post_obj.batches:
    #         batches=  '\t'+'\n'.join(self.parsing_post_obj.batches.values())
    #         all_samples.append(batches)
    #     all_samples.close()






    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in  DESeqFromCountsMatrixForm(self.username).fields:
            if 'form_id' in DESeqFromCountsMatrixForm(self.username).fields[key].widget.attrs and \
                    DESeqFromCountsMatrixForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in DESeqFromCountsMatrixForm(self.username).fields[key].widget.attrs and \
                    DESeqFromCountsMatrixForm(self.username).fields[key].widget.attrs['form_id'] == 'basic':
                self.basic[key] = self.post_params[key]
        self.basic['Samples'] = self.samples
        if self.parsing_post_obj.factors:
             self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
        if self.parsing_post_obj.batches:
             self.basic['Batches'] = self.parsing_post_obj.batches
        self.relevant_parameters['advanced_parameters'] = self.advanced
        self.relevant_parameters['basic_parameters'] = self.basic


    def specific_steps_to_run(self):
        return 'rule_1_report'




