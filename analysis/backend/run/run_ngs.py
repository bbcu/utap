"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os
from abc import ABCMeta, abstractmethod

from analysis.backend.run.run_base import RunAnalysis
from analysis.backend.settings import RESULTS_DIR, RUN_LOCAL, RUN_LOCAL_HOST_CONDA, MAX_CORES
from analysis.backend.utils.general import samples_in_fastq_dir
from analysis.backend.utils.snakemake import Snakemake
from analysis.backend.utils.users import get_user_dir
from analysis.backend.views.views_general import remote_samples_list_ajax

logger = logging.getLogger(__name__)


class RunAnalysisNGS(RunAnalysis):
    __metaclass__ = ABCMeta

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        # input_dir = os.path.abspath(os.path.join(get_user_dir(username), self.clean_data_form.input_folder))
        # self.update_kwargs_if_not_exists(kwargs, input_dir=input_dir)
        super(RunAnalysisNGS, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                             params_yaml_file, params_json_file, post_params, date, output_dir,
                                             **kwargs)
        self.input_dir = os.path.abspath(os.path.join(get_user_dir(username), self.clean_data_form.input_folder))
        self.samples = self.get_samples_list(self.input_dir)
        self.samples_num = len(self.samples)

    def get_prj_dir(self):
        return os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.output_folder))

    @abstractmethod
    def save_relevant_parameters(self):
        pass

    def send(self):
        self.run_commands()

    @abstractmethod
    def create_config_file(self):
        pass

    @abstractmethod
    def specific_steps_to_run(self):
        pass

    def get_samples_list(self, root_dir):
        if not os.path.isdir(root_dir):  # in submitted
            return remote_samples_list_ajax(self.request, list_format=True, post=True)
        else:
            return samples_in_fastq_dir(root_dir, self.pipeline)[0]

    def run_commands(self):
        os.system('mkdir -p %s' % self.output_dir)
        os.system('ln -s %s %s' % (self.results_dir, os.path.join(RESULTS_DIR, self.job_name)))

        self.create_config_file()
        snakefile_name = os.path.join(self.output_dir, 'snakefile_%s' % self.date)
        if RUN_LOCAL:
            snakemake = Snakemake(self.job_name, self.pipeline, self.date, snakefile_name, self.queue, self.config_file, num_jobs=None,
                                  num_cores=MAX_CORES, cluster=False)
        else:
            snakemake = Snakemake(self.job_name, self.pipeline, self.date, snakefile_name, self.queue, self.config_file,
                                  self.samples_num)
        snakemake.copy_snakefile(self.snakefile_source)
        snakemake.prepare_cmd_file(allowed_step=self.specific_steps_to_run())
        if RUN_LOCAL and not RUN_LOCAL_HOST_CONDA:
            snakemake.run_snakefile()
        else:
            snakemake.run_snakefile_ssh()
