"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging
import re
from abc import ABCMeta, abstractmethod
from string import Template

from analysis.backend.run.run_base import RunAnalysis
from analysis.backend.settings import NEXTSEQ_SERVER, NEXTSEQ_USER, BCL_REMOTE_PATH, BCL_REMOTE_PATH_INCPM, \
    BCL_REMOTE_PATH_INCPM_NOVA, DEMULTIPLEXING_SCRIPT, PARAMETERS_DIR, RUN_LOCAL, RUN_LOCAL_HOST_CONDA, \
    BCL2FASTQ_EXE, PIPELINE_URL, FASTQ_MULTX, JAVA_EXE, FASTQC_EXE, MULTIQC_EXE
from analysis.backend.utils.bcl2fastq import RunParametersReader, CreateSampleSheet
from analysis.backend.utils.consts import DemultConst, DimultiplexBCL, DimultiplexFASTQ
from analysis.backend.utils.general import check_samplesheet
from analysis.backend.utils.snakemake import Snakemake

logger = logging.getLogger(__name__)


class RunDemultiplexing(RunAnalysis):
    __metaclass__ = ABCMeta

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunDemultiplexing, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                params_yaml_file, params_json_file, post_params, date, output_dir,
                                                **kwargs)
        self.results_link_ = "%s/demultiplexing_results/" % (PIPELINE_URL)
        # self.protocol = self.clean_data_form.protocol
        self.sample_sheet_file = os.path.join(self.user_commands_dir, 'SampleSheet_' + self.job_name)
        # self.clean_samplesheet = []
        self.clean_samplesheet = {}
        self.pipeline_commands = []
        if not output_dir:  # for run again in the same output_dir - for now always the class not get output folder for demultiplexing pipelines
            self.output_dir = os.path.join(self.prj_dir, self.job_name + '_Demultiplexing')

    @abstractmethod
    def get_prj_dir(self):
        pass

    @abstractmethod
    def save_relevant_parameters(self):
        pass

    @abstractmethod
    def demultiplexing_commands(self):
        pass  # return response (raise Exception or return stdout etc.)

    def send(self):
        self.edit_samplesheet_form()
        self.demultiplexing_commands()
        self.run_commands()

    def edit_samplesheet_form(self):
        self.clean_samplesheet,error_massage=check_samplesheet(self.clean_data_form.samplesheet.split(','))
        if not self.clean_samplesheet and error_massage:
            raise Exception(error_massage)


        # error_massage = 'The SampleSheet of run %s is not corrected: missing barcodes or sample name.' % self.job_name
        # splited_ss = self.clean_data_form.samplesheet.split(',')
        # logger.info('this is the label:'+ str(self.request.GET.keys())+','+ str(self.request.POST['samplesheet']))
        # for key, value in self.form.items():
        #     logger.info('key:'+ key)
        #     logger.info('key:' + value)
        # for samp, barc in zip(splited_ss[::2], splited_ss[1::2]):
        #     if samp and barc:
        #         self.clean_samplesheet.append([samp, barc])
                # illegal_characters = [i for i in
                #                       [" ", "?", "(", ")", "[", "]", ".", "/", "\\", "=", "+", "<", ">", ":", ";", "\"",
                #                        "'", "*", "^", "|", "&"] if i in samp]
                # if illegal_characters:
                #     illegal_characters_error = "The sample name %s contains illegal character/s [%s]" % (
                #         samp, ','.join(illegal_characters))
                #     logger.error(illegal_characters_error)
                #     raise Exception(illegal_characters_error)
                # reg = re.compile('^[AGCT]+$')
        #         if not reg.match(barc):
        #             illegal_bar_error = "The barcode of sample %s contains illegal base/s. Barcode can contains only [AGCT] characters" % (
        #                 samp)
        #             logger.error(illegal_bar_error)
        #             raise Exception(illegal_bar_error)
        #     elif not (samp and barc):
        #         continue
        #     else:
        #         logger.error(error_massage)
        #         raise Exception(error_massage)
        # if not self.clean_samplesheet:
        #     logger.error(error_massage)
        #     raise Exception(error_massage)

    def run_commands(self):
        snakefile_name = os.path.join(self.user_commands_dir, 'snakefile_%s' % self.job_name)
        if RUN_LOCAL:
            snakemake = Snakemake(self.job_name, self.pipeline, self.date, snakefile_name, self.queue, config_file=None, num_jobs=None,
                                  num_cores=1, cluster=False)
        else:
            snakemake = Snakemake(self.job_name, self.pipeline, self.date, snakefile_name, self.queue)
        snakemake.create_snakefile(self.pipeline_commands, self.wildcards)
        snakemake.prepare_cmd_file()
        if RUN_LOCAL and not RUN_LOCAL_HOST_CONDA:
            snakemake.run_snakefile()
        else:
            snakemake.run_snakefile_ssh()

    def create_samplesheet_file(self, ss_format):
        SampleSheet = CreateSampleSheet(self.sample_sheet_file, ss_format, self.clean_samplesheet)
        #SampleSheet.validate_samplesheet()
        SampleSheet.write_samplesheet()



    def format_rule(self, rule, params, dual_input, dual_ouput):
        logger.info("inside format_rule")
        logger.info(rule)
        formated_rule=[]
        if dual_input:
            rule[0] = rule[0].replace("{strand_num}","1") + ',\n        ' + rule[0].replace("{strand_num}","2")
        else:
            rule[0] = rule[0].replace("{strand_num}","1")
        if dual_ouput:
            logger.info(rule[1])
            rule[1] = rule[1].replace("{strand_num}", "1") + ',\n        ' + rule[1].replace("{strand_num}", "2")
        else:
            rule[1] = rule[1].replace("{strand_num}", "1")
        for rule_atrr in rule:
            if type(rule_atrr) == str:
                formated_rule.append(rule_atrr.format(exe=params["exe"], bases_mask =params["param"],num=params["num"],folder_num=params["folder"], prev_folder=params["folder"]-1,
                                output=params["output_dir"],input=params["input_dir"], command_dir= params["command_dir"],
                                   sample_sheet= params["sample_sheet"], sample=params["sample"], SAMPLES=params["sample"][1:-1].upper()+"S", _sample_=params["sample"][1:-1] , UMI=params["UMI"],barcode= params["barcode"]))
            else:
                formated_rule.append(rule_atrr)
            logger.info(rule_atrr)
        logger.info(formated_rule)
        self.pipeline_commands.append(formated_rule)




    def fastQC(self,rule_params, dual_index):
        #add fastqc rule
        self.rule_params["output_dir"] = self.output_dir
        self.rule_params["folder"] +=1
        self.rule_params["exe"] =FASTQC_EXE + " -j " + JAVA_EXE
        self.format_rule(DemultConst().get_fastqc_atrr(),rule_params,dual_index,dual_index)
        #add multiqc rule
        self.rule_params["folder"] +=1
        self.rule_params["exe"] =MULTIQC_EXE
        self.format_rule(DemultConst().get_multiqc_atrr(), rule_params, dual_index, False)


class RunDemultiplexingFastq(RunDemultiplexing):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        # fastq_r1 = self.clean_data_form.fastq_file_R1
        # fastq_r2 = self.clean_data_form.fastq_file_R2  # if self.clean_data_form.fastq_file_R2 else "none"
        # fastq_I = self.clean_data_form.fastq_file_I
        # self.update_kwargs_if_not_exists(kwargs, fastq_r1=fastq_r1, fastq_r2=fastq_r2, fastq_I=fastq_I)
        super(RunDemultiplexingFastq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                     params_yaml_file, params_json_file, post_params, date, output_dir,
                                                     **kwargs)
        # if 'fastq_file_R1' in self.clean_data_form.__dict__:
        #     self.fastq_r1 = self.clean_data_form.fastq_file_R1
        # # if self.clean_data_form.fastq_file_R2 else "none"
        # if 'fastq_file_R2' in self.clean_data_form.__dict__:
        #     self.fastq_r2 = self.clean_data_form.fastq_file_R2
        # if 'fastq_file_I' in self.clean_data_form.__dict__:
        #     self.fastq_I = self.clean_data_form.fastq_file_I

        self.rule_atrr=[]
        self.rule_params={}
        self.pools={}
        self.fastq_commands = []
        self.wildcards={}
        if hasattr(self.clean_data_form, 'protocol'):
            self.protocol = self.clean_data_form.protocol
        else:
            self.protocol = ""
        if 'fastq_folder' in self.clean_data_form.__dict__:
            self.fastq_folder = self.clean_data_form.fastq_folder
        else:
            self.fastq_folder = self.output_dir

    def get_prj_dir(self):
        return os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.output_folder))

    def modify_fastq(self):
        self.modify_fastq_atrr = DimultiplexFASTQ().get_modify_fastq()
        self.rule_params["UMI"] = self.clean_data_form.UMI_length
        self.format_rule(self.modify_fastq_atrr, self.rule_params, self.dual_index and self.rule_atrr[0],
                         self.dual_index and self.rule_atrr[1])

    def save_relevant_parameters(self):
        self.relevant_parameters['fastq_folder'] = self.fastq_folder
        self.relevant_parameters['Output dir'] = self.output_dir
        self.relevant_parameters['SampleSheet file'] = self.sample_sheet_file

    def demultiplexing_fastq_cmd(self):
        self.wildcards['SAMPLES'] = self.clean_samplesheet['Sample']
        self.barcode_len= len(self.clean_samplesheet['Index'][0])
        self.dual_index = True if 'Index2' in self.clean_samplesheet.keys() else False
        self.samples = ' '.join([samp_barc[0] for samp_barc in self.clean_samplesheet])
        self.rule_params = {"exe": FASTQ_MULTX, "param": "", "folder": 1,
                            "output_dir": self.output_dir, "input_dir": self.fastq_folder,  "num":"",
                            "command_dir": self.user_commands_dir, "sample_sheet": self.sample_sheet_file,
                            "sample": "{sample}", "UMI":"", "barcode":str(self.barcode_len)}

        if self.protocol == DemultConst.SCRAB_SEQ:
            self.pools = self.clean_data_form.pool
            if self.pools:
                self.rule_atrr = DimultiplexFASTQ().get_scrb_pool_atrr()
                self.rule_params["folder"] +=2
                self.format_rule(self.rule_atrr, self.rule_params, self.dual_index and self.rule_atrr[0],
                                 self.dual_index and self.rule_atrr[1])
            # else:
            #     self.pipeline_commands= self.pipeline_commands[:-1]
            #     self.rule_params["folder"] -= 1
            #self.rule_params["folder"] += 1
            self.modify_fastq()
            # self.modify_fastq_atrr = DimultiplexFASTQ().get_modify_fastq()
            # self.rule_params["UMI"] = self.clean_data_form.UMI_length
            # self.format_rule(self.modify_fastq_atrr, self.rule_params, self.dual_index and self.rule_atrr[0],
            #                  self.dual_index and self.rule_atrr[1])


        else:
            self.rule_atrr=DimultiplexFASTQ().get_rule_atrr()
            self.format_rule(self.rule_atrr, self.rule_params, False,
                             self.dual_index and self.rule_atrr[1])
        logger.info("after fastq dimpultiplexing")
        self.fastQC(self.rule_params, self.dual_index)

    def demultiplexing_commands(self):
        self.create_samplesheet_file(ss_format=DemultConst.FASTQ_V)
        self.demultiplexing_fastq_cmd()
        logger.info("after eerything")


class RunDemultiplexingBcl(RunDemultiplexingFastq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        # bcl_dir = os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.bcl_files))
        # runinfo_file = os.path.join(bcl_dir, 'RunInfo.xml')
        # self.update_kwargs_if_not_exists(kwargs, bcl_dir=bcl_dir, runinfo_file=runinfo_file)
        super(RunDemultiplexingBcl, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                   params_yaml_file, params_json_file, post_params, date, output_dir,
                                                  **kwargs)


        self.protocol = self.clean_data_form.protocol
        self.delete_bcl=self.clean_data_form.bcl_delete
        if 'bcl_files' in self.clean_data_form.__dict__:
            self.bcl_dir = os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.bcl_files))
            self.runinfo_file_ = os.path.join(self.bcl_dir, 'RunInfo.xml')

    def get_prj_dir(self):
        return os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.output_folder))

    def get_reads_type(self):
        reads_details = RunParametersReader().parse(self.runinfo_file_)
        reads_type = []
        for i, read in enumerate(reads_details):
            reads_type.append('i*' if read.is_indexed else 'y*')
        return reads_type, reads_details

    def save_relevant_parameters(self):
        self.relevant_parameters['Bcl dir'] = self.bcl_dir
        self.relevant_parameters['Output dir'] = self.output_dir
        self.relevant_parameters['Protocol'] = self.clean_data_form.protocol
        self.relevant_parameters['SampleSheet file'] = self.sample_sheet_file

    def edit_pool_samplesheet_form(self):
        self.clean_pool_samplesheet, error_massage = check_samplesheet(self.clean_data_form.pool.split(','))
        logger.info(self.clean_pool_samplesheet)
        if not self.clean_pool_samplesheet and error_massage:
            raise Exception(error_massage)
        CreateSampleSheet(self.sample_sheet_file, DemultConst.BCL_V, self.clean_pool_samplesheet).write_samplesheet()
        logger.info(self.clean_pool_samplesheet)



    def bcl2fastq_cmd(self, demultiplexing):
        reads_type,read_details = self.get_reads_type()
        reads= (",").join(reads_type)
        self.barcode_len = len(self.clean_samplesheet['Index'][0])
        self.rule_params = {"exe": BCL2FASTQ_EXE, "param": reads, "folder": 1, "num": 1,
                            "output_dir": self.output_dir, "input_dir": self.bcl_dir,
                            "command_dir": self.user_commands_dir, "sample_sheet": self.sample_sheet_file,
                            "sample": "{sample}", "UMI":"", "barcode":str(self.barcode_len)}
        self.wildcards = {'SAMPLES': self.clean_samplesheet['Sample']}
        self.dual_index = True if 'Index2' in self.clean_samplesheet.keys() else False
        if demultiplexing:
            if self.delete_bcl== "yes":
                self.rule_atrr = DimultiplexBCL().get_deleted_bcl()
            else:
                self.rule_atrr = DimultiplexBCL().get_rule_atrr()
            if self.protocol == DemultConst.MARS_SEQ:
                self.rule_params["param"]= "y*,i7y*"
            if self.protocol ==  DemultConst.SCRAB_SEQ:
                self.pools = self.clean_data_form.pool
                if self.pools:
                    self.clean_pool_samplesheet = {}
                    self.edit_pool_samplesheet_form()
                    self.rule_params["sample"]="{pool}"
                    self.rule_params["num"] = 8
                    self.dual_index = True if 'Index2' in self.clean_pool_samplesheet.keys() else False
                    self.wildcards = {'POOLS': self.clean_pool_samplesheet['Sample']}
                else:
                    self.rule_atrr = DimultiplexBCL().get_unzip_bcl_out()
                    self.modify_fastq()


        else:
            reads_type = self.get_reads_type()
            num_reads_no_index = reads_type.count('y*')
            num_reads_index = reads_type.count('i*')

            self.fastq_dir = os.path.join(self.prj_dir, self.job_name + '_bcl2fastq')
            self.fastq_r1 = os.path.join(self.fastq_dir, 'Undetermined_S0_R1_001.fastq.gz')
            self.fastq_I = os.path.join(self.fastq_dir, 'Undetermined_S0_I1_001.fastq.gz')
            self.fastq_r2 = "none" if num_reads_no_index == 1 else os.path.join(self.fastq_dir,
                                                                                'Undetermined_S0_R2_001.fastq.gz')
            if num_reads_index == 0:
                self.fastq_I = os.path.join(self.fastq_dir, 'Undetermined_S0_R2_001.fastq.gz')
                self.fastq_r2 = "none"

        self.format_rule(self.rule_atrr, self.rule_params,False,self.dual_index and self.rule_atrr[1])
        self.pipeline_commands = self.pipeline_commands[-1:] + self.pipeline_commands[:-1]
        self.fastQC(self.rule_params,self.dual_index)


    def demultiplexing_bcl_cmd(self):
        self.bcl2fastq_cmd(demultiplexing=True)
        if  self.protocol ==  DemultConst.SCRAB_SEQ and self.pools:
            super(RunDemultiplexingBcl, self).demultiplexing_commands()
        else:
            self.create_samplesheet_file(ss_format=DemultConst.BCL_V)

    def demultiplexing_commands(self):
        self.demultiplexing_bcl_cmd()


class RunDemultiplexingRunid(RunDemultiplexingBcl):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunDemultiplexingRunid, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                     params_yaml_file, params_json_file, post_params, date, output_dir,
                                                     **kwargs)
        self.bcl_dir = os.path.join(self.user_base_dir, self.run_id_ + '_' + self.job_name,
                                    self.run_id_ + '_copied_bcl')
        bcl_remote_path = BCL_REMOTE_PATH
        if 'NB501540' in self.run_id_:
            bcl_remote_path = BCL_REMOTE_PATH_INCPM 
        elif 'A00929' in self.run_id_:
            bcl_remote_path = BCL_REMOTE_PATH_INCPM_NOVA
        self.bcl_remote_dir = os.path.join(bcl_remote_path, self.run_id_)  # On stefan server
        os.system('mkdir -p %s' % PARAMETERS_DIR)
        self.runinfo_file_ = os.path.join(PARAMETERS_DIR, 'RunInfo_%s.xml' % self.job_name)
        #self.runinfo_file_ = os.path.join(self.user_commands_dir, 'RunInfo_%s.xml' % self.job_name)
        logger.info("before scp")
        copy_cmd = 'scp %s@%s:%s %s ' % (
            NEXTSEQ_USER, NEXTSEQ_SERVER, os.path.join(self.bcl_remote_dir, 'RunInfo.xml'), self.runinfo_file_)
        logger.info(copy_cmd)
        os.system(copy_cmd)

    def get_prj_dir(self):
        self.run_id_ = self.clean_data_form.run_id.strip()  # before the calling to parent
        return os.path.abspath(
            os.path.join(self.user_base_dir, self.clean_data_form.output_folder, self.run_id_ + '_' + self.job_name))

    def save_relevant_parameters(self):
        self.relevant_parameters['Run id'] = self.run_id_
        self.relevant_parameters['Output dir'] = self.output_dir
        self.relevant_parameters['Protocol'] = self.clean_data_form.protocol
        self.relevant_parameters['SampleSheet file'] = self.sample_sheet_file

    def copy_bcl_files_cmd(self):
        scp_bcl_cmd = "mkdir -p %s && rsync -av %s@%s:%s/* %s " % (self.bcl_dir, NEXTSEQ_USER, NEXTSEQ_SERVER, self.bcl_remote_dir, self.bcl_dir)
        logger.info(scp_bcl_cmd)
        self.pipeline_commands.append(["",'"'+ self.bcl_dir +'"',"",scp_bcl_cmd,"",'RunDemultiplexingRunid', 1,""])

    def demultiplexing_commands(self):
        self.demultiplexing_bcl_cmd()
        self.copy_bcl_files_cmd()
        self.pipeline_commands = self.pipeline_commands[-1:] + self.pipeline_commands[:-1]

