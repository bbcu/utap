"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging

from analysis.backend.forms.forms_chipseq import ChipSeqForm
from analysis.backend.run.parsing_post import ParsingPost
from analysis.backend.run.run_ngs import RunAnalysisNGS
from analysis.backend.utils.consts import CHIPSEQ_ANALYSIS_TYPE
from analysis.backend.settings import SNAKEFILE_CHIPSEQ, PIPELINE_URL_HTTP, RESULTS_URL, JAVA_EXE, MULTIQC_EXE, BOWTIE2_EXE, \
    PICARD_EXE, IGVTOOLS_EXE, MACS2_EXE, BEDTOOLS_EXE, SNAKEFILE_SCRIPTS, SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, \
    RSCRIPT, R_LIB_PATHS, CUTADAPT_EXE, FASTQC_EXE, SAMTOOLS_EXE, NGS_PLOT_EXE, CONDA_ROOT, GS_EXE, MAX_CORES, \
    KENTUTILS_EXE, HOMER_EXE, PIPELINE_SERVER_PORT

# do we need GS_EXE and MAX_CORES
logger = logging.getLogger(__name__)


class RunChipSeq(RunAnalysisNGS):
    pipeline_name_ = CHIPSEQ_ANALYSIS_TYPE

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunChipSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)

        self.snakefile_source = SNAKEFILE_CHIPSEQ
        if not self.output_dir:  # In case of Submitted analyses the class get output_dir as input
            self.output_dir = os.path.join(self.prj_dir, self.job_name + '_' + self.pipeline_name_)
        self.config_file = os.path.join(self.output_dir, 'config-chipseq-%s.yaml' % self.date)
        self.factors_file = None if self.clean_data_form.treat_vs_control == 'no' else os.path.join(
            self.output_dir, 'pheno_data-%s.tsv' % self.date)
        logger.info("Run ChIP-Seq pipeline with treat_vs_control ? %s" % self.clean_data_form.treat_vs_control)
        self.parsing_post_obj = ParsingPost(self.samples, self.factors_file, self.post_params_obj, self.pipeline)
        self.report_dir_name = self.parsing_post_obj.get_report_dir_name() if self.parsing_post_obj.get_report_dir_name() else  "_".join(self.job_name.split("_", 2)[2:])
        if PIPELINE_SERVER_PORT != 7000:
            self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        else:
            self.results_link_ = "%s/chipseq_results/" % (PIPELINE_URL_HTTP)
        self.results_dir = os.path.join(self.output_dir, '10_reports')
        if self.run_by_user == 'submit' and 'Submit' not in self.pipeline:  # run submitted
            if not os.path.isdir(self.input_dir):
                raise Exception('The copy of the run %s from stefan don\'t exists.' % self.clean_data_form.run_id)
        # self.copy_fastq_directory()
        self.genome = self.clean_data_form.genome.fasta
        self.username = username
        self.genome_index =self.clean_data_form.genome.path   # for all analyses the genome is actually the genome index, in the chipseq analyses we have to use the actuall genome and not the genome index, therfore in order to distinguish between genome and genome index we use for the actuall genome the name fasta
        self.adapters = self.clean_data_form.adapter_on_R1.lstrip()
        if hasattr(self.clean_data_form, 'adapter_on_R2'):
            self.adapters += "," + self.clean_data_form.adapter_on_R2.lstrip()
        self.chromosome_info = self.clean_data_form.chromosome_info.path
        #advanced parametres:
        #CUTADAPT
        self.additional_adapter_R1 = self.clean_data_form.aditional_adapter_on_R1
        self.additional_adapter_R2 = self.clean_data_form.aditional_adapter_on_R2
        self.cutadapt_qvalue = self.clean_data_form.cutadapt_qvalue
        self.cutadapt_min_len =self.clean_data_form.cutadapt_min_len
        self.cutadapt_cut_start = '' if str(self.clean_data_form.cutadapt_cut_start) == 'None' else self.clean_data_form.cutadapt_cut_start
        self.cutadapt_cut_end = '' if str(self.clean_data_form.cutadapt_cut_end) == 'None' else self.clean_data_form.cutadapt_cut_end
        #MACS2
        self.macs_qvalue = self.clean_data_form.macs_qvalue
        self.macs_bandwidth = self.clean_data_form.macs_bandwidth



    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in ChipSeqForm(self.username).fields:
            if 'form_id' in ChipSeqForm(self.username).fields[key].widget.attrs and \
                    ChipSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in ChipSeqForm(self.username).fields[key].widget.attrs and \
                    ChipSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'basic' and key != "genome":
                self.basic[key] = self.post_params[key]
        self.basic["genome"] = self.genome
        if self.parsing_post_obj.factors:
             self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
        self.relevant_parameters ['advanced_parameters']= self.advanced
        self.relevant_parameters['basic_parameters']= self.basic

        # self.relevant_parameters[''] = "basic parameters"
        # self.relevant_parameters['Input dir'] = self.input_dir
        # self.relevant_parameters['Output dir'] = self.output_dir
        # self.relevant_parameters['Genome'] = self.genome
        # self.relevant_parameters['Adapters'] = self.adapters
        # self.relevant_parameters['Genome Index'] = self.genome_index
        # self.advanced=[dict(zip(self.relevant_parameters[self.field],self.request.GET[self.field])) for self.field in self.request.GET if self.request.GET[self.field].widget.attrs['form_id'] and self.request.GET[self.field].widget.attrs['form_id'] == 'advanced']

    def create_config_file(self):
        with open(self.config_file, 'w') as conf_f:
            conf_f.writelines('---\n')
            conf_f.writelines('pipeline: "' + self.pipeline_name_ + '"\n')
            conf_f.writelines('run_id: "' + self.date + '"\n')
            conf_f.writelines('job_name: "' + self.job_name + '"\n')
            conf_f.writelines('fastq_dir: ' + self.input_dir + '\n')
            conf_f.writelines('report_dir_name: ' + self.report_dir_name + '\n')
            conf_f.writelines('output_dir: ' + self.output_dir + '\n')
            conf_f.writelines('report_link: ' + RESULTS_URL + '/' + self.job_name + '\n')
            conf_f.writelines('yaml_file: ' + self.params_yaml_file + '\n')
            conf_f.writelines('adaptor: ' + self.adapters + '\n')
            conf_f.writelines('genome: ' + self.genome + '\n')
            conf_f.writelines('genomeIndx: ' + self.genome_index + '\n')
            conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
            conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
            conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
            conf_f.writelines('Rscript: ' + RSCRIPT + '\n')
            conf_f.writelines('R_lib_paths: ' + R_LIB_PATHS + '\n')
            conf_f.writelines('conda_root: ' + CONDA_ROOT + '\n')
            conf_f.writelines('java: ' + JAVA_EXE + '\n')
            conf_f.writelines('cutadapt_exe: ' + CUTADAPT_EXE + '\n')
            conf_f.writelines('fastqc_exe: ' + FASTQC_EXE + '\n')
            conf_f.writelines('multiqc_exe: ' + MULTIQC_EXE + '\n')
            conf_f.writelines('bowtie2_exe: ' + BOWTIE2_EXE + '\n')
            conf_f.writelines('samtools_exe: ' + SAMTOOLS_EXE + '\n')
            conf_f.writelines('picard_exe: ' + PICARD_EXE + '\n')
            conf_f.writelines('igvtools_exe: ' + IGVTOOLS_EXE + '\n')
            conf_f.writelines('ngs_plot_exe: ' + NGS_PLOT_EXE + '\n')
            conf_f.writelines('gs_exe: ' + GS_EXE + '\n')
            conf_f.writelines('macs2_exe: ' + MACS2_EXE + '\n')
            conf_f.writelines('bedtools_exe: ' + BEDTOOLS_EXE + '\n')
            conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')
            conf_f.writelines('chr_info: ' + self.chromosome_info + '\n')
            conf_f.writelines('KentUtils_exe: ' + KENTUTILS_EXE + '\n')
            conf_f.writelines('homer_exe: ' + HOMER_EXE + '\n')
            conf_f.writelines('additional_Adapter_R1: ' + self.additional_adapter_R1 + '\n')
            conf_f.writelines('additional_Adapter_R2: ' + self.additional_adapter_R2 + '\n')
            conf_f.writelines('cutadapt_qvalue: "' + str(self.cutadapt_qvalue) + '"\n')
            conf_f.writelines('cutadapt_min_len: "' + str(self.cutadapt_min_len) + '"\n')
            conf_f.writelines('cutadapt_cut_start: "' + str(self.cutadapt_cut_start) + '"\n')
            conf_f.writelines('cutadapt_cut_end: "' + str(self.cutadapt_cut_end) + '"\n')
            conf_f.writelines('macs2_qvalue: "' + str(self.macs_qvalue) + '"\n')
            conf_f.writelines('macs2_bandwidth: "' + str(self.macs_bandwidth) + '"\n')
            if self.parsing_post_obj.factors:
                conf_f.writelines('treat_vs_control: ' + self.factors_file + '\n')
                self.parsing_post_obj.create_factors_file()

    def specific_steps_to_run(self):
        return ''


class SubmitChipSeq(RunChipSeq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunChipSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir,
                                         **kwargs)
        #self.results_link_ = "%s/no_results/" % (PIPELINE_URL_HTTP)
        #self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        if PIPELINE_SERVER_PORT != 7000:
            self.results_link_ = '%s/%s/%s_%s/report_Chromatin_pipelines.html' % (RESULTS_URL, self.job_name, self.report_dir_name, self.date)
        else:
            self.results_link_ = "%s/chipseq_results/" % (PIPELINE_URL_HTTP)

    # Overide the function of RunAnalysis class
    def send_analysis(self):
        try:
            self.save_relevant_parameters()
            self.create_yaml_parameters_file(self.params_yaml_file)
            json = create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params, self.user,
                                                       self.output_dir)
            logger.info("Parameters of the analysis are: " + json)
            return None, self.results_link_, self.output_dir
        except Exception as e:
            logger.error('Cannot submit analysis because Error: %s' % str(e))
            return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir
