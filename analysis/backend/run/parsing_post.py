"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import re
from collections import OrderedDict

logger = logging.getLogger(__name__)

MAX_CATEGORIES = 50  # Categories in transcriptome pipeline (max number of boxes)
MAX_BATCHES = 20  # Batches in transcriptome pipeline (max number of colors)
MAX_TREAT_VS_CONTROL = 50  # Must be even number. Boxes in chromatin pipelines (ATAC-seq and Chip-seq (max number of boxes - treatment and control together)


class ParsingPost(object):
    """Parse the post object and create pheno-data file from the html objects of boxes of the samples.
    """

    def __init__(self, samples, factors_file, post_params_obj, pipeline):
        logger.info(factors_file)
        self.samples = samples
        self.factors_file = factors_file
        self.post_params_obj = post_params_obj
        self.pipeline = pipeline
        self.factors = self.get_factors()
        self.batches = self.get_batches()

    def get_factors(self):
        if "Transcriptome" in self.pipeline or "DESeq2 from counts matrix" in self.pipeline:
            return self.get_factors_transcriptome()
        elif "ATAC" in self.pipeline or "ChIP" in self.pipeline:
            return self.get_factors_chromatin()

    def get_batches(self):
        if "Transcriptome" in self.pipeline or "DESeq2 from counts matrix" in self.pipeline:
            return self.get_batches_transcriptome()
        elif "ATAC" in self.pipeline or "ChIP" in self.pipeline:
            return {}

    def create_factors_file(self):
        if "Transcriptome" in self.pipeline or "DESeq2 from counts matrix" in self.pipeline:
            return self.create_factors_file_transcriptome()
        elif "ATAC" in self.pipeline or "ChIP" in self.pipeline:
            return self.create_factors_file_chromatin()

    def create_factors_file_chromatin(self):
        fact_f = open(self.factors_file, 'w')
        fact_f.writelines('\t'.join(["#group_number", "sample_name", "treatment_or_control"]) + '\n')
        for (group_num, sample_name, treat_or_control) in self.factors.values():
            fact_f.writelines('\t'.join([str(group_num), sample_name, treat_or_control]) + '\n')
        fact_f.close()

    def get_factors_chromatin(self):
        if not self.factors_file:
            return {}
        factors = OrderedDict()
        i = 0
        for box_number in xrange(1, MAX_TREAT_VS_CONTROL, 2):
            if hasattr(self.post_params_obj, 'to_' + str(box_number)):
                samples_indexes_treat = self.get_samples_indexed_from_box(box_number)
                samples_indexes_control = self.get_samples_indexed_from_box(box_number + 1)
                for sample_index in samples_indexes_treat:
                    sample_name = self.samples[int(sample_index)]
                    factors[i] = ((box_number+1)/2, sample_name, 'treatment')
                    i += 1
                for sample_index in samples_indexes_control:
                    sample_name = self.samples[int(sample_index)]
                    factors[i] = ((box_number+1)/2, sample_name, 'control')
                    i += 1
        logger.info("The treatment vs. control is: %s" % factors.items())
        return factors

    def get_samples_indexed_from_box(self, box_number):
        samples_indexes = []
        raw_samples_indexes = getattr(self.post_params_obj, 'to_' + str(box_number))
        if type(raw_samples_indexes) is list:  # post with json file
            if len(raw_samples_indexes) == 1 and "[" in raw_samples_indexes[0]:  # post with submit
                samples_indexes = raw_samples_indexes[0][1:-1].replace(' ', '').replace('u', '').replace('\'',
                                                                                                         '').split(
                    ',')
            else:
                samples_indexes = raw_samples_indexes
        elif isinstance(raw_samples_indexes, unicode):  # post by browser and only one sample in category
            samples_indexes = [raw_samples_indexes]
        return samples_indexes

    def create_factors_file_transcriptome(self):
        fact_f = open(self.factors_file, 'w')
        for sample_name, category_name in self.factors.items():
            if not self.batches:
                fact_f.writelines('\t'.join([sample_name, category_name]) + '\n')
            else:
                batch = self.batches[sample_name] if sample_name in self.batches else 'Batch-' + str(MAX_BATCHES + 1)
                fact_f.writelines('\t'.join([sample_name, category_name, batch]) + '\n')
        fact_f.close()

    def get_factors_transcriptome(self):
        if not self.factors_file:
            return {}
        factors = OrderedDict()
        for i in xrange(1, MAX_CATEGORIES):
            logger.info('this i: %s',i)
            logger.info("this is post params: %s",self.post_params_obj)
            if hasattr(self.post_params_obj, 'to_' + str(i)):
                category_name = getattr(self.post_params_obj, 'category_name_' + str(i))
                logger.info('category name print:'+category_name)
                samples_indexes = self.get_samples_indexed_from_box(i)
                for sample_index in samples_indexes:
                    logger.info("sample index print: %s", sample_index)
                    sample_name = self.samples[int(sample_index)]
                    factors[sample_name] = category_name
        logger.info(factors)
        return factors

    def get_batches_transcriptome(self):
        batches = {}
        for i in xrange(1, MAX_BATCHES):
            if hasattr(self.post_params_obj, 'batch' + str(i) + '-samples'):
                samples_indexes = getattr(self.post_params_obj, 'batch' + str(i) + '-samples')
                samples_indexes = samples_indexes.split('_')
                for sample_index in samples_indexes:
                    if sample_index:  # not ''
                        sample_name = self.samples[int(sample_index)]
                        if sample_name in self.factors:
                            batches[sample_name] = 'Batch-' + str(i)
        return batches

    @staticmethod
    def handle_post_params(post_params):
        post_params = {k: re.sub('[^0-9a-zA-Z\.\-@+_/]+', '_', v[0].rstrip('\n')) if 'to_' not in k and len(v) == 1 else v for k, v
                       in post_params.lists()}
        for i in xrange(1, MAX_CATEGORIES + 1):  # delete boxes without content
            if 'to_' + str(i) not in post_params.keys():
                if 'category_name_' + str(i) in post_params.keys():
                    del post_params['category_name_' + str(i)]

        for i in xrange(1, MAX_TREAT_VS_CONTROL + 1):
            if 'to_' + str(i) not in post_params.keys():
                if 'treat_name_' + str(i) in post_params.keys():
                    del post_params['treat_name_' + str(i)]
                if 'control_name_' + str(i) in post_params.keys():
                    del post_params['control_name_' + str(i)]
        return post_params



    def get_report_dir_name(self):
        if hasattr(self.post_params_obj, 'report_name'):
            report_name = getattr(self.post_params_obj, 'report_name')
            report_name_corrected= re.sub('[^0-9a-zA-Z-_]+', '_', report_name.strip())
        else:
            report_name_corrected =""
        return report_name_corrected

    def get_UMI_len(self):
        if hasattr(self.post_params_obj, 'UMI_barcode_length'):
            UMI_len = getattr(self.post_params_obj, 'UMI_barcode_length')
            UMI_len_corrected= re.sub('[^0-9]+', '', UMI_len.strip())
        else:
            UMI_len_corrected =""
        return UMI_len_corrected
# path = os.path.join(r'Z:\jordanal\UTAP\Rscripts\report_count_matrix/countsMatrix.txt')
# with open(path, 'r') as the_file:
#     line = the_file.readlines()[0].split()
# print (line)