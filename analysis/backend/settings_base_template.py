import os

PACKAGE_ROOT = os.path.dirname(os.path.dirname(__file__))  # utap/analysis
PROJECT_ROOT = os.path.abspath(os.path.join(PACKAGE_ROOT, os.pardir))  # utap
PROJECT_ENV = os.path.abspath(os.path.join(PROJECT_ROOT, os.pardir))  # ../utap
PROJECT_NAME = os.path.basename(PACKAGE_ROOT)

PIPELINE_OUTPUT_FOLDER = 'utap-output'

########################################################################################################################
# Don't change it, it is template for sed command (include the ' charcters)
########################################################################################################################
HOST_MOUNT=PROJECT_ROOT  #Must be real value, else the code couldn't compiled
PIPELINE_SERVER='PIPELINE_SERVER'
PROXY_URL='PROXY_URL'
USER_CLUSTER = 'USER_CLUSTER'
NOTIFICATION_INTERNAL_MAILING_LIST = ['NOTIFICATION_INTERNAL_MAILING_LIST_SED_TEMPLATE'] 
INSTITUTE_NAME = 'INSTITUTE_NAME' 
QUEUE_USERS='CLUSTER_QUEUE' 
CLUSTER_RESOURCES_PARAMS='CLUSTER_RESOURCES_PARAMS'
SQLITE_DB='DB_PATH'
SMTP_SERVER_NAME='MAIL_SERVER' 
PIPELINE_SERVER_PORT='PIPELINE_SERVER_PORT_SED' 
BIOUTILS_PACKAGE='BIOUTILS_PACKAGE' 
SNAKEFILE_PACKAGE='SNAKEFILE_PACKAGE' 
CONDA_ROOT='CONDA_ROOT'
RSCRIPT='RSCRIPT'
R_LIB_PATHS='R_LIB_PATHS' 
MAX_UPLOAD_SIZE='MAX_UPLOAD_SIZE'  # In Mb. for example : 2* 1024 *1024 = 2Gb
CLUSTER_TYPE='CLUSTER_TYPE'  #lsf, pbs, local

SNAKEMAKE_EXE='SNAKEMAKE_EXE'
GS_EXE='GS_EXE'
CLUSTER_EXE='CLUSTER_EXE' 
CUTADAPT_EXE='CUTADAPT_EXE' 
FASTQC_EXE='FASTQC_EXE' 
STAR_EXE='STAR_EXE' 
SAMTOOLS_EXE='SAMTOOLS_EXE' 
NGS_PLOT_EXE='NGS_PLOT_EXE' 
HTSEQ_COUNT_EXE='HTSEQ_COUNT_EXE' 
BCL2FASTQ_EXE='BCL2FASTQ_EXE'
CELLRANGER_EXE='CELLRANGER_EXE'
#For chromatin pipelines (ATAC-Seq and CHIP-seq) - for now not installed inside the docker:
JAVA_EXE='JAVA_EXE'
MULTIQC_EXE='MULTIQC_EXE'
BOWTIE1_EXE='BOWTIE1_EXE'
BOWTIE2_EXE='BOWTIE2_EXE'
PICARD_EXE='PICARD_EXE'
IGVTOOLS_EXE='IGVTOOLS_EXE'
MACS2_EXE='MACS2_EXE'
BEDTOOLS_EXE='BEDTOOLS_EXE'
TOPHAT_EXE='TOPHAT_EXE'
MAX_CORES='MAX_CORES' # Maximum cores on the local host or on the nodes of the cluster
#==ATAC-Seq pipeline
RUN_LOCAL=True #Run inside the docker
RUN_LOCAL_HOST_CONDA=True #Run commands without cluster, but by ssh on the host.
DEMO_SITE=False
#==CHIP-seq pipeline # run insdie the docker
NGS_PLOT_EXE='NGS_PLOT_EXE'
KENTUTILS_EXE='KENTUTILS_EXE'
HOMER_EXE='HOMER_EXE'
PEAKSPLITTER_EXE='PEAKSPLITTER_EXE'
#==Ribo-seq pipeline
PYTHON_EXE = 'PYTHON_EXE'
JRE_EXE= 'JRE_EXE'
MACS_EXE= 'MACS_EXE'
IGVTOOLS_EXE_FOR_RIBOSEQ='IGVTOOLS_EXE_FOR_RIBOSEQ'
########################################################################################################################

if PROXY_URL != 'None':
    PIPELINE_URL = PROXY_URL
else:
    PIPELINE_URL = PIPELINE_SERVER + ":" + str(PIPELINE_SERVER_PORT)

PIPELINE_URL_HTTP = PIPELINE_URL if PIPELINE_URL.startswith('http') else 'http://'+PIPELINE_URL
RESULTS_URL = PIPELINE_URL_HTTP + '/reports'



################################################
# RSCRIPT = '/apps/RH7U2/gnu/R/3.4.2/bin/Rscript'
# R_LIB_PATHS = '/apps/RH7U2/scripts/bbcu-R-packages'
# SNAKEFILE_PACKAGE = '/home/labs/bioservices/services/test_env/python27-ve-ngs-snakemakeTest'
# BIOUTILS_PACKAGE = '/apps/RH7U2/scripts/bbcu-python-packages/ve-2.7-bioutils/'
# HOST_MOUNT = '/home/labs/bioservices/services/test_env/'
# PIPELINE_SERVER = 'ngspipe.wexac.weizmann.ac.il'
# NOTIFICATION_INTERNAL_MAILING_LIST = ['refael.kohen@weizmann.ac.il']
# INSTITUTE_NAME = 'BBCU'
# QUEUE_USERS = 'bio'
# SQLITE_DB = os.path.join(HOST_MOUNT, 'db.sqlite3')
# PIPELINE_SERVER_PORT = 8000
# SMTP_SERVER_NAME = 'localhost'
# RESULTS_URL = 'http://' + PIPELINE_SERVER + '/\runs'
##############################################

LAB_DIR = os.path.join(HOST_MOUNT, PIPELINE_OUTPUT_FOLDER) # Output of the pipeline

SMTP_SERVER_PORT = 25

DEFAULT_LOG_DIR = os.path.join(HOST_MOUNT, 'logs-utap')
PARAMETERS_DIR = os.path.join(HOST_MOUNT, 'parameters_files')
JOBS_STATUS_FILE = os.path.join(HOST_MOUNT, 'jobs_status.txt')
RESULTS_DIR = os.path.join(HOST_MOUNT, 'reports') # Links to reports - DocumentsDir of apache




DEMULTIPLEXING_SCRIPT = os.path.join(BIOUTILS_PACKAGE, 'bin/demultiplexing-fastq.py')
FASTQ_MULTX = os.path.join('/home/labs/bioservices/services/miniconda2/envs/utap/bin/fastq-multx')
SNAKEFILE_SCRIPTS = os.path.join(SNAKEFILE_PACKAGE, 'bin')
SNAKEFILE_TAMPLATES = os.path.join(SNAKEFILE_PACKAGE, 'lib/python2.7/site-packages/ngs-snakemake')
SNAKEFILE_MARSSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-marseq.txt')
SNAKEFILE_RNASEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-rnaseq.txt')
SNAKEFILE_SINGLECELL = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-singlecell.txt')
SNAKEFILE_ATACSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-atacseq.txt')
SNAKEFILE_CHIPSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-chipseq.txt')
SNAKEFILE_RIBOSEQ = os.path.join(SNAKEFILE_TAMPLATES, 'snakefiles', 'snakefile-riboseq.txt')
SNAKEFILE_DESEQ_FROM_COUNTS_MATRIX = os.path.join(SNAKEFILE_TAMPLATES,'snakefiles', 'snakefile-deseq-from-counts-matrix.txt')
SNAKEFILE_RNASEQ_WITH_UMI= os.path.join(SNAKEFILE_TAMPLATES,'snakefiles', 'snakefile-rnaseq-with-umi.txt')
SNAKEFILE_PYTHON = os.path.join(SNAKEFILE_SCRIPTS, 'python')
SNAKEFILE_VERSION = os.path.join(SNAKEFILE_TAMPLATES, 'VERSION.txt')
with open(os.path.join(PROJECT_ROOT, "VERSION.txt"),"r") as f: UTAP_VERSION = f.read()


DEMULTIPLEXING_MEMORY = '3000'  # 3G per thread in snakemake in the parameter rusage[mem=3000] - don't include bare genome

# Snakemake repeats on run of jobs if it is failed (for example suspend by the cluster)
JOB_REPEATS_NUMBER = 3

#Mininum GB on disk for running the analysis. Under this value, the user will get warning.
MIN_DISK_FREE = 300 #300GB

