"""
This file is part of UTAP!.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging

from django import forms
from django.utils.translation import gettext as _
from django.utils.translation import ugettext_lazy as _

from analysis.backend.forms.forms_base import AnalysisBaseForm
#from analysis.backend.utils.consts import CHIPseqConst
from analysis.models import RiboSeqAnalysis

logger = logging.getLogger(__name__)

#why are we importing chipseq/atacseq again
from analysis.backend.utils.consts import BASIC_ID, ADVANCED_ID, RiboSeqConst,RiboSeqFields #,chip_fields


class RiboSeqForm(AnalysisBaseForm):
    commands = RiboSeqConst.COMMANDS
    def __init__(self, user_name, *args, **kwargs):
        #why are we inheriting from the same class we want these attributes to be first applied
        super(RiboSeqForm, self).__init__(user_name, *args, **kwargs)
        # FileBrowseField override the help_text from Meta class
        self.fields['input_folder'].help_text = _(
            'Folder with fastq files in appropriate structure (see help for details).')
        self.fields['input_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['input_folder'].required = True  # blank=true don't work in FileBrowseField
        # ChainedForeignKey override the genome and annotation form of the Meta class
        self.fields['genome'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['annotation'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.get_all_class_fields(RiboSeqFields())




    class Meta(AnalysisBaseForm.Meta):
        model = RiboSeqAnalysis
        fields = ['name', 'pipeline', 'input_folder', 'genome','annotation','output_folder', 'email']
