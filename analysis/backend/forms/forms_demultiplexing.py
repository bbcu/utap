"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging

from django import forms

from analysis.backend.forms.forms_base import AnalysisBaseForm
from analysis.backend.utils.consts import DemultConst, BASIC_ID
from analysis.models import DemultiplexingAnalysis

from django.utils.translation import gettext as _


logger = logging.getLogger(__name__)


class DemultiplexingForm(AnalysisBaseForm):
    def __init__(self, user_name, *args, **kwargs):
        super(DemultiplexingForm, self).__init__(user_name, *args, **kwargs)

    class Meta(AnalysisBaseForm.Meta):
        model = DemultiplexingAnalysis
        # fields = ['name', 'pipeline', 'fastq_file_R1', 'fastq_file_R2', 'fastq_file_I', 'bcl_files', 'run_id',
        #           'output_folder', 'email']
        fields = ['name', 'pipeline', 'fastq_folder', 'bcl_files', 'run_id','output_folder', 'email']
        localized_fields = '__all__'
        run_id_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 40}
        run_id_help = _('ID of the run on the Stefan server, for example: 170731_NB501465_0138_AH32N7BGX3. You can find it in the email that you got in the end of the sequencing.')
        AnalysisBaseForm.Meta.widgets['run_id'] = forms.TextInput(attrs=run_id_attr)
        AnalysisBaseForm.Meta.help_texts['run_id'] = run_id_help


    protocol = forms.ChoiceField(initial=DemultConst.PROTOCOL_INIT,
                                 choices=DemultConst.PROTOCOL_CHOICES,
                                 help_text=DemultConst.PROTOCOL_HELP,
                                 label=DemultConst.PROTOCOL_LABEL, widget=forms.Select(
            attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
    samplesheet = forms.CharField(required=True, label ="" ,initial='', widget=forms.HiddenInput())


class DemultiplexingFastqForm(DemultiplexingForm):
    def __init__(self, user_name, *args, **kwargs):
        super(DemultiplexingFastqForm, self).__init__(user_name, *args, **kwargs)
        del self.fields['bcl_files']
        del self.fields['run_id']
        self.fields['protocol'].choices =(("", _("")), (DemultConst.MARS_SEQ, _(DemultConst.MARS_SEQ)))
        self.fields['protocol'].initial=DemultConst.MARS_SEQ
        # FileBrowseField override the help_text from Meta class
        # self.fields['fastq_file_R1'].help_text = _(
        #     'Fastq file that contains the first read (Generally file name is *R1*)'),
        # self.fields['fastq_file_R2'].help_text = _(
        #     'Optional, Fastq file that contains the second read (Generally file name is *R2*)'),
        # self.fields['fastq_file_I'].help_text = _(
        #     'Fastq file that contains the barcodes (Generally file name is *I1*)'),
        # self.fields['fastq_file_R1'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        # self.fields['fastq_file_R2'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        # #self.fields['fastq_file_I'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        # self.fields['fastq_file_R1'].required = True  # blank = False don't work in FileBrowseField
        # self.fields['fastq_file_R2'].required = False
        #self.fields['fastq_file_I'].required = True
        self.fields['fastq_folder'].help_text = _(
             'Folder that contains the first read (Generally file name is *R1*) and optionaly, the seconde read (Generally file name is *R2*)'),
        self.fields['fastq_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'id': 'id_input_folder'}
        self.fields['fastq_folder'].required = True


class DemultiplexingBclForm(DemultiplexingForm):
    def __init__(self, user_name, *args, **kwargs):
        super(DemultiplexingBclForm, self).__init__(user_name, *args, **kwargs)
        # del self.fields['fastq_file_R1']
        # del self.fields['fastq_file_R2']
        # del self.fields['fastq_file_I']
        del self.fields['fastq_folder']
        del self.fields['run_id']
        # FileBrowseField override the help_text from Meta class
        self.fields['bcl_files'].help_text = _('Folder of the raw data of the sequencing in the original format'),
        self.fields['bcl_files'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['bcl_files'].required = True  # blank = False don't work in FileBrowseField
        self.fields['bcl_delete'] = forms.ChoiceField(required=True, initial= 'yes', choices=(('yes', _('yes')), ('no', _('no'))),
                                         help_text='BCL files are large and consume a lot of space therfore it\'s recomended to delete them after the demultiplexing',
                                         label='Delete BCL files',
                                         widget=forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
        self.fields['pool'] = forms.CharField(required=False, label="", initial='', widget=forms.HiddenInput())
        self.fields['UMI_length'] = forms.IntegerField(required=False, label="UMI length", help_text='Enter the UMI length', initial='', widget=  forms.NumberInput(attrs={'class': 'btn btn-default','form_id': BASIC_ID}))

class DemultiplexingRunidForm(DemultiplexingForm):
    def __init__(self, user_name, *args, **kwargs):
        super(DemultiplexingRunidForm, self).__init__(user_name, *args, **kwargs)
        del self.fields['bcl_files']
        del self.fields['fastq_folder']
        # del self.fields['fastq_file_R1']
        # del self.fields['fastq_file_R2']
        # del self.fields['fastq_file_I']
        self.fields['output_folder'].initial = self.user_dir
        self.fields['bcl_delete'] = forms.ChoiceField(required=True, initial= 'yes', choices=(('yes', _('yes')), ('no', _('no'))),
                                         help_text='BCL files are large and consume a lot of space therfore it\'s recomended to delete them after the demultiplexing',
                                         label='Delete BCL files',
                                         widget=forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
        self.fields['pool'] = forms.CharField(required=False, label="", initial='', widget=forms.HiddenInput())
        self.fields['UMI_length'] = forms.IntegerField(required=False, label="UMI length",
                                                       help_text='Enter the UMI length', initial='',
                                                       widget=forms.HiddenInput())
