"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os

from django import forms
from django.utils.translation import gettext as _

from analysis.backend.forms.forms_base import AnalysisBaseForm
from analysis.backend.utils.consts import BASIC_ID, ADVANCED_ID,DESeqFromCountsMatrixFields,DESeqFromCountsMatrixConst
from analysis.backend.utils.users import get_user_email
from analysis.models import CustomUser,DESeqFromCountsMatrix, InterMine

logger = logging.getLogger(__name__)


class DESeqFromCountsMatrixForm(AnalysisBaseForm):
    commands = DESeqFromCountsMatrixConst.COMMANDS
    def __init__(self, user_name, *args, **kwargs):
        super(DESeqFromCountsMatrixForm, self).__init__(user_name, *args, **kwargs)
        # FileBrowseField override the help_text from Meta class
        self.fields['input_folder'].help_text = _('csv or txt file format with the raw counts matrix in which the rows represent the genes and columns represent the different samples')
        self.fields['input_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}
        self.fields['input_folder'].required = True  # blank =true don't work in FileBrowseField
        # ChainedForeignKey override the genome and annotation form of the Meta class
        self.fields['intermine_genome'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['intermine_genome'].required = False
        self.get_all_class_fields(DESeqFromCountsMatrixFields())


    class Meta(AnalysisBaseForm.Meta):
        model = DESeqFromCountsMatrix
        fields = ['name', 'pipeline', 'input_folder', 'output_folder','intermine_genome','email']
        labels = {
            "intermine_genome": "Genomes for Functional Analysis"
        }





#
# class MarsSeqDeseqForm(MarsSeqForm):
#     def __init__(self, user_name, *args, **kwargs):
#         temp = {}
#         # save kwargs in temp, and delete all fields from kwargs. Because not all fields exists in AnalysisBaseForm.
#         # Notice: if the basic analysis made by submit, now we don't get run_id field
#         for field, value in kwargs.items():
#             if field in MarsSeqForm(user_name).fields:
#                 temp[field] = value
#             kwargs.pop(field)
#         super(MarsSeqDeseqForm, self).__init__(user_name, *args, **kwargs)
#
#         self.fields['input_folder'].widget.attrs['size'] = 120
#         self.fields['output_folder'].widget.attrs['size'] = 120
#         input_folder_help = _('Folder with fastq files - The same folder used by the basic analysis.')
#         output_folder_help = _('Output folder will be created within output folder of the basic analysis.')
#         self.fields['input_folder'].help_text = input_folder_help
#         self.fields['output_folder'].help_text = output_folder_help
#
#         for field_name in temp:
#             self.fields[field_name].initial = temp[field_name]
#             if field_name != 'email':
#                 if 'form_id' in self.fields[field_name].widget.attrs and self.fields[field_name].widget.attrs['form_id'] != 'advanced':
#                     self.fields[field_name].widget.attrs['readonly'] = True
#                     self.fields[field_name].widget.attrs['disabled'] = True
#
#     deseq_run_name = forms.CharField(required=True, help_text='Name of the new analysis of Deseq.',
#                                      label='Deseq run name',
#                                      widget=forms.TextInput(
#                                          attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))


