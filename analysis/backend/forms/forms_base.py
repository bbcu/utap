"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage, DefaultStorage
from django.forms import ModelForm, Select
from django.utils.translation import ugettext_lazy as _
from filebrowser.sites import site

from analysis.backend.settings import MAX_UPLOAD_SIZE
from analysis.backend.utils.consts import ANALYSIS_TYPES_DISPLAY, BASIC_ID, ANALYSIS_TYPES_DISPLAY_SUPERUSER
from analysis.backend.utils.users import get_user_dir, get_user_email
from analysis.models import Analysis, CustomUser

logger = logging.getLogger(__name__)


# TODO: problem with this function. It is validate only the size of the first file
def file_size(value):  # add this to some file where you can import it from
    if int(str(value.size)) / (1024 * 1024) > MAX_UPLOAD_SIZE:
        raise ValidationError('File too large. Size should not exceed %s megabytes.' % MAX_UPLOAD_SIZE)


class UploadFileForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(UploadFileForm, self).__init__(*args, **kwargs)

    file_field = forms.FileField(validators=[file_size], label="Select directory", widget=forms.ClearableFileInput(
        attrs={'class': 'btn btn-default', 'multiple': True, 'webkitdirectory': True, 'directory': True}))


# , 'onchange': 'validate_size(value);'

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    email = forms.EmailField(max_length=254, required=True, help_text='Required. Inform a valid email address.')

    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'first_name', 'last_name', 'password1', 'password2',)

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if CustomUser.objects.filter(email=email).exists():
            raise ValidationError("Email already exists.")
        return email


# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'attr': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.TextInput(attrs={'attr': 'form-control', 'name': 'password'}))


class LoginAsUserForm(forms.Form):
    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'attr': 'form-control', 'name': 'username'}))


class AnalysisChoiceForm(forms.Form):
    pipeline = forms.ChoiceField( label='Choose pipeline',
                                  widget=Select(
                                      attrs={'class': 'btn btn-default', 'onclick': 'mysubmit(value);'}))
    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(AnalysisChoiceForm, self).__init__(*args, **kwargs)
        logger.info(self.request.user.is_superuser)
        if self.request.user.is_superuser:
            self.fields['pipeline'].choices = (('', '---------'),) + ANALYSIS_TYPES_DISPLAY_SUPERUSER
        else:
            self.fields['pipeline'].choices = (('', '---------'),) + ANALYSIS_TYPES_DISPLAY


class AnalysisBaseForm(ModelForm):
    def __init__(self, user_name, *args, **kwargs):
        super(AnalysisBaseForm, self).__init__(*args, **kwargs)
        self.user_name = user_name
        self.user_dir = get_user_dir(str(self.user_name))

        # site.storage = FileSystemStorage(location='/')  # TODO: move it to settings file (MEDIA_ROOT="/" don't work)
        # site.directory = "./"  # TODO: move it to settings file (DIRECTORY="./") #No need. FileBrowseField concatenate the site.storage with this only if directory is not decleared but we declare it bellow (in .widget.directory).
        # site.storage = FileSystemStorage(location=self.user_dir)  # TODO: move it to settings file (MEDIA_ROOT="/" don't work)
        # site.directory = "./"  # TODO: move it to settings file (DIRECTORY="./") #No need. FileBrowseField concatenate the site.storage with this only if directory is not decleared but we declare it bellow (in .widget.directory).

        # FileBrowseField override the help_text from Meta class
        self.fields['output_folder'].help_text = _(
            'Select existing path to which the pipeline will create output folder for all the results')
        self.fields['output_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}
        self.fields['output_folder'].required = True  # blank = False don't work in FileBrowseField
        # # You can also declare directory in models, but we need to change it dynamically for each user
        # self.fields['output_folder'].widget.directory = self.user_dir[1:]  # remove first '/'. FileBrowseField concatenate the site.storage with this path.
        # self.fields['input_folder'].widget.directory = self.user_dir[1:]  # remove first '/'. FileBrowseField concatenate the site.storage with this path.
        self.fields['email'].initial = get_user_email(self.user_name)
        self.fields['lab_path'] = forms.CharField(required=True, initial=self.user_dir, widget=forms.HiddenInput())
        if CustomUser.objects.get(username=str(self.user_name)).is_superuser:
            self.fields['pipeline'].choices = (('', '---------'),) + ANALYSIS_TYPES_DISPLAY_SUPERUSER
        else:
            self.fields['pipeline'].choices = (('', '---------'),) + ANALYSIS_TYPES_DISPLAY

    #
    # def get_all_class_fields(self, pipline_fields):
    #     Not_SuperUser = not CustomUser.objects.get(username=str(self.user_name)).is_superuser
    #     for Name, Form_field, Required, Initial, Help_text, Lable, Widget in pipline_fields().get_all_char_fields():
    #         self.fields[Name] = Form_field(required=Required, initial=Initial, help_text=Help_text, label=Lable,
    #                                        widget=Widget)
    #         if Not_SuperUser:
    #             if 'form_id' in self.fields[Name].widget.attrs and self.fields[Name].widget.attrs[
    #                 'form_id'] == 'advanced':
    #                 self.fields[Name].widget.attrs['disabled'] = 'disabled'
    #     for Name, Form_field, Required, Initial, Choise, Help_text, Lable, Widget in pipline_fields().get_all_choise_fields():
    #         self.fields[Name] = Form_field(required=Required, initial=Initial, choices=Choise, help_text=Help_text,
    #                                        label=Lable, widget=Widget)
    #         if Not_SuperUser:
    #             if 'form_id' in self.fields[Name].widget.attrs and self.fields[Name].widget.attrs[
    #                 'form_id'] == 'advanced':
    #                 self.fields[Name].widget.attrs['disabled'] = 'disabled'
    #     for Name, Form_field, Required, Initial, Help_text, Lable, Widget in pipline_fields().get_all_float_fields():
    #         self.fields[Name] = Form_field(required=Required, initial=Initial, help_text=Help_text, label=Lable,
    #                                        widget=Widget)
    #         if Not_SuperUser:
    #             if 'form_id' in self.fields[Name].widget.attrs and self.fields[Name].widget.attrs[
    #                 'form_id'] == 'advanced':
    #                 self.fields[Name].widget.attrs['disabled'] = 'disabled'
    #     for Name, Form_field, Required, Initial, Help_text, Lable, Widget in pipline_fields().get_all_integer_fields():
    #         self.fields[Name] = Form_field(required=Required, initial=Initial, help_text=Help_text, label=Lable,
    #                                        widget=Widget)
    #         if Not_SuperUser:
    #             if 'form_id' in self.fields[Name].widget.attrs and self.fields[Name].widget.attrs[
    #                 'form_id'] == 'advanced':
    #                 self.fields[Name].widget.attrs['disabled'] = 'disabled'
    def get_all_class_fields(self, pipline_fields):
        is_super_user = CustomUser.objects.get(username=str(self.user_name)).is_superuser #for making cetain fields available for admin users only
        for name, form_field, required, initial, help_text, lable, widget in pipline_fields.get_char_fields():
            self.fields[name] = form_field(required=required, initial=initial, help_text=help_text, label=lable,
                                           widget=widget)
            logger.info(CustomUser.objects.filter(username=str(self.user_name)))
            logger.info(CustomUser.objects.filter(username=str(self.user_name)).filter(is_superuser=True))
            # if not is_super_user:
            #     if 'form_id' in self.fields[name].widget.attrs and self.fields[name].widget.attrs[
            #         'form_id'] == 'advanced':
            #         self.fields[name].widget.attrs['disabled'] = 'disabled'
        for name, form_field, required, initial, Choise, help_text, lable, widget in pipline_fields.get_choise_fields():
            self.fields[name] = form_field(required=required, initial=initial, choices=Choise, help_text=help_text,
                                           label=lable, widget=widget)
            # if not is_super_user:
            #     if 'form_id' in self.fields[name].widget.attrs and self.fields[name].widget.attrs[
            #         'form_id'] == 'advanced':
            #         self.fields[name].widget.attrs['disabled'] = 'disabled'
        for name, form_field, required, initial, help_text, lable, widget in pipline_fields.get_float_fields():
            self.fields[name] = form_field(required=required, initial=initial, help_text=help_text, label=lable,
                                           widget=widget)
            # if not is_super_user:
            #     if 'form_id' in self.fields[name].widget.attrs and self.fields[name].widget.attrs[
            #         'form_id'] == 'advanced':
            #         self.fields[name].widget.attrs['disabled'] = 'disabled'
        for name, form_field, required, initial, help_text, lable, widget in pipline_fields.get_integer_fields():
            self.fields[name] = form_field(required=required, initial=initial, help_text=help_text, label=lable,
                                           widget=widget)
            # if not is_super_user:
            #     if 'form_id' in self.fields[name].widget.attrs and self.fields[name].widget.attrs[
            #         'form_id'] == 'advanced':
            #         self.fields[name].widget.attrs['disabled'] = 'disabled'

    class Meta:
        model = Analysis
        fields = ['name', 'pipeline', 'output_folder', 'email']
        localized_fields = '__all__'
        help_texts = {
            'name': _('Name of the job (please use only the characters: 0-9, a-z, A-Z or -, _)'),
            'email': _('Email to get notification in the end of the run'),
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}),
            'pipeline': forms.Select(attrs={'class': 'btn btn-default', 'onclick': 'redirect(value);'}),
            'email': forms.EmailInput(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 30}),
        }


class TestForm(forms.Form):
    BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
    FAVORITE_COLORS_CHOICES = (
        ('blue', 'Blue'),
        ('green', 'Green'),
        ('black', 'Black'),
    )
    favorite_colors = forms.ChoiceField(
        widget=forms.RadioSelect(attrs={'id': 'id_favorite_colors', 'onclick': 'refresh(value);'}),
        choices=FAVORITE_COLORS_CHOICES)

# # Custom ChoiceField
# class FolderFastqField(ChoiceField):
#     # match=".*_R1.*(.fq|.fastq|.fq.gz|.fastq.gz)$",
#     def __init__(self, path, match=None, required=True, widget=None, label=None, initial=None, help_text='', *args,
#                  **kwargs):
#         super(FolderFastqField, self).__init__(choices=(), required=required, widget=widget, label=label,
#                                                initial=initial, help_text=help_text, *args, **kwargs)
#
#         match = match if match else '.*'
#         # if self.required:
#         #     self.choices = []
#         # else:
#         #     self.choices = [("", "---------")]
#         self.choices = [(None, "---------")]
#
#         for root_dir, bla, bla in os.walk(path):
#             logger.debug('start ' + root_dir)
#             num_samples = 0
#             num_files_in_sample = 0
#             root_invalid = False
#             if os.path.isdir(root_dir):
#                 for sample_dir in os.listdir(root_dir):
#                     match_sample = match
#                     if os.path.isdir(os.path.join(path, root_dir, sample_dir)):
#                         files = glob.glob(os.path.join(root_dir, sample_dir, '*'))
#
#                         if num_files_in_sample == 0:
#                             num_files_in_sample = len(files)
#
#                         if not files or len(files) > 2 or len(
#                                 files) != num_files_in_sample:  # all samples contain the same number of files
#                             root_invalid = True
#                             break
#                         for file in sorted(files):  # at least one invalid file
#                             if not re.compile(match_sample).search(file):
#                                 root_invalid = True
#                                 break
#                             if file[-3:] == '.gz':  # all files in samples must be compressed or uncompressed
#                                 match_sample = match_sample.replace('fq', 'fq.gz').replace('fastq', 'fastq.gz')
#                             else:
#                                 match_sample = match_sample.replace('fq.gz', 'fq').replace('fastq.gz', 'fastq')
#                             match_sample = match_sample.replace('_R1', '_R2')  # for second file in sample
#                         if not root_invalid:
#                             num_samples += 1
#
#             if not root_invalid and num_samples > 1:  # at least 2 valid samples
#                 self.choices.append((root_dir, root_dir))
#         self.widget.choices = self.choices
#
