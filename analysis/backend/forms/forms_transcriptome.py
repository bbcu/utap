"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os

from django import forms
from django.utils.translation import gettext as _

from analysis.backend.forms.forms_base import AnalysisBaseForm
from analysis.backend.utils.consts import BASIC_ID, ADVANCED_ID, RNAseqConst, RNAseqFields, MarsSeqFields, ScrbSeqFields, MarsSeqConst, ScrbSeqConst
from analysis.backend.utils.users import get_user_email
from analysis.models import CustomUser, TranscriptomAnalysis, TranscriptomSubmitAnalysis

logger = logging.getLogger(__name__)



class TranscriptomeBaseForm(AnalysisBaseForm):
    def __init__(self, user_name, *args, **kwargs):
        super(TranscriptomeBaseForm, self).__init__(user_name, *args, **kwargs)
        # FileBrowseField override the help_text from Meta class
        self.fields['input_folder'].help_text = _(
            'Folder with fastq files in appropriate structure (see help for details).')
        self.fields['input_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}
        self.fields['input_folder'].required = True  # blank =true don't work in FileBrowseField
        # ChainedForeignKey override the genome and annotation form of the Meta class
        self.fields['genome'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['annotation'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}

    class Meta(AnalysisBaseForm.Meta):
        model = TranscriptomAnalysis
        fields = ['name', 'pipeline', 'input_folder', 'genome', 'annotation', 'output_folder', 'email']

    # deseq_run = forms.ChoiceField(required=True,
    #                               initial=RNAseqConst.DESEQRUN_INIT,
    #                               choices=RNAseqConst.DESEQRUN_CHOICES,
    #                               help_text=RNAseqConst.DESEQRUN_HELP,
    #                               label=RNAseqConst.DESEQRUN_LABEL,
    #                               widget=forms.Select(
    #                                   attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))


class MarsSeqForm(TranscriptomeBaseForm):
    commands = MarsSeqConst.COMMANDS
    def __init__(self, user_name, *args, **kwargs):
        super(MarsSeqForm, self).__init__(user_name, *args, **kwargs)
        self.get_all_class_fields(MarsSeqFields())

class ScrbSeqForm(TranscriptomeBaseForm):
    commands = ScrbSeqConst.COMMANDS
    def __init__(self, user_name, *args, **kwargs):
        super(ScrbSeqForm, self).__init__(user_name, *args, **kwargs)
        self.get_all_class_fields(ScrbSeqFields())


class RnaSeqForm(TranscriptomeBaseForm):
    commands = RNAseqConst.COMMANDS

    def __init__(self, user_name, *args, **kwargs):
        super(RnaSeqForm, self).__init__(user_name, *args, **kwargs)
        self.get_all_class_fields(RNAseqFields())

    #field_order = TranscriptomeBaseForm.Meta.fields + ['stranded', 'adapter_on_R1', 'adapter_on_R2', 'deseq_run']

    # stranded = forms.ChoiceField(required=True,
    #                              choices=RNAseqConst.STRANDED_CHOICES,
    #                              help_text=RNAseqConst.STRANDED_HELP,
    #                              label=RNAseqConst.STRANDED_LABEL,
    #                              widget=forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
    #
    # adapter_on_R1 = forms.CharField(required=True, initial=RNAseqConst.ADAPTER1_INIT,
    #                                 help_text=RNAseqConst.ADAPTER1_HELP,
    #                                 label=RNAseqConst.ADAPTER1_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}))
    #
    # adapter_on_R2 = forms.CharField(required=False, initial=RNAseqConst.ADAPTER2_INIT,
    #                                 help_text=RNAseqConst.ADAPTER2_HELP,
    #                                 label=RNAseqConst.ADAPTER2_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}))

class MarsSeqSubmitForm(MarsSeqForm): #submit form doesnt work (link had been removed from susanc) relevant to 31.12.21
    def __init__(self, user_name, *args, **kwargs):
        run_id = kwargs.pop('run_id')  # remove run_id from kwargs, because MarsSeqForm have not this field
        super(MarsSeqSubmitForm, self).__init__(user_name, *args, **kwargs)
        run_as = CustomUser.objects.get(username=user_name).loggedas_username
        self.fields['email'].initial = get_user_email(run_as)
        self.fields['run_id'].initial = run_id
        self.fields['input_folder'].initial = os.path.join(self.user_dir, run_id + '_copied_from_stefan', run_id)
        self.fields['output_folder'].initial = os.path.join(self.user_dir, run_id + '_copied_from_stefan')
        input_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
        output_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
        self.fields['input_folder'].widget = forms.TextInput(attrs=input_folder_attr)
        self.fields['output_folder'].widget = forms.TextInput(attrs=output_folder_attr)


    class Meta(MarsSeqForm.Meta):
            model = TranscriptomSubmitAnalysis
            fields = ['pipeline', 'name', 'run_id', 'input_folder', 'genome', 'annotation', 'output_folder', 'email']
            run_id_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 40}
            #input_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
            #output_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
            input_folder_help = _(
                'Folder with fastq files - this folder will be created when NextSeq run will be ended.')
            output_folder_help = _(
                'Select existing path to which the pipeline will create output folder for all the results.')
            MarsSeqForm.Meta.widgets['run_id'] = forms.TextInput(attrs=run_id_attr)
            #MarsSeqForm.Meta.widgets['input_folder'] = forms.TextInput(attrs=input_folder_attr)
            #MarsSeqForm.Meta.widgets['output_folder'] = forms.TextInput(attrs=output_folder_attr)
            MarsSeqForm.Meta.help_texts['run_id'] = 'ID of the run on the Stefan server.'
            MarsSeqForm.Meta.help_texts['input_folder'] = input_folder_help
            MarsSeqForm.Meta.help_texts['output_folder'] = output_folder_help


class RnaSeqSubmitForm(RnaSeqForm): #submit form doesnt work (link had been removed from susanc) relevant to 31.12.21
    def __init__(self, user_name, *args, **kwargs):
        run_id = kwargs.pop('run_id')
        super(RnaSeqSubmitForm, self).__init__(user_name, *args, **kwargs)
        run_as = CustomUser.objects.get(username=user_name).loggedas_username
        self.fields['email'].initial = get_user_email(run_as)
        self.fields['run_id'].initial = run_id
        self.fields['input_folder'].initial = os.path.join(self.user_dir, run_id + '_copied_from_stefan', run_id)
        self.fields['output_folder'].initial = os.path.join(self.user_dir, run_id + '_copied_from_stefan')
        input_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
        output_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
        self.fields['input_folder'].widget = forms.TextInput(attrs=input_folder_attr)
        self.fields['output_folder'].widget = forms.TextInput(attrs=output_folder_attr)

    class Meta(RnaSeqForm.Meta):
            model = TranscriptomSubmitAnalysis
            fields = ['pipeline', 'name', 'run_id', 'input_folder', 'genome', 'annotation', 'output_folder', 'email']
            run_id_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 40}
            #input_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
            #output_folder_attr = {'class': 'btn btn-default', 'form_id': BASIC_ID, 'readonly': 'readonly', 'size': 120}
            input_folder_help = _(
                'Folder with fastq files - this folder will be created when NextSeq run will be ended.')
            output_folder_help = _(
                'Select existing path to which the pipeline will create output folder for all the results.')
            RnaSeqForm.Meta.widgets['run_id'] = forms.TextInput(attrs=run_id_attr)
            #RnaSeqForm.Meta.widgets['input_folder'] = forms.TextInput(attrs=input_folder_attr)
            #RnaSeqForm.Meta.widgets['output_folder'] = forms.TextInput(attrs=output_folder_attr)
            RnaSeqForm.Meta.help_texts['run_id'] = 'ID of the run on the Stefan server.'
            RnaSeqForm.Meta.help_texts['input_folder'] = input_folder_help
            RnaSeqForm.Meta.help_texts['output_folder'] = output_folder_help


class MarsSeqDeseqForm(MarsSeqForm):
    def __init__(self, user_name, *args, **kwargs):
        temp = {}
        # save kwargs in temp, and delete all fields from kwargs. Because not all fields exists in AnalysisBaseForm.
        # Notice: if the basic analysis made by submit, now we don't get run_id field
        for field, value in kwargs.items():
            if field in MarsSeqForm(user_name).fields:
                temp[field] = value
            kwargs.pop(field)
        super(MarsSeqDeseqForm, self).__init__(user_name, *args, **kwargs)

        self.fields['input_folder'].widget.attrs['size'] = 120
        self.fields['output_folder'].widget.attrs['size'] = 120
        input_folder_help = _('Folder with fastq files - The same folder used by the basic analysis.')
        output_folder_help = _('Output folder will be created within output folder of the basic analysis.')
        self.fields['input_folder'].help_text = input_folder_help
        self.fields['output_folder'].help_text = output_folder_help

        for field_name in temp:
            self.fields[field_name].initial = temp[field_name]
            if field_name != 'email':
                if 'form_id' in self.fields[field_name].widget.attrs and self.fields[field_name].widget.attrs['form_id'] != 'advanced':
                    self.fields[field_name].widget.attrs['readonly'] = True
                    self.fields[field_name].widget.attrs['disabled'] = True

    deseq_run_name = forms.CharField(required=True, help_text='Name of the new analysis of DESeq2.',
                                     label='DESeq2 run name',
                                     widget=forms.TextInput(
                                         attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))

class ScrbSeqDeseqForm(MarsSeqDeseqForm):
    def __init__(self, user_name, *args, **kwargs):
        super(ScrbSeqDeseqForm, self).__init__(user_name, *args, **kwargs)
    deseq_run_name = forms.CharField(required=True, help_text='Name of the new analysis of DESeq2.',
                                         label='DESeq2 run name',
                                         widget=forms.TextInput(
                                             attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))

class RnaSeqDeseqForm(RnaSeqForm):
    def __init__(self, user_name, *args, **kwargs):
        temp = {}  # save kwargs in temp, and delete all fields from kwargs. Because not all fields exists in AnalysisBaseForm
        for field, value in kwargs.items():
            if field in RnaSeqForm(user_name).fields:
                temp[field] = value
            kwargs.pop(field)
        super(RnaSeqDeseqForm, self).__init__(user_name, *args, **kwargs)

        self.fields['input_folder'].widget.attrs['size'] = 120
        self.fields['output_folder'].widget.attrs['size'] = 120
        input_folder_help = _('Folder with fastq files - The same folder used by the basic analysis.')
        output_folder_help = _('Output folder will be created within output folder of the basic analysis.')
        self.fields['input_folder'].help_text = input_folder_help
        self.fields['output_folder'].help_text = output_folder_help

        for field_name in temp:
            self.fields[field_name].initial = temp[field_name]
            if field_name != 'email':
                if 'form_id' in self.fields[field_name].widget.attrs and self.fields[field_name].widget.attrs['form_id'] != 'advanced':
                    self.fields[field_name].widget.attrs['readonly'] = True
                    self.fields[field_name].widget.attrs['disabled'] = True

    deseq_run_name = forms.CharField(required=True, help_text='Name of the new analysis of DESeq2.',
                                     label='DESeq2 run name',
                                     widget=forms.TextInput(
                                         attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
