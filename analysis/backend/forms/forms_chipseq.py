"""
This file is part of UTAP!.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging

from django import forms
from django.utils.translation import gettext as _
from django.utils.translation import ugettext_lazy as _

from analysis.backend.forms.forms_base import AnalysisBaseForm
#from analysis.backend.utils.consts import CHIPseqConst
from analysis.models import ChipSeqAnalysis

logger = logging.getLogger(__name__)

#why are we importing chipseq/atacseq again
from analysis.backend.utils.consts import BASIC_ID, ADVANCED_ID, CHIPseqConst,ChipSeqFields #,chip_fields


class ChipSeqForm(AnalysisBaseForm):
    commands = CHIPseqConst.COMMANDS
    def __init__(self, user_name, *args, **kwargs):
        #why are we inheriting from the same class we want these attributes to be first applied
        super(ChipSeqForm, self).__init__(user_name, *args, **kwargs)
        # FileBrowseField override the help_text from Meta class
        self.fields['input_folder'].help_text = _(
            'Folder with fastq files in appropriate structure (see help for details).')
        self.fields['input_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['input_folder'].required = True  # blank=true don't work in FileBrowseField
        # ChainedForeignKey override the genome and annotation form of the Meta class
        self.fields['genome'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['chromosome_info'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.get_all_class_fields(ChipSeqFields())



    class Meta(AnalysisBaseForm.Meta):
        model = ChipSeqAnalysis
        fields = ['name', 'pipeline', 'input_folder', 'genome', 'chromosome_info', 'output_folder', 'email']





    # aditional_adapter_on_R1, aditional_adapter_on_R2= chip_fields.return_all_field()
    # #
    # #
    # #
    # adapter_on_R1 = forms.CharField(required=True, initial=CHIPseqConst.ADAPTER1_INIT,
    #                                 help_text=CHIPseqConst.ADAPTER1_HELP,
    #                                 label=CHIPseqConst.ADAPTER1_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}))
    #
    # adapter_on_R2 = forms.CharField(required=False, initial=CHIPseqConst.ADAPTER2_INIT,
    #                                 help_text=CHIPseqConst.ADAPTER2_HELP,
    #                                 label=CHIPseqConst.ADAPTER2_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}))

    # example for advanced parameters in the chip-seq pipeline
    # aditional_adapter_on_R1= forms.CharField(required=False, initial=CHIPseqConst.SELECT_ADAPTER1_INIT,
    #                                 help_text=CHIPseqConst.SELECT_ADAPTER1_HELP,
    #                                 label=CHIPseqConst.SELECT_ADAPTER1_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID , 'size': 80}))
    #
    # aditional_adapter_on_R2= forms.CharField(required=False, initial=CHIPseqConst.SELECT_ADAPTER2_INIT,
    #                                 help_text=CHIPseqConst.SELECT_ADAPTER2_HELP,
    #                                 label=CHIPseqConst.SELECT_ADAPTER2_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID , 'size': 80}))

    #
    # treat_vs_control = forms.ChoiceField(required=True,
    #                                      initial=CHIPseqConst.TRT_CONT_INIT,
    #                                      choices=CHIPseqConst.TRT_CONT_CHOICES,
    #                                      help_text=CHIPseqConst.TRT_CONT_HELP,
    #                                      label=CHIPseqConst.TRT_CONT_LABEL,
    #                                      widget=forms.Select(
    #                                          attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
    #
