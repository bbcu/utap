# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2021-11-09 14:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0017_auto_20211109_1430'),
    ]

    operations = [
        migrations.RenameField(
            model_name='allgenomes',
            old_name='bowtie1_Rrna',
            new_name='bowtie1_rRNA',
        ),
        migrations.AddField(
            model_name='bowtie1rrna',
            name='fasta',
            field=models.CharField(blank=True, max_length=500),
        ),
    ]
