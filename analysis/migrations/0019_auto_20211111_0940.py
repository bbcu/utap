# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2021-11-11 07:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0018_auto_20211109_1644'),
    ]

    operations = [
        migrations.AlterField(
            model_name='allgenomes',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
