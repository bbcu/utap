# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2021-11-09 12:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0016_auto_20211107_1052'),
    ]

    operations = [
        migrations.CreateModel(
            name='AllGenomes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creature', models.CharField(max_length=200)),
                ('alias', models.CharField(max_length=30)),
            ],
            options={
                'ordering': ('creature',),
                'verbose_name_plural': 'AllGenomes',
            },
        ),
        migrations.RemoveField(
            model_name='htseqannotation',
            name='genome',
        ),
        migrations.RemoveField(
            model_name='riboseqanalysis',
            name='Bowtie1_rRNA',
        ),
        migrations.RemoveField(
            model_name='riboseqanalysis',
            name='HTSeq_count_Annotation',
        ),
        migrations.RemoveField(
            model_name='riboseqanalysis',
            name='chromosome_info',
        ),
        migrations.AddField(
            model_name='riboseqanalysis',
            name='annotation',
            field=smart_selects.db_fields.ChainedForeignKey(auto_choose=True, chained_field='genome', chained_model_field='genome', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.StarAnnotation'),
        ),
        migrations.AddField(
            model_name='starannotation',
            name='path_UTR_CDS',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AlterField(
            model_name='bowtie1rrna',
            name='genome',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='riboseqanalysis',
            name='genome',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.AllGenomes'),
        ),
        migrations.DeleteModel(
            name='HtseqAnnotation',
        ),
        migrations.AddField(
            model_name='allgenomes',
            name='bowtie1_Rrna',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.Bowtie1Rrna'),
        ),
        migrations.AddField(
            model_name='allgenomes',
            name='bowtie2_genome',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.Bowtie2Genome'),
        ),
        migrations.AddField(
            model_name='allgenomes',
            name='chromosomes_sizes',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.ChromosomeInfo'),
        ),
        migrations.AddField(
            model_name='allgenomes',
            name='star_Annotation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.StarAnnotation'),
        ),
        migrations.AddField(
            model_name='allgenomes',
            name='star_genome',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.StarGenome'),
        ),
    ]
