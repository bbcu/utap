# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2020-05-06 18:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0012_auto_20200119_1738'),
    ]

    operations = [
        migrations.CreateModel(
            name='DESeqFromCountsMatrix',
            fields=[
                ('analysis_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='analysis.Analysis')),
                ('input_folder', filebrowser.fields.FileBrowseField(max_length=1000, null=True, verbose_name='Input folder')),
            ],
            options={
                'verbose_name_plural': 'DESeq to counts matrix Analyses',
            },
            bases=('analysis.analysis',),
        ),
        migrations.CreateModel(
            name='InterMine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('interMine_creature', models.CharField(max_length=200)),
                ('creature', models.CharField(blank=True, max_length=200)),
                ('alias', models.CharField(blank=True, max_length=30)),
                ('interMine_web_query', models.CharField(max_length=200)),
                ('intermine_web_base', models.CharField(max_length=200)),
                ('gene_db_url', models.CharField(blank=True, max_length=200)),
                ('genome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.StarGenome')),
            ],
            options={
                'verbose_name_plural': 'InterMine',
            },
        ),
        migrations.AlterField(
            model_name='analysis',
            name='pipeline',
            field=models.CharField(blank=True, choices=[(b'Transcriptome RNA-seq', 'Transcriptome RNA-seq'), (b'Transcriptome Mars-seq', 'Transcriptome Mars-seq'), (b'Demultiplexing_from_BCL', 'Demultiplexing_from_BCL'), (b'DESeq from counts matrix', 'DESeq from counts matrix'), (b'ATAC-seq', 'ATAC-seq'), (b'CHIP-seq', 'CHIP-seq'), (b'Demultiplexing_from_RUNID', 'Demultiplexing_from_RUNID'), (b'Transcriptome Mars-seq Deseq', 'Transcriptome Mars-seq Deseq'), (b'Transcriptome RNA-seq Deseq', 'Transcriptome RNA-seq Deseq')], max_length=200, verbose_name='Choose pipeline'),
        ),
        migrations.AddField(
            model_name='deseqfromcountsmatrix',
            name='intermine_genome',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.InterMine'),
        ),
    ]
