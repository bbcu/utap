# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2020-03-19 15:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0017_intermine_alias'),
    ]

    operations = [
        migrations.AddField(
            model_name='intermine',
            name='gene_db_url',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
