from __future__ import unicode_literals

import datetime
import logging

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from filebrowser.fields import FileBrowseField
from smart_selects.db_fields import ChainedForeignKey

from analysis.backend.utils.consts import ANALYSIS_TYPES
from analysis.backend.utils.general import updated_user_dir

logger = logging.getLogger(__name__)


@python_2_unicode_compatible  # only if you need to support Python 2
class CustomUser(AbstractUser):
    phone = models.CharField(max_length=20, blank=True)
    pi = models.ForeignKey('self', null=True, blank=True)
    lab_path = models.CharField(max_length=100, null=True, blank=True)
    loggedas_username = models.CharField(max_length=30, null=True, blank=True)
    loggedas_lab_path = models.CharField(max_length=100, null=True, blank=True)
    AbstractUser._meta.get_field('is_staff').default = True

    def __str__(self):
        return self.username


@python_2_unicode_compatible
class Profile(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=CustomUser)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@python_2_unicode_compatible
class CellRangerGenome(models.Model):
    creature = models.CharField(max_length=200)
    path = models.CharField(max_length=500)
    alias = models.CharField(max_length=30)
    version = models.CharField(max_length=100)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")_V" + str(self.version)

    class Meta:
        verbose_name_plural = "Cellranger Genomes"
        ordering = ('creature',)


@python_2_unicode_compatible
class Bowtie2Genome(models.Model):
    creature = models.CharField(max_length=200)
    alias = models.CharField(max_length=30)
    version = models.FloatField()
    source = models.CharField(max_length=1000)
    path = models.CharField(max_length=500)
    fasta = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"

    class Meta:
        verbose_name_plural = "Bowtie2 Genomes"
        ordering = ('creature',)


@python_2_unicode_compatible
class TssFile(models.Model):
    genome = models.ForeignKey(Bowtie2Genome)
    creature = models.CharField(max_length=200)
    alias = models.CharField(max_length=30)
    version = models.FloatField()
    source = models.CharField(max_length=1000)
    path = models.CharField(max_length=500)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"

    class Meta:
        verbose_name_plural = "TSS Files"


@python_2_unicode_compatible
class ChromosomeInfo(models.Model):
    genome = models.ForeignKey(Bowtie2Genome)
    creature = models.CharField(max_length=200, blank=True)
    alias = models.CharField(max_length=30, blank=True)
    path = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"
    class Meta:
        verbose_name_plural = "ChoromosomeInfo_files"



@python_2_unicode_compatible
class Bowtie1Rrna(models.Model):
    genome = models.CharField(max_length=200)
    creature = models.CharField(max_length=200, blank=True)
    alias = models.CharField(max_length=30, blank=True)
    path = models.CharField(max_length=500, blank=True)
    fasta= models.CharField(max_length=500, blank=True)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"
    class Meta:
        verbose_name_plural = "Bowtie1 rRNA"


@python_2_unicode_compatible
class StarGenome(models.Model):
    creature = models.CharField(max_length=200)
    alias = models.CharField(max_length=30)
    version = models.FloatField()
    source = models.CharField(max_length=1000)
    path = models.CharField(max_length=500)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"

    class Meta:
        verbose_name_plural = "STAR Genomes"
        ordering = ('creature',)


@python_2_unicode_compatible
class AllGenomes(models.Model):
    id= models.AutoField(primary_key=True)
    star_genome = models.ForeignKey(StarGenome, null=True, blank=True, on_delete=models.DO_NOTHING)
    bowtie2_genome = models.ForeignKey(Bowtie2Genome, null=True, blank=True, on_delete=models.DO_NOTHING)
    bowtie1_rRNA = models.ForeignKey(Bowtie1Rrna, null=True, blank=True, on_delete=models.DO_NOTHING)
    chromosomes_sizes= models.ForeignKey(ChromosomeInfo, null=True, blank=True, on_delete=models.DO_NOTHING)
    creature = models.CharField(max_length=200)
    alias = models.CharField(max_length=30)
    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"
    #
    # def save(self, *args, **kwargs):
    #     star_genome_id = StarGenome.objects.get(alias=self.alias)
    #     self.id =star_genome_id.id


    class Meta:
        verbose_name_plural = "AllGenomes"
        ordering = ('creature',)


@python_2_unicode_compatible
class StarAnnotation(models.Model):
    all_genome=models.ForeignKey(AllGenomes,blank=True, null=True)
    genome = models.ForeignKey(StarGenome)
    creature = models.CharField(max_length=200)
    alias = models.CharField(max_length=30)
    version = models.FloatField()
    source = models.CharField(max_length=1000)
    path = models.CharField(max_length=500)
    path3p = models.CharField(max_length=500)
    path_UTR_CDS = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"

    class Meta:
        verbose_name_plural = "STAR Annotations"


@python_2_unicode_compatible
class InterMine(models.Model):
    genome = models.ForeignKey(StarGenome, null=True, blank=True, on_delete=models.DO_NOTHING)
    interMine_creature = models.CharField(max_length=200)
    creature = models.CharField(max_length=200, blank=True)
    alias = models.CharField(max_length=30, blank=True)
    interMine_web_query= models.CharField(max_length=200)
    intermine_web_base= models.CharField(max_length=200)
    gene_db_url = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return str(self.creature) + " (" + str(self.alias) + ")"

    class Meta:
        verbose_name_plural = "InterMine"
        ordering = ('creature',)

        
@python_2_unicode_compatible
class Analysis(models.Model):
    name = models.CharField("Project name", max_length=80, blank=False, null=True)
    output_folder = FileBrowseField("Output folder", max_length=1000, blank=False, null=True)
    parameters = models.CharField("parameters file", max_length=500, blank=True)
    parameters_json = models.CharField("parameters json file", max_length=500, blank=True)
    date = models.DateTimeField("date created", auto_now_add=True, blank=True)
    user = models.ForeignKey(CustomUser, blank=True)
    user_run = models.CharField(max_length=30, null=True, blank=True)
    pipeline = models.CharField("Choose pipeline", max_length=200, choices=ANALYSIS_TYPES, blank=True)
    status = models.CharField("run status", max_length=200, blank=True)
    results = models.CharField("results", max_length=200, null=True, blank=True)
    results_temp = models.CharField("results_temp", max_length=200, null=True, blank=True)
    email = models.EmailField("User email", max_length=40, blank=False, null=True)

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    def was_created_recently(self):
        return self.date >= timezone.now() - datetime.timedelta(days=1)

    was_created_recently.admin_order_field = 'date'
    was_created_recently.boolean = True
    was_created_recently.short_description = 'Created recently?'

    class Meta:
        verbose_name_plural = "Analyses"


@python_2_unicode_compatible
class TranscriptomAnalysis(Analysis):
    input_folder = FileBrowseField("Input folder", max_length=1000, blank=False, null=True)
    # models.DO_NOTHING in genome and annotation: In each run of docker all genomes and the annotations are deleted from the DB we want deny the deletion of the analyses that use with them
    genome = models.ForeignKey(StarGenome, null=True, blank=False, on_delete=models.DO_NOTHING)
    annotation = ChainedForeignKey(StarAnnotation, chained_field="genome", chained_model_field="genome", show_all=False,
                                   auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    class Meta:
        verbose_name_plural = "Transcriptome Analyses"


class TranscriptomSubmitAnalysis(TranscriptomAnalysis):
    run_id = models.CharField("Run ID", max_length=40, blank=False, null=True)

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)


@python_2_unicode_compatible
class DemultiplexingAnalysis(Analysis):
    bcl_files = FileBrowseField("BCL files", max_length=1000, blank=False, null=True)
    fastq_folder = FileBrowseField("Fastq folder", max_length=1000, blank=False, null=True)
#    fastq_file_R1 = FileBrowseField("Fastq file read1", max_length=1000, blank=False, null=True)
#    fastq_file_R2 = FileBrowseField("Fastq file read2 (Optional)", max_length=1000, blank=True, null=True)
#    fastq_file_I = FileBrowseField("Fastq file index", max_length=1000, blank=False, null=True)
    run_id = models.CharField("Run ID", max_length=40, blank=False, null=True)

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    class Meta:
        verbose_name_plural = "Demultiplexing Analyses"


@python_2_unicode_compatible
class SinglecellAnalysis(Analysis):
    input_folder = FileBrowseField("Input folder", max_length=1000, blank=False, null=True)
    # models.DO_NOTHING in cellrangerGenome : In each run of docker all cellrangerGenomes are deleted from the DB we want deny the deletion of the analyses that use with them
    cellranger_genome = models.ForeignKey(CellRangerGenome, null=True, blank=False,
                                          on_delete=models.DO_NOTHING)  # In each run of docker all genomes are deleting

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    class Meta:
        verbose_name_plural = "Singlecell Analyses"


@python_2_unicode_compatible
class AtacSeqAnalysis(Analysis):
    input_folder = FileBrowseField("Input folder", max_length=1000, blank=False, null=True)
    # models.DO_NOTHING in genome and annotation: In each run of docker all genomes and the annotations are deleted from the DB we want deny the deletion of the analyses that use with them
    genome = models.ForeignKey(Bowtie2Genome, null=True, blank=False, on_delete=models.DO_NOTHING)
    tss_file = ChainedForeignKey(TssFile, chained_field="genome", chained_model_field="genome", show_all=False,
                                 auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    class Meta:
        verbose_name_plural = "ATAC-seq Analyses"


@python_2_unicode_compatible
class ChipSeqAnalysis(Analysis):
    input_folder = FileBrowseField("Input folder", max_length=1000, blank=False, null=True)
    # models.DO_NOTHING in genome and annotation: In each run of docker all genomes and the annotations are deleted from the DB we want deny the deletion of the analyses that use with them
    genome = models.ForeignKey(Bowtie2Genome, null=True, blank=False, on_delete=models.DO_NOTHING)
    chromosome_info = ChainedForeignKey(ChromosomeInfo, chained_field="genome", chained_model_field="genome", show_all=False,
                                 auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    class Meta:
        verbose_name_plural = "CHIP-seq Analyses"

@python_2_unicode_compatible
class RiboSeqAnalysis(Analysis):
    input_folder = FileBrowseField("Input folder", max_length=1000, blank=False, null=True)
    # models.DO_NOTHING in genome and annotation: In each run of docker all genomes and the annostations are deleted from the DB we want deny the deletion of the analyses that use with them
    genome = models.ForeignKey(AllGenomes, null=True, blank=False, on_delete=models.DO_NOTHING)
    annotation = ChainedForeignKey(StarAnnotation, chained_field="genome", chained_model_field="all_genome", show_all=False,
                                   auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)
    # chromosome_info = ChainedForeignKey(ChromosomeInfo, chained_field="genome", chained_model_field="genome", show_all=False,
    #                              auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)
    # Bowtie1_rRNA=ChainedForeignKey(Bowtie1Rrna, chained_field="genome", chained_model_field="genome", show_all=False,
    #                              auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)
    # HTSeq_count_Annotation = ChainedForeignKey(HtseqAnnotation, chained_field="genome", chained_model_field="genome", show_all=False,
    #                                  auto_choose=True, sort=True, null=True, blank=False, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.name + ", job_id: " + str(self.id)


    class Meta:
        verbose_name_plural = "Ribo-seq Analyses"





@python_2_unicode_compatible
class DESeqFromCountsMatrix(Analysis):
    input_folder = FileBrowseField("Input file", max_length=1000, blank=False, null=True)
    # genome = models.ForeignKey(StarGenome, null=True, blank=False, on_delete=models.DO_NOTHING)
    #intermine_genome = models.ForeignKey(InterMine, null=True, blank=False, on_delete=models.DO_NOTHING)
    intermine_genome= models.ForeignKey(InterMine, null=True, blank=False, on_delete=models.DO_NOTHING)


    def __str__(self):
        return self.name + ", job_id: " + str(self.id)

    class Meta:
        verbose_name_plural = "DESeq to counts matrix Analyses"

        
        


def user_logged_in_handler(sender, request, user, **kwargs):
    username = user.username
    user_obj = CustomUser.objects.get(username=username)
    user_obj.lab_path = updated_user_dir(username)
    user_obj.save()


def user_logged_out_handler(sender, request, user, **kwargs):
    username = user.username
    user_obj = CustomUser.objects.get(username=username)
    if user_obj.loggedas_username:
        user_obj.loggedas_username = None
        user_obj.loggedas_lab_path = None
    user_obj.save()


user_logged_in.connect(user_logged_in_handler)
user_logged_out.connect(user_logged_out_handler)
