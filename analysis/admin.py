from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin

from analysis.backend.utils.consts import RNASEQ_ANALYSIS_TYPE, MARSSEQ_ANALYSIS_TYPE, RNASEQ_DESEQ_ANALYSIS_TYPE, \
    MARSSEQ_DESEQ_ANALYSIS_TYPE, ALL_TRANSCRIPTOME_PIPELINES, DEMULTIPLEXING_BCL_ANALYSIS_TYPE, DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE, \
    DEMULTIPLEXING_RUNID_ANALYSIS_TYPE, SINGLECELL_CELRANGER_ANALYSIS_TYPE, ATACSEQ_ANALYSIS_TYPE, CHIPSEQ_ANALYSIS_TYPE, RIBOSEQ_ANALYSIS_TYPE, DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE
from analysis.models import Analysis, TranscriptomAnalysis, TranscriptomSubmitAnalysis, DemultiplexingAnalysis, \
    SinglecellAnalysis, AtacSeqAnalysis, ChipSeqAnalysis, RiboSeqAnalysis, CustomUser, StarGenome, StarAnnotation, CellRangerGenome, Bowtie2Genome, TssFile, ChromosomeInfo, DESeqFromCountsMatrix, InterMine, Bowtie1Rrna ,AllGenomes#HtseqAnnotation


class StarAnnotationInline(admin.TabularInline):
    model = StarAnnotation
    extra = 2

class TssFileInline(admin.TabularInline):
    model = TssFile
    extra = 2

class ChromosomeInfoInline(admin.TabularInline):
    model = ChromosomeInfo
    extra = 2


class Bowtie1RrnaInline(admin.TabularInline):
    model = Bowtie1Rrna
    extra = 2

class TranscriptomAnalysisInline(admin.StackedInline):  # admin.StackedInline):
    model = TranscriptomAnalysis
    extra = 3


class TranscriptomSubmitAnalysisInline(admin.StackedInline):  # admin.StackedInline):
    model = TranscriptomSubmitAnalysis
    extra = 3


class DemultiplexingAnalysisInline(admin.StackedInline):  # admin.StackedInline):
    model = DemultiplexingAnalysis
    extra = 3


class SinglecellAnalysisInline(admin.StackedInline):  # admin.StackedInline):
    model = SinglecellAnalysis
    extra = 3


class AtacSeqAnalysisInline(admin.StackedInline):  # admin.StackedInline):
    model = AtacSeqAnalysis
    extra = 3
# why is the model single cell analysis

class ChipSeqAnalysisInline(admin.StackedInline):  # admin.StackedInline):
    model =  ChipSeqAnalysis
    extra = 3


class RiboSeqAnalysisInline(admin.StackedInline):
    model = RiboSeqAnalysis
    extra = 3


class DESeqFromCountsMatrixAnalysisInline(admin.StackedInline):
    model = DESeqFromCountsMatrix
    extra = 3

class InterMineAnalysisInline(admin.StackedInline):
    model = InterMine
    extra = 3


class AnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    fieldsets = [(None, {'fields': ['name']}), ]
    list_filter = ['date', 'user', 'pipeline', 'status']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = (
        'id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline', 'was_created_recently', 'results')

    def get_inline_instances(self, request, obj=None):
        if obj.pipeline in ALL_TRANSCRIPTOME_PIPELINES:
            if hasattr(obj, 'run_id_'):  # submit pipeline
                self.inlines = [TranscriptomSubmitAnalysisInline]
            else:
                self.inlines = [TranscriptomAnalysisInline]
        if obj.pipeline in [DEMULTIPLEXING_BCL_ANALYSIS_TYPE, DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE,
                            DEMULTIPLEXING_RUNID_ANALYSIS_TYPE]:
            self.inlines = [DemultiplexingAnalysisInline]
        if obj.pipeline == SINGLECELL_CELRANGER_ANALYSIS_TYPE:
            self.inlines = [SinglecellAnalysisInline]
        if obj.pipeline == ATACSEQ_ANALYSIS_TYPE:
            self.inlines = [AtacSeqAnalysisInline]
        if obj.pipeline == CHIPSEQ_ANALYSIS_TYPE:
            self.inlines = [ChipSeqAnalysisInline]
        if obj.pipeline == DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE:
            self.inlines = [DESeqFromCountsMatrixAnalysisInline]
        if obj.pipeline == RIBOSEQ_ANALYSIS_TYPE:
            self.inlines = [RiboSeqAnalysisInline]

        return super(AnalysisAdmin, self).get_inline_instances(request, obj)


class DemultiplexingAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = ('id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline', 'was_created_recently', 'results')

class DESeqFromCountsMatrixAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = ('id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline','was_created_recently', 'results')

    def interMine(self, obj):
        return '<a href="/admin/analysis/intermine/%s">%s</a>' % (obj.interMine.pk, obj.interMine)


    interMine.allow_tags = True

class TranscriptomeAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status', 'genome', 'annotation']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = (
        'id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline', 'Genome', 'Annotation',
        'was_created_recently', 'results')

    def Genome(self, obj):
        return '<a href="/admin/analysis/stargenome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    def Annotation(self, obj):
        return '<a href="/admin/analysis/starannotation/%s">%s</a>' % (obj.annotation.pk, obj.annotation)

    Genome.allow_tags = True
    Annotation.allow_tags = True


class AtacSeqAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status', 'genome', 'tss_file']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = (
        'id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline', 'Bowtie2Genome', 'TssFile',
        'was_created_recently', 'results')

    def Bowtie2Genome(self, obj):
        return '<a href="/admin/analysis/bowtie2genome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    def TssFile(self, obj):
        # if obj.tss_file:
        return '<a href="/admin/analysis/tssfile/%s">%s</a>' % (obj.tss_file.pk, obj.tss_file)

    Bowtie2Genome.allow_tags = True
    TssFile.allow_tags = True


class ChipSeqAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status', 'genome']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = (
        'id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline', 'Bowtie2Genome', 'ChromosomeInfo',
        'was_created_recently', 'results')

    def Bowtie2Genome(self, obj):
        return '<a href="/admin/analysis/bowtie2genome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    def ChromosomeInfo(self, obj):
        return '<a href="/admin/analysis/chromosomeinfo/%s">%s</a>' % (obj.chromosome_info.pk, obj.chromosome_info)

    Bowtie2Genome.allow_tags = True
    ChromosomeInfo.allow_tags = True


class RiboSeqAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status', 'genome']
    search_fields = ['name', 'id', 'status']  # , 'genome', 'annotation'] #cannot include forign keys in search field
    list_display = (
        'id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline','genome', 'was_created_recently', 'results')
    def AllGenomes(self,obj):
        return '<a href="/admin/analysis/allgenomes/%s">%s</a>' % (obj.genome.pk, obj.genome)


    #Bowtie2Genome.allow_tags = True
    #ChromosomeInfo.allow_tags = True
    #HtseqAnnotation.allow_tags = True
    #Bowtie1Rrna.allow_tags = True
    AllGenomes.allow_tags = True


class SinglecellAnalysisAdmin(admin.ModelAdmin):
    list_display_links = ('name',)
    list_filter = ['date', 'user', 'pipeline', 'status', 'cellranger_genome']
    search_fields = ['name', 'id', 'status']  # , 'cellranger_genome'] #cannot include forign keys in search field
    list_display = (
        'id', 'name', 'status', 'user', 'parameters', 'date', 'pipeline', 'CellRangerGenome', 'was_created_recently', 'results')

    def CellRangerGenome(self, obj):
        return '<a href="/admin/analysis/cellrangergenome/%s">%s</a>' % (obj.cellranger_genome.pk, obj.cellranger_genome)

    CellRangerGenome.allow_tags = True


class CellRangerGenomesAdmin(admin.ModelAdmin):
    list_display_links = ('id',)
    list_filter = ['creature', 'alias']
    search_fields = ['creature', 'id', 'alias']
    list_display = ('id', 'creature', 'alias', 'version', 'path')


class Bowtie2GenomeAdmin(admin.ModelAdmin):
    inlines = [TssFileInline]
    list_display_links = ('id',)
    list_filter = ['creature', 'alias']
    search_fields = ['creature', 'id', 'alias']
    list_display = ('id', 'creature', 'alias', 'version', 'source', 'path','fasta')


class StarGenomeAdmin(admin.ModelAdmin):
    inlines = [InterMineAnalysisInline]
    inlines = [StarAnnotationInline]
    list_display_links = ('id',)
    list_filter = ['creature', 'alias']
    search_fields = ['creature', 'id', 'alias']
    list_display = ('id', 'creature', 'alias', 'version', 'source', 'path')


class StarAnnotationAdmin(admin.ModelAdmin):
    list_display_links = ('id',)
    list_filter = ['creature', 'alias']
    search_fields = ['creature', 'id', 'alias']
    list_display = ('id', 'Genome', 'creature', 'alias', 'version', 'source', 'path', 'path3p', 'path_UTR_CDS')

    def Genome(self, obj):
        return '<a href="/admin/analysis/stargenome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    Genome.allow_tags = True


class TssFileAdmin(admin.ModelAdmin):
    list_display_links = ('id',)
    list_filter = ['creature', 'alias']
    search_fields = ['creature', 'id', 'alias']
    list_display = ('id', 'Bowtie2Genome', 'creature', 'alias', 'version', 'source', 'path')

    def Bowtie2Genome(self, obj):
        return '<a href="/admin/analysis/bowtie2genome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    Bowtie2Genome.allow_tags = True


class ChromosomeInfoAdmin(admin.ModelAdmin):
    list_display_links = ('id',)
    list_filter = ['creature']
    search_fields = ['creature', 'id']
    list_display = ('id', 'Bowtie2Genome', 'creature', 'alias', 'path')

    def Bowtie2Genome(self, obj):
        return '<a href="/admin/analysis/bowtie2genome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    Bowtie2Genome.allow_tags = True


class InterMineAdmin(admin.ModelAdmin):
    list_display_links = ('id',)
    list_filter = ['creature']
    search_fields = ['creature', 'id']
    list_display = ('id', 'interMine_creature','Genome', 'creature', 'interMine_web_query', 'intermine_web_base')

    def Genome(self, obj):
        return '<a href="/admin/analysis/stargenome/%s">%s</a>' % (obj.genome.pk, obj.genome)

    Genome.allow_tags = True


class Bowtie1RrnaAdmin(admin.ModelAdmin):
    list_display_links = ('id',)
    list_filter = ['creature']
    search_fields = ['creature', 'id']
    list_display = ('id', 'creature', 'alias', 'path')




class AllGenomesAdmin(admin.ModelAdmin):
    list_display_links = ('id','Star_genome', 'Bowtie2_genome', 'Bowtie1_rRNA', 'Chromosomes_sizes')
    list_filter = ['creature']
    search_fields = ['creature', 'id']
    list_display = ('id','Star_genome', 'Bowtie2_genome', 'Bowtie1_rRNA', 'Chromosomes_sizes','creature', 'alias')

    def Bowtie2_genome(self, obj):
        return '<a href="/admin/analysis/bowtie2genome/%s">%s</a>' % (obj.bowtie2_genome.pk, obj.bowtie2_genome)

    def Bowtie1_rRNA(self, obj):
        return '<a href="/admin/analysis/bowtie1rrna/%s">%s</a>' % (obj.bowtie1_rRNA.pk, obj.bowtie1_rRNA)

    def Star_genome(self, obj):
        return '<a href="/admin/analysis/stargenome/%s">%s</a>' % (obj.star_genome.pk, obj.star_genome)

    def Chromosomes_sizes(self, obj):
        return '<a href="/admin/analysis/chromosomeinfo/%s">%s</a>' % (obj.chromosomes_sizes.pk, obj.chromosomes_sizes)

    Bowtie1_rRNA.allow_tags = True
    Bowtie2_genome.allow_tags = True
    Star_genome.allow_tags = True
    Chromosomes_sizes.allow_tags = True

class UserAdmin(BaseUserAdmin):
    personl_info = BaseUserAdmin.fieldsets[1:2]
    personl_info[0][1]['fields'] += ('pi', 'phone', 'lab_path', 'loggedas_username', 'loggedas_lab_path')
    list_display = BaseUserAdmin.list_display


# admin.site.register(Group)
admin.site.register(CustomUser, UserAdmin)
admin.site.register(Analysis, AnalysisAdmin)
admin.site.register(TranscriptomAnalysis, TranscriptomeAnalysisAdmin)
admin.site.register(DemultiplexingAnalysis, DemultiplexingAnalysisAdmin)
admin.site.register(SinglecellAnalysis, SinglecellAnalysisAdmin)
admin.site.register(AtacSeqAnalysis, AtacSeqAnalysisAdmin)
admin.site.register(ChipSeqAnalysis, ChipSeqAnalysisAdmin)
admin.site.register(RiboSeqAnalysis, RiboSeqAnalysisAdmin)
admin.site.register(CellRangerGenome, CellRangerGenomesAdmin)
admin.site.register(Bowtie2Genome, Bowtie2GenomeAdmin)
admin.site.register(TssFile, TssFileAdmin)
admin.site.register(StarGenome, StarGenomeAdmin)
admin.site.register(StarAnnotation, StarAnnotationAdmin)
admin.site.register(ChromosomeInfo, ChromosomeInfoAdmin)
admin.site.register(DESeqFromCountsMatrix, DESeqFromCountsMatrixAnalysisAdmin)
admin.site.register(InterMine, InterMineAdmin)
admin.site.register(Bowtie1Rrna, Bowtie1RrnaAdmin)
admin.site.register(AllGenomes, AllGenomesAdmin)
