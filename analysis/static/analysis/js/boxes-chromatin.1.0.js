//For now no need to import it in navigation_bar.html file
//import this file as ES6 "module" - <script type="module" src="on_get_samples_trascriptome.js"></script>

var MAX_LEVELS = 50;//Maximum number of groups. Must to be even number

//Each group is 2 boxes one for the treatment and the other for the control.
//The control box can contain only one sample.
function add_group_treat(number, name) {
    var box = `
        <div class='added catg_${number}'><br><br>
            <div class="row">
                <div class="col-md-3">
                    <!--<button type="button" id="multi_d_rightAll_${number}" class="btn btn-default btn-block" style="margin-top: 5px;"><i class="glyphicon glyphicon-forward"></i></button>-->
                    <button type="button" id="multi_d_rightSelected_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="multi_d_leftSelected_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                    <!--<button type="button" id="multi_d_leftAll_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-backward"></i></button>-->
                </div>
                <div class="col-md-7">
                    <b><input id='treat_name_${number}' name='treat_name_${number}' value='${name}' class="form-control" readonly="readonly" /></b>
                    <select id="multi_d_to_${number}" onclick="colorSelected(this)" name="to_${number}" class="form-control treat_box" size="3" multiple="multiple"></select>
                </div>
            </div>
        </div>
    `;
    return box;
}

function add_group_control(number, name) {
    var box = `
        <div class='added catg_${number}'><br><br>
            <div class="row">
                <div class="col-md-3">
                    <button type="button" id="multi_d_rightSelected_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="multi_d_leftSelected_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                </div>
                <div class="col-md-7">
                    <b><input id='control_name_${number}' name='control_name_${number}' value='${name}' class="form-control" readonly="readonly" /></b>
                    <select id="multi_d_to_${number}" onclick="colorSelected(this)" name="to_${number}" class="form-control control_box" size="3" multiple="multiple"></select><hr>
                </div>
            </div>
        </div>
    `;
    return box;
}

export function hide_all_boxes_chromatin() {
    for (var i = 3; i <= MAX_LEVELS; i++) {
        $('.' + "catg_" + i).hide();
        $("#treat_name_" + i).removeAttr('required');
        $("#control_name_" + i).removeAttr('required');
        //$("#multi_d_to_"+i).removeAttr('required');
    }
}


export function add_multiselect_chromatin(samples) {
    samples = samples.sort();
    $(".added").remove();
    // https://stackoverflow.com/questions/16521565/html-select-with-different-background-color-for-every-option-that-works-properly
    var multiselect_temp = `
                            <div class='added'><br><br>
                                <ul>
                                    <li>Move the samples <b>(or part of them)</b> into right boxes.</li>
                                    <li>Each group must contain at least one sample in each of the treatment and control boxes.</li>
                                    <li>If you have more than one treatment against control, press on the "Add group" button.</li>
                                    <li>When moving a sample to the control box, a copy of the sample is retained, so that you can use it again in a new group.</li>
                                    <li>If you move more than one sample to the treatment or control box, the pipeline will automatically combine the samples into one big treatment/control sample.</li>
                                    <li><b>Important:</b> All of the pipeline steps (mapping, counts etc.) will be run <b>(only) on the samples in the treatment/control boxes</b>.</li>
                                </ul>
                                <br>
                                <div class='row'>
                                    <div class='col-md-6'></div>
                                    <div class='col-md-4'>
                                        <input type='hidden' id='num_groups' value=2>
                                        <button id='add_group' class='btn btn-primary' type='button'>Add <br>group</button>
                                        <button id='remove_group' class='btn btn-primary' type='button'>Remove <br>group</button>
                                    </div>
                                </div>
                            `;

    multiselect_temp += "<div class='row'><div class='col-md-5'><select name='from[]' id='multi_d' class='form-control' size='" + samples.length * 1.2 + "' multiple='multiple'>";
    for (var i = 0; i < samples.length; i++) {
        multiselect_temp += '<option value="' + i + '">' + samples[i] + '</option>';
    }
    multiselect_temp += "</select></div>";//close col-md-5
    multiselect_temp += "<div class='col-md-6'>";
    multiselect_temp += add_group_treat(1, 'Treatment group 1');
    multiselect_temp += add_group_control(2, 'Control group 1');
    for (i = 3; i <= MAX_LEVELS; i += 2) {
        multiselect_temp += add_group_treat(i, 'Treatment group ' + Math.ceil(i / 2));
        multiselect_temp += add_group_control(i + 1, 'Control group ' + (i + 1) / 2);
    }
    multiselect_temp += "</div></div>"; //close row and added class
    $("#multiselect_temp").append(multiselect_temp);
}


export function reload_multiselect_chromatin() {
    var rightSelected_template = [];
    for (var i = 1; i < MAX_LEVELS; i++) rightSelected_template.push('#multi_d_rightSelected_' + i);
    var right_template = [];
    for (i = 1; i < MAX_LEVELS; i++) right_template.push('#multi_d_to_' + i);
    var leftSelected_template = [];
    for (i = 1; i < MAX_LEVELS; i++) leftSelected_template.push('#multi_d_leftSelected_' + i);
    var rightAll_template = [];
    for (i = 1; i < MAX_LEVELS; i++) rightAll_template.push('#multi_d_rightAll_' + i);
    var leftAll_template = [];
    for (i = 1; i < MAX_LEVELS; i++) leftAll_template.push('#multi_d_leftAll_' + i);

    $('#multi_d').multiselect({
        rightSelected: rightSelected_template.join(),
        right: right_template.join(),
        leftSelected: leftSelected_template.join(),
        rightAll: rightAll_template.join(),
        leftAll: leftAll_template.join(),

        search: {
            left: '<br><br><input type="text" name="q" class="form-control" placeholder="Filter samples (type part of the name)" />'
        },

        moveToRight: function (Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id'); // the button is: multi_d_rightSelected_* or multi_d_rightAll_*
            var i = button.split('_').pop(); //destination box number.
            if (button == 'multi_d_rightSelected_' + i) {
                if ($('#multi_d_to_' + i)[0] && $('#multi_d_to_' + i)[0].options.length > 0) {
                    var combine  = confirm('Adding more than one sample will cause to combining the samples into one big sample.\nPress on "Add group" if you want use with more than one group of treatment vs. control.');
                    if (!combine) {return}
                }
                var $left_options = Multiselect.$left.find('> option:selected');
                //"Copy" the samlpe to right box and not remove it from the left box
                if ($('.control_box#multi_d_to_' + i)[0]) {// If the destination is control
                    if ($("#multi_d_to_" + i  + " option[value='" + $left_options[0].value + "']")[0]) { // Already exists in this control
                        alert('The sample "' + $left_options[0].text + '" already selected.');
                        return;
                    }
                    var cln = $left_options[0].cloneNode(true);
                    // alert(JSON.stringify(Multiselect.$left[0]));
                    Multiselect.$right.eq(i - 1).append(cln);
                } else {
                    if ($(".control_box option[value='" + $left_options[0].value + "']")[0]) {
                        alert('The sample "' + $left_options[0].text + '" already selected as control, you cannot select it as treatment');

                    } else {
                        Multiselect.$right.eq(i - 1).append($left_options);
                    }
                }

                if (typeof Multiselect.callbacks.sort == 'function' && !silent) {
                    Multiselect.$right.eq(i - 1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(i - 1));
                }
            } else if (button == 'multi_d_rightAll_' + i) {
                var combine  = confirm('Adding more than one sample will cause to combining the samples into one big sample.\nPress on "Add group" if you want use with more than one group of treatment vs. control.');
                if (!combine) {return}

                var $left_options = Multiselect.$left.children(':visible');
                alert($left_options.length)
                for (var j=0; j<$left_options.length; ++j) {
                    if ($(".control_box option[value='" + $left_options[j].value + "']")[0]) {
                        alert('The sample "' + $left_options[j].text + '" already selected as control, you cannot select it as treatment');
                    } else {
                        Multiselect.$right.eq(i - 1).append($left_options[j]);
                    }
                }

                if (typeof Multiselect.callbacks.sort == 'function' && !silent) {
                    Multiselect.$right.eq(i - 1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(i - 1));
                }
            }
            $(".added").find("option:selected").removeAttr("selected");
        },

        moveToLeft: function (Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id'); // the button is: multi_d_leftSelected_* or multi_d_leftAll_*
            var i = button.split('_').pop(); //destination box number.
            if (button == 'multi_d_leftSelected_' + i) {
                var $right_options = Multiselect.$right.eq(i - 1).find('> option:selected');
                // Moving from right control to left, only delete from right but not add it to left. because it is already exists in left (moving to control not delete it from left box)
                if ($('.control_box#multi_d_to_' + i)[0]) { //If it is control box
                    for (var j = 0; j < $('#multi_d')[0].options.length; ++j) {
                        if ($right_options[0].value == $('#multi_d')[0].options[j].value) {
                            $(".control_box#multi_d_to_" + i + " option[value='" + $right_options[0].value + "']").remove();
                            $right_options = null;
                            break;
                        }
                    }
                } else {
                    Multiselect.$left.append($right_options);
                }
                if (typeof Multiselect.callbacks.sort == 'function' && !silent) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_leftAll_' + i) {
                var $right_options = Multiselect.$right.eq(i - 1).children(':visible');
                Multiselect.$left.append($right_options);

                if (typeof Multiselect.callbacks.sort == 'function' && !silent) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            }
            $(".added").find("option:selected").removeAttr("selected");
        }
    });
}


$(document).ready(function () {
    //Handle in events of objects that not created yet. It is possible because the parent: #multiselect_temp exists
    $('#multiselect_temp').on('click', '#add_group', function (event) {
        var num_groups = $("#num_groups").val();
        var next_group_class_treat = "catg_" + (parseInt(num_groups) + 1);
        var next_group_class_control = "catg_" + (parseInt(num_groups) + 2);
        $('.' + next_group_class_treat).show();
        $('.' + next_group_class_control).show();
        $("#treat_name_" + (parseInt(num_groups) + 1)).attr('required', true);
        $("#control_name_" + (parseInt(num_groups) + 2)).attr('required', true);
        //$("#multi_d_to_"+(parseInt(num_groups)+1)).attr('required', true);
        if (num_groups < MAX_LEVELS) {
            $("#num_groups").val(parseInt(num_groups) + 2);
        }
    });

    $('#multiselect_temp').on('click', '#remove_group', function (event) {
        var num_groups = $("#num_groups").val();
        if (num_groups > 2) {
            var last_group_class_treat = "catg_" + (num_groups - 1);
            var last_group_class_control = "catg_" + num_groups;
            $('.' + last_group_class_treat).hide();
            $('.' + last_group_class_control).hide();
            $("#treat_name_" + num_groups - 1).removeAttr('required');
            $("#control_name_" + num_groups).removeAttr('required');
            //$("#multi_d_to_"+num_groups).removeAttr('required');
            $("#num_groups").val(parseInt(num_groups) - 2);
        }
    });
});

