
var colNum =2;
var rows_pool = [];
var rows = [];
var samplesheetData = [['', ''],['', ''],['', ''],['', ''],];
var samplesheetDataPool = [['', ''],['', ''],['', ''],['', ''],];
$(document).ready(function(){
    document.getElementById("Run").disabled = false;
    $('#id_UMI_length_wrap').hide();
    $('#samplesheet_table').jexcel({
        data:samplesheetData,
        colHeaders: ['Sample', 'Index'],
        colWidths: [ 300, 150],
        columns: [
            { type: 'text' },
            { type: 'text' },
        ]
    });
    $('#pool_table').jexcel({
        data:samplesheetDataPool,
        colHeaders: ['Pool', 'Index'],
        colWidths: [ 300, 150],
         columns: [
            { type: 'text' },
            { type: 'text' },
         ]
    });

});


//
//function AddPools() {
//  $("#samplesheet_table").append($("<td></td>"));
//}
//

function check_ss(){
//        alert(samplesheetData);
//        var ajax_results=[];
    var $table = $("#samplesheet_table");
    var $pool_table =$("#pool_table");
    var ajax_results = [];
    var headers = [];
    var headers_pools = [];
    rows_pool = [];
    rows = [];



    $table.find("thead tr td:not(:first-child)").each(function () {
            rows.push('$'+ $(this).html());
        });

    $table.find("tbody tr").each(function () {
        $(this).find("td:not(:first-child)").each(function (i) {
            if ($(this).html() !="" )
                rows.push($(this).html());
        });
    });

//    rows.unshift(headers);


    $pool_table.find("tbody tr").each(function () {
        $(this).find("td:not(:first-child)").each(function (i) {
        //                var key = header[i],
            if ($(this).html() !="" )
                rows_pool.push($(this).html());
            });
    });

    if (rows_pool.length){
        $pool_table.find("thead tr td:not(:first-child)").each(function () {
            rows_pool.unshift('$'+ $(this).html());
    });
//        rows_pool.unshift(headers_pools);
    } else {
        rows_pool=[];
        }



//    $table.find("thead tr td:not(:first-child)").each(function () {
//        headers.push('$'+ $(this).html());
//    });
//
//    samplesheetData.unshift(headers);
//
//    alert(samplesheetDataPool)
//    if (samplesheetDataPool.join('').toString() != initData.join('').toString() && samplesheetDataPool.toString() !=""){
//        $pool_table.find("thead tr td:not(:first-child)").each(function () {
//            headers_pools.push('$'+ $(this).html());
//        });
//        samplesheetDataPool.unshift(headers_pools);
//    }
//    else
//        samplesheetDataPool=[];

    $.ajax({
        url: '/ajax/check_samplesheet/',
        dataType: 'json',
        data: {
        json_data: JSON.stringify({'samplesheet_data': rows, 'pool_data': rows_pool})},
        traditional: true,
        async: false, //Js waits until the ajax ended
        success: function(data_out) {
            ajax_results = [true, data_out];
        },
        error: function(data_out, textStatus, errorThrown) {
            ajax_results = [false, data_out, textStatus, errorThrown]; //textStatus, errorThrown not in use yet
        },
    });
//    samplesheetData = samplesheetData.filter(item => item !== headers);
//    samplesheetDataPool = samplesheetDataPool.filter(item => item !== headers_pools);
    return ajax_results;

};

$(function(){
    $("#id_protocol").change(function() {
        $('#samplesheet_table thead tr').find('td:eq(2)').nextAll().remove();
        $("#DualInd").removeClass("active");
        $("#DualInd").text("Add Dual Index");
        $("#DualInd").show();
        var selected_txt= $("#id_protocol :selected").text();
        if(selected_txt == "SCRB-Seq") {
            $('#id_UMI_length_wrap').show();
            $('#pool_table thead tr').find('td:eq(2)').nextAll().remove();
            //$("#pool_class").show();
            $("#pool").removeClass("active");
            $("#DualIndPool").removeClass("active");
            $("#DualIndPool").text("Add Dual Index");
            $("#pool").show();
        //    $("#pool_title").show();
        }
        else {
            $('#pool_class').attr('class', 'collapse');
            $('#pool_class').attr("aria-expanded","false");
            $('#pool').attr("aria-expanded","false");
            $("#pool").hide();
        //      $("#pool_title").hide();
              //$("#pool_class").hide();
            $('#id_UMI_length_wrap').hide();
            $('#samplesheet_table thead tr').find('td:eq(2)').nextAll().remove();
        }
        });
    //  $("#AddPools").click(function(){
    //     $('#samplesheet_table').jexcel('insertColumn', 1, { header:'Pool name' ,column: { type:'text' },width:'200', align:'center'});
    //     $('#samplesheet_table').jexcel('insertColumn', 1, { header:'Index' ,column: { type:'text' },width:'200', align:'center'});
    //
    //
    //  });

    $('#samplesheet_table').on('contextmenu', 'tr', function(e){
    //      var  colspan = $("#samplesheet_table thead tr:first td").length;
    //        if(colspan > 4)
        e.preventDefault();
        return false;
    });
    $('#pool_table').on('contextmenu', 'tr', function(e){
    //      var  colspan = $("#samplesheet_table thead tr:first td").length;
    //        if(colspan > 4)
        e.preventDefault();
        return false;
    });

    $("#DualInd").click(function(){
        if ($(this).hasClass("active")) {
            $("#DualInd").removeClass("active");
            $('#samplesheet_table thead tr').find('td:eq(2)').nextAll().remove()
            $("#DualInd").text("Add Dual Index");
        }
        else {
            $(this).addClass("active");
            $("#DualInd").text("Remove Dual Index");
            $('#samplesheet_table').jexcel('insertColumn', 1, { header:'Index2' ,column: { type:'text' },width:'150', align:'center'});
        }
    });

    $("#DualIndPool").click(function(){
        if ($(this).hasClass("active")) {
            $("#DualIndPool").removeClass("active");
            $('#pool_table thead tr').find('td:eq(2)').nextAll().remove()
            $("#DualIndPool").text("Add Dual Index");
        }
        else {
            $(this).addClass("active");
            $("#DualIndPool").text("Remove Dual Index");
            $('#pool_table').jexcel('insertColumn', 1, { header:'Index2' ,column: { type:'text' },width:'150', align:'center'});
        }
    //$("#samplesheet_table tbody").find('tr:eq(0)').hide();
    //    $("#samplesheet_table thead tr").append($("<td id=\"col-"+colNum+"\" type=\"text\" width=\"150\" align=\"center\">Pool</td>"));
    //    colNum++;
    //    $("#samplesheet_table thead tr").append($("<td id=\"col-"+colNum+"\" type=\"text\" width=\"100\" align=\"center\">Pool Barcode</td>"));
    //    colNum++;
    //    $("#samplesheet_table tbody tr").each(function(i, tr) {
    //    $(tr).append('<td id=\"'+parseInt(colNum-2)+'-'+parseInt(i)+'\" class=\"c'+parseInt(colNum-2)+' r'+parseInt(i)+'\" width=\"80\" align=\"center\" ></td>');
    //    $(tr).append('<td id=\"'+parseInt(colNum-1)+'-'+parseInt(i)+'\" class=\"c'+parseInt(colNum-1)+' r'+parseInt(i)+'\" width=\"80\" align=\"center\" ></td>');
    });


    $("#pool").click(function(){
        if ($(this).hasClass("active")) {
            $("#pool").removeClass("active");
            $('#samplesheet_table thead tr').find('td:eq(2)').nextAll().remove()
            $("#DualInd").removeClass("active");
            $("#DualInd").text("Add Dual Index");
            $("#DualInd").show();
        }
        else {
            $(this).addClass("active");
            $('#samplesheet_table thead tr').find('td:eq(2)').nextAll().remove()
            $('#samplesheet_table').jexcel('insertColumn', 1, { header:'Pool' ,column: { type:'text' },width:'150', align:'center'});
            $("#DualInd").hide();
        }
    });

    $("#checkSS").click(function(){
        var data_out = check_ss();
        rows.splice(0, 2);
        rows_pool.splice(0, 2);
        if(rows.indexOf("$") > -1)
            rows.splice(0, 1);
        if(rows_pool.indexOf("$") > -1)
            rows.splice(0, 1);
        if (data_out[0]) {
            alert("" + data_out[1].valid);
        }
        else {
            var json = JSON.parse(data_out[1].responseText);
            alert(json.message);
        }
    });

});


function enable_select_fields() {
    $('select:disabled, input:disabled').each(function () {
       $(this).removeAttr('disabled');
    });
}

function submit_disable_button() {
    if ( document.getElementById("samplesheet_table") ){
//        var cellText =[];
//        $('#samplesheet_table thead tr td').each(function() {
//            cellText = cellText+ ",$" + $(this).html();
//        });
//        samplesheetData.push(cellText);
        response=check_ss();
        if (response[0]){
            document.getElementById("id_samplesheet").value = rows;
            var pool = document.getElementById("id_pool");
            if(pool){
                document.getElementById("id_pool").value = rows_pool;
            }
        }
        else{
            var json = JSON.parse(response[1].responseText);
            alert(json.message);
            document.getElementById("Run").disabled = true;

        }
    }
    enable_select_fields();
    if ($('#form_analysis')[0].checkValidity()){//deny to press again on run_analysis button.
        document.getElementById("Run").disabled = true;
    }
    $('#hidden-submit').click();
};
