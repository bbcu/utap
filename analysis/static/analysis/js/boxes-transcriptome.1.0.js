//For now no need to import it in navigation_bar.html file
//import this file as ES6 "module" - <script type="module" src="on_get_samples_trascriptome.js"></script>

var MAX_LEVELS = 50;//Maximum number of categories.
var MAX_BATCHES = 12;//Maximum number of batches.

export function get_report_dir_name(){
    var report_dir_name= ` <div class='report_name_added'><br><br>
                            <div class="row">
                                  <div class="col-md-4">
                                    <abbr title="Report directory name(please use only the characters: 0-9, a-z, A-Z or -, _)">
                                      <label for="id_report_name">report directory name:</label> <br><br>
                                  </div>
                                  <div class="col-md-10">
                                       <input type="text" name="report_name" id="id_report_name" required="" maxlength="15" class="btn btn-default" form_id="basic">
                                  </div>
                             </div>
                            </div>
                         `;
    $("#report_dir_name").append(report_dir_name);
}


function add_category(number, name){
    // language=HTML
    var box =`
        <div class='added catg_${number}'><br><br>
            <div class="row">
                <div class="col-md-3">
                    <button type="button" id="multi_d_rightAll_${number}" class="btn btn-default btn-block" style="margin-top: 5px;"><i class="glyphicon glyphicon-forward"></i></button>
                    <button type="button" id="multi_d_rightSelected_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="multi_d_leftSelected_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                    <button type="button" id="multi_d_leftAll_${number}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-backward"></i></button>
                </div>
                <div class="col-md-7">
                    <b><input id='category_name_${number}' name='category_name_${number}' placeholder='${name}' class="form-control" required /></b>
                    <select id="multi_d_to_${number}" onclick="colorSelected(this)" name="to_${number}" class="form-control" size="8" multiple="multiple"></select><br>
                </div>
            </div>
        </div>
    `;
    return box;
}


export function hide_all_boxes_transcriptome() {
    $(".batch").hide();
    for (var i=3; i<=MAX_LEVELS; i++) {
        $('.'+"catg_"+i).hide();
        $("#category_name_"+i).removeAttr('required');
        //$("#multi_d_to_"+i).removeAttr('required');
    }
}


export function add_UMI_len() {
    $(".UMI_len_added").remove();
    if ($("#id_UMI").val() === 'yes' ){
        var UMI_len= ` <div class='UMI_len_added'><br><br>
                            <div class="row">
                                  <div class="col-md-4">
                                    <abbr title="Please enter the UMI barcode length">
                                      <label for="id_UMI_length">UMI length:</label> <br><br>
                                  </div>
                                  <div class="col-md-4">
                                       <input type="number" name="UMI_barcode_length" id="id_UMI_length" required class="btn btn-default" form_id="basic">
                                  </div>
                             </div>
                            </div>
                         `;
        $("#UMI_length").append(UMI_len);
        }
    }

export function add_multiselect_transcriptome(samples){
    samples = samples.sort();
    $(".added").remove();
    // https://stackoverflow.com/questions/16521565/html-select-with-different-background-color-for-every-option-that-works-properly
    var multiselect_temp = `
                            <div class='added'><br><br>
                                <ul>
                                    <li>Move the samples <b>(or part of them)</b> into category boxes. The order of the comparisons will be determined by the order of the category boxes, for example: DESeq2 will output "Treatment" vs "Control" comparison in case that user inserts the "Treatment" as the first category and the "Control" as the second.</li>
                                    <li>If the samples were prepared in different batches, you can add this information: <b>After moving the samples into category boxes,</b> click on "Add Batch Effect" button, then select the samples from the category boxes that belongs to one batch and click on "Batch 1" button. Repeat the operation with the other batches. Be sure that the batch effect is designed correctly: <a href="https://bioconductor.org/packages/3.7/bioc/vignettes/DESeq2/inst/doc/DESeq2.html#model-matrix-not-full-rank">DESeq2 documentation</a></li>
                                    <li>All steps of the pipeline (mapping, counts etc.) will be run on all samples, but DESeq2 will be run only on the samples w\ithin categories.</li>
                                </ul>
                                <br>
                                <div class='row'>
                                    <div class='col-md-6'></div>
                                    <div class='col-md-4'>
                                        <input type='hidden' id='num_categories' value=2>
                                        <button id='add_category' class='btn' type='button'>Add <br>Category</button>
                                        <button id='remove_category' class='btn' type='button'>Remove <br>Category</button>
                                    </div>
                                    <div class='col-md-1'>
                                        <input type='button' value='Add Batch Effect' id='batch_effect' class='btn btn-primary'></input>
                                        <input type='button' value='Add More Batches' id='more_batches' class='btn btn-alert batch'></input>
                                    </div>
                                </div>
                            `;

    multiselect_temp += "<div class='row'><div class='col-md-5'><select name='from[]' id='multi_d' class='form-control' size='"+samples.length*1.2+"' multiple='multiple'>";
    for (var i=0; i<samples.length; i++){
        multiselect_temp += '<option value="'+i+'">'+ samples[i] +'</option>';
    }
    multiselect_temp += "</select></div>";//close col-md-5
    multiselect_temp += "<div class='col-md-6'>";
    multiselect_temp += add_category(1, 'Category 1 name');
    multiselect_temp += add_category(2, 'Category 2 name');
    for (i=3; i<=MAX_LEVELS; i++) {
        multiselect_temp += add_category(i, 'Category '+i+' name');
    }
    multiselect_temp += "</div>";
    multiselect_temp += "<div class='col-md-1'>";
    multiselect_temp += "<br><br><div class='row'><button id='batch1' class='btn batch red' type='button' onclick='changeColor(\"red\", \"1\")'>Batch 1</button></div>" +
                    "<div class='row'><button id='batch2' class='btn batch saddleBrown' type='button' onclick='changeColor(\"saddleBrown\", \"2\")'>Batch 2</button></div>" +
                    "<div class='row'><button id='batch3' class='btn batch green' type='button' onclick='changeColor(\"green\", \"3\")'>Batch 3</button></div>" +
                    "<div class='row'><button id='batch4' class='btn batch orange' type='button' onclick='changeColor(\"orange\", \"4\")'>Batch 4</button></div>" +
                    "<div class='row'><button id='batch5' class='btn batch navy' type='button' onclick='changeColor(\"navy\", \"5\")'>Batch 5</button></div>"+
                    "<div class='row'><button id='batch6' class='btn batch mediumVioletRed' type='button' onclick='changeColor(\"mediumVioletRed\", \"6\")'>Batch 6</button></div>"+
                    "<div class='row'><button id='batch7' class='btn batch darkGrey' type='button' onclick='changeColor(\"darkGrey\", \"7\")'>Batch 7</button></div>"+
                    "<div class='row'><button id='batch8' class='btn batch pink' type='button' onclick='changeColor(\"pink\", \"8\")'>Batch 8</button></div>"+
                    "<div class='row'><button id='batch9' class='btn batch yellowGreen' type='button' onclick='changeColor(\"yellowGreen\", \"9\")'>Batch 9</button></div>"+
                    "<div class='row'><button id='batch10' class='btn batch gold' type='button' onclick='changeColor(\"gold\", \"10\")'>Batch 10</button></div>"+
                    "<div class='row'><button id='batch11' class='btn batch aqua' type='button' onclick='changeColor(\"aqua\", \"10\")'>Batch 11</button></div>"+
                    "<div class='row'><button id='batch12' class='btn batch orchid' type='button' onclick='changeColor(\"orchid\", \"10\")'>Batch 12</button></div>"+
                    "<input type='hidden' id='batch1-samples' name='batch1-samples' class='batch'>" +
                    "<input type='hidden' id='batch2-samples' name='batch2-samples' class='batch'>" +
                    "<input type='hidden' id='batch3-samples' name='batch3-samples' class='batch'>" +
                    "<input type='hidden' id='batch4-samples' name='batch4-samples' class='batch'>" +
                    "<input type='hidden' id='batch5-samples' name='batch5-samples' class='batch'>" +
                    "<input type='hidden' id='batch6-samples' name='batch6-samples' class='batch'>" +
                    "<input type='hidden' id='batch7-samples' name='batch7-samples' class='batch'>" +
                    "<input type='hidden' id='batch8-samples' name='batch8-samples' class='batch'>" +
                    "<input type='hidden' id='batch9-samples' name='batch9-samples' class='batch'>" +
                    "<input type='hidden' id='batch10-samples' name='batch10-samples' class='batch'>" +
                    "<input type='hidden' id='batch11-samples' name='batch11-samples' class='batch'>" +
                    "<input type='hidden' id='batch12-samples' name='batch12-samples' class='batch'>";
    multiselect_temp += "</div>";
    multiselect_temp += "</div></div>"; //close row and added class
    $("#multiselect_temp").append(multiselect_temp);
}


export function reload_multiselect_transcriptome(){
    var rightSelected_template = [];
    for (var i=1; i<MAX_LEVELS; i++) rightSelected_template.push('#multi_d_rightSelected_'+i);
    var right_template = [];
    for (i=1; i<MAX_LEVELS; i++) right_template.push('#multi_d_to_'+i);
    var leftSelected_template = [];
    for (i=1; i<MAX_LEVELS; i++) leftSelected_template.push('#multi_d_leftSelected_'+i);
    var rightAll_template = [];
    for (i=1; i<MAX_LEVELS; i++) rightAll_template.push('#multi_d_rightAll_'+i);
    var leftAll_template = [];
    for (i=1; i<MAX_LEVELS; i++) leftAll_template.push('#multi_d_leftAll_'+i);

    $('#multi_d').multiselect({
        rightSelected:  rightSelected_template.join(),
        right: right_template.join(),
        leftSelected: leftSelected_template.join(),
        rightAll: rightAll_template.join(),
        leftAll: leftAll_template.join(),

        search: {
            left: '<br><br><input type="text" name="q" class="form-control" placeholder="Filter samples (type part of the name)" />'
        },

        moveToRight: function(Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id'); // the button is: multi_d_rightSelected_* or multi_d_rightAll_*
            var i = button.split('_').pop(); //destination box number.
            if (button == 'multi_d_rightSelected_'+i) {
                var $left_options = Multiselect.$left.find('> option:selected');
                Multiselect.$right.eq(i-1).append($left_options);

                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(i-1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(i-1));
                }
            } else if (button == 'multi_d_rightAll_'+i) {
                var $left_options = Multiselect.$left.children(':visible');
                Multiselect.$right.eq(i-1).append($left_options);

                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(i-1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(i-1));
                }
            }
            $(".added").find("option:selected").removeAttr("selected");
        },

        moveToLeft: function(Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id'); // the button is: multi_d_leftSelected_* or multi_d_leftAll_*
            var i = button.split('_').pop(); //destination box number.
            if (button == 'multi_d_leftSelected_'+i) {
                var $right_options = Multiselect.$right.eq(i-1).find('> option:selected');
                Multiselect.$left.append($right_options);

                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_leftAll_'+i) {
                var $right_options = Multiselect.$right.eq(i-1).children(':visible');
                Multiselect.$left.append($right_options);

                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            }
            $(".added").find("option:selected").removeAttr("selected");
        }
    });
}



$(document).ready(function(){
    //Handle in events of objects that not created yet. It is possible because the parent: #multiselect_temp exists
    $('#multiselect_temp').on('click', '#batch_effect', function(event){
        if ($(this).val() == "Add Batch Effect"){
            $(this).val("Remove Batch Effect");
            $(".batch").show();
            $("#more_batches").show();
            for (var i = 1; i<=MAX_BATCHES; i++) {
                $("#batch"+i+"-samples").val('');
            }
            for (i = 3; i<=MAX_BATCHES; i++) {
                $("#batch"+i).hide();
            }
        } else {
            $(this).val("Add Batch Effect");
            $(".batch").hide();
            $("#more_batches").hide();
            $('*[class^="selectedcolor_"]').removeClass();
            for (var i = 1; i<=MAX_BATCHES; i++) {
                $("#batch"+i+"-samples").val('');
            }

        }
    });

    $('#multiselect_temp').on('click', '#more_batches', function(event){
        for (var i = 3; i<=MAX_BATCHES; i++) {
            if (!$("#batch"+i).is(":visible")) {
                $("#batch"+i).show();
                break;
            }
        }
    });

    $('#multiselect_temp').on('click', '#add_category', function(event){
        var num_categories = $("#num_categories").val();
        var next_category_class = "catg_"+(parseInt(num_categories)+1);
        $('.'+next_category_class).show();
        $("#category_name_"+(parseInt(num_categories)+1)).attr('required', true);
        //$("#multi_d_to_"+(parseInt(num_categories)+1)).attr('required', true);
        if (num_categories<MAX_LEVELS){
            $("#num_categories").val(parseInt(num_categories)+1);
        }
    });

    $('#multiselect_temp').on('click', '#remove_category', function(event){
        var num_categories = $("#num_categories").val();
        if (num_categories>2){
            var last_category_class = "catg_"+num_categories;
            $('.'+last_category_class).hide();
            $("#category_name_"+num_categories).removeAttr('required');
            //$("#multi_d_to_"+num_categories).removeAttr('required');
            $("#num_categories").val(parseInt(num_categories)-1);
        }
    });
});

