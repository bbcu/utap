//import this file as ES6 "module" - <script type="module" src="on_get_samples_trascriptome.js"></script>

import {get_samples_list} from "./get-samples.1.0.js";
import {free_space} from "./get-samples.1.0.js";
import {dirname} from "./get-samples.1.0.js";
import {add_multiselect_transcriptome} from "./boxes-transcriptome.1.0.js";
import {reload_multiselect_transcriptome} from "./boxes-transcriptome.1.0.js";
import {hide_all_boxes_transcriptome} from "./boxes-transcriptome.1.0.js";
import {add_multiselect_chromatin} from "./boxes-chromatin.1.0.js";
import {reload_multiselect_chromatin} from "./boxes-chromatin.1.0.js";
import {hide_all_boxes_chromatin} from "./boxes-chromatin.1.0.js";
import {get_report_dir_name} from "./boxes-transcriptome.1.0.js";
import {add_UMI_len} from "./boxes-transcriptome.1.0.js";



var input_folder = '';
var output_folder = '';
var bcl_files = '';
var fastq_file_1 = '';
var fastq_file_2 = '';
var fastq_file_I = '';
var fastq_folder = '';

function validate_input_folder() {
    var lab_path = $("#id_lab_path").val();
    if ($("#id_input_folder").val()) {
    if ( $("#id_input_folder").val() != input_folder) {
        if ($("#id_input_folder").val().startsWith('./')){
            $("#id_input_folder").val(lab_path + $("#id_input_folder").val().substr(1));
        }
        input_folder = $("#id_input_folder").val();
        $("#id_output_folder").val(dirname(input_folder));
        output_folder = $("#id_output_folder").val();
        $(".added").remove();
        $("#id_deseq_run").val('no');
        $("#id_treat_vs_control").val('no');
        free_space();
        var pipeline = $("#id_chosen_pipeline").val();
        var path = $("#id_input_folder").val();
        var response = get_samples_list(path, pipeline, false);//It is not submit because in submit there is no button for choosing input folder - so we don't arrive to current function.
        on_get_samples_list(pipeline, path, response[0], response[1], false, false);//success (true/false), data_out(object with samples/failed, deseq(true/false), is_submit(true/false)
    }}
    if ($("#id_output_folder").val()){
    if ($("#id_output_folder").val() != output_folder) {
        if ($("#id_output_folder").val().startsWith('./')){
            $("#id_output_folder").val(lab_path + $("#id_output_folder").val().substr(1));
        }
        if ($("#id_output_folder").val().startsWith(input_folder)){
            alert("Output folder cannot to be inside the input folder");
            $("#id_output_folder").val('');
        } else {
            $.ajax({
                url: '/ajax/folder_exists/',
                dataType: 'json',
                data: {
                    'folder': $("#id_output_folder").val()
                },
                success: free_space(),
                error: function(data_out, textStatus, errorThrown) {
                    var json = JSON.parse(data_out.responseText);
                    alert(json.message);//message with error that output folder don't exist
                    $("#id_output_folder").val('');
                }
            });
        }
        output_folder = $("#id_output_folder").val();
    }}
    if ($("#id_bcl_files").val()){
    if ($("#id_bcl_files").val() != bcl_files) {
        if ($("#id_bcl_files").val().startsWith('./')){
            $("#id_bcl_files").val(lab_path + $("#id_bcl_files").val().substr(1));
        }
        bcl_files = $("#id_bcl_files").val();
        $("#id_output_folder").val(dirname(bcl_files));
        output_folder = $("#id_output_folder").val();
    }}
    if ($("#id_fastq_folder").val()){
    if ($("#id_fastq_folder").val() != fastq_folder) {
        if ($("#id_fastq_folder").val().startsWith('./')){
            $("#id_fastq_folder").val(lab_path + $("#id_fastq_folder").val().substr(1));
        }
        fastq_folder = $("#id_fastq_folder").val();
        $("#id_output_folder").val(dirname(fastq_folder));
        output_folder = $("#id_output_folder").val();
    }}
    if ($("#id_fastq_file_R1").val()){
    if ($("#id_fastq_file_R1").val() != fastq_file_1) {
        if ($("#id_fastq_file_R1").val().startsWith('./')){
            $("#id_fastq_file_R1").val(lab_path + $("#id_fastq_file_R1").val().substr(1));
        }
        fastq_file_1 = $("#id_fastq_file_R1").val();
        $("#id_output_folder").val(dirname(fastq_file_1));
        output_folder = $("#id_output_folder").val();
    }}
    if ($("#id_fastq_file_R2").val()){
    if ($("#id_fastq_file_R2").val() != fastq_file_2) {
        if ($("#id_fastq_file_R2").val().startsWith('./')){
            $("#id_fastq_file_R2").val(lab_path + $("#id_fastq_file_R2").val().substr(1));
        }
        fastq_file_2 = $("#id_fastq_file_R2").val();
    }}
    if ($("#id_fastq_file_I").val()){
    if ($("#id_fastq_file_I").val() != fastq_file_I) {
        if ($("#id_fastq_file_I").val().startsWith('./')){
            $("#id_fastq_file_I").val(lab_path + $("#id_fastq_file_I").val().substr(1));
        }
        fastq_file_I = $("#id_fastq_file_I").val();
    }}
};


//success (true/false), data_out(object with samples/failed, deseq(true/false),is_submit(true/false)
function on_get_samples_list(pipeline, path, success, data_out, create_boxes, is_submit) {
    var deseq_from_matrix = pipeline.startsWith("DESeq2",0)
    if (success) {
//       if (pipeline.indexOf("Transcriptome" ) !== -1 || pipeline.indexOf("DESeq") !== -1) {
//          $("#id_deseq_run").val('yes');
//         }
        if (data_out.samples.length === 1) {
            if (create_boxes) {
                alert("Cannot create groups of samples. There is only one sample.")
            }
            $("#id_deseq_run").val('no');
            $("#id_treat_vs_control").val('no');
        } else if (data_out.samples) {
            if (create_boxes){
                if (pipeline.indexOf("Transcriptome") !== -1 || deseq_from_matrix) {
                    add_multiselect_transcriptome(data_out.samples);
                    hide_all_boxes_transcriptome();
                    reload_multiselect_transcriptome();
                } else {
                    add_multiselect_chromatin(data_out.samples);
                    hide_all_boxes_chromatin();
                    reload_multiselect_chromatin();
                }
                get_report_dir_name();
                if (pipeline.indexOf("MARS") !== -1 ) {
                    document.getElementById('id_report_name').setAttribute('title', 'Report directories names: There will be 2 directories:\nname_date(D/M/Y)_time(H/M/S)\nname_umi_date(D/M/Y)_time(H/M/S)');
                } else {
                    document.getElementById('id_report_name').setAttribute('title', 'Report directory name: There will be one directory:\nname_date(D/M/Y)_time(H/M/S)');
                }
            } else if (!is_submit) { //For now we not pop up this message in submit form
                if(deseq_from_matrix){
                    alert("The input file contains the following samples:\n" + data_out.samples.join("\n"));
                } else {
                    alert("The input folder contains these samples:\n" + data_out.samples.join("\n"));
                    }
            }
        }
    } else {
        if (path){
            var json = JSON.parse(data_out.responseText);
            if(deseq_from_matrix){
                alert(json.message);
            } else {
                alert(json.message + " in root input folder: " + path);
            }
            $("#id_deseq_run").val('no');
            $("#id_treat_vs_control").val('no');
            $("#id_input_folder").val('');
            $("#id_output_folder").val('');
            input_folder = '';
        }
        else {
            if (create_boxes) {
                if(deseq_from_matrix){
                    alert("Select input file");
                } else {
                    alert("Select input folder");
                }
            }
            $("#id_deseq_run").val('no');
            $("#id_treat_vs_control").val('no');
        }
    }
}


function refresh_sample_boxes() {
    $(".added").remove();
    $(".report_name_added").remove();
    if ($("#id_deseq_run").val() === 'yes' || $("#id_treat_vs_control").val() === 'yes'){
        var pipeline = $("#id_chosen_pipeline").val();
        var is_submit = pipeline.indexOf("Submit")>=0;
        var path = $("#id_input_folder").val();
        var response = get_samples_list(path, pipeline, is_submit);//It is submit pipeline (the input folder is on stefan server)
        on_get_samples_list(pipeline, path, response[0], response[1], true, is_submit);//success (true/false), data_out(object with samples/failed, deseq(true/false), is_submit(true/false)
    }
}






$(document).ready(function(){
    //TODO: For development (deny the need to select input folder each time you want to get boxes). Comment out this line in the production.
    //$("#id_treat_vs_control").val('yes');
    //$("#id_deseq_run").val('yes');
    //$("#id_input_folder").val('/home/labs/bioservices/services/devel_env/UTAP-data-devel/utap-output/admin/input-folder');
    //refresh_sample_boxes();
    //TODO: END

    //fb_show is class of the fileBrowser button
    $(".fb_show").click(function () {
        setInterval(validate_input_folder, 1000);
    });

    //For run deseq again.
    if ($("#id_chosen_pipeline").val()){
        if ($("#id_chosen_pipeline").val().endsWith("DESeq2")){
            $("#id_deseq_run").val('yes');//if in the first run deseq not run
            refresh_sample_boxes();
        }
    }

    //Update the samples list when user select input folder
    $("#id_deseq_run").change(function () {
        refresh_sample_boxes();
    });

    $("#id_treat_vs_control").change(function () {
        refresh_sample_boxes();
    });

    $("#id_UMI").change(function () {
        add_UMI_len();
    });
});
