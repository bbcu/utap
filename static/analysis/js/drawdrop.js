
$(document).ready(function(){
    let next = 2;
    $(".add-more").click(function(e){
        e.preventDefault();
        var last= "#field" + next;
        next = next + 1;
        var Input = '<br><br></td><td><input id="field' + next + '" name="OPT_FCT' + next + '" type="text" placeholder="Level_' + next + '" data-items="8" required></td></tr>';
        $(Input).insertAfter($(last));
        $("#OPT_FCTN").val(next);
    });

    $('.remove-me').click(function(e){
        e.preventDefault();
        var fieldNum = $("#OPT_FCTN").val();
        if (fieldNum > 2) {
                var fieldID = "#field" + fieldNum;
                $(fieldID).remove();
                $("#OPT_FCTN").val(fieldNum-1);
                next = fieldNum-1;
        }
    });
});


function validateForm() {
    var elements = document.forms['frm'].elements;
    var j = 0;
    var flag_samples = false;
    for (var i = 0, element; element = elements[i++];) {
        if (element.name.includes("OPT_SAMP_")) {
            flag_samples = true;
            if (element.value && element.value != "") {
                    j++;
            //              alert("Drag and drop the sample " + element.name.substring(9) + " to one of the factors fields");

            //              return false;
            }
        }
    }
    if(flag_samples && j<2) {
        alert("Drag and drop at least 2 samples to their factors fields");
        return false;
    }
}




$(document).ready(function() {

    interact('.draggable')
    .draggable({
        // enable inertial throwing
        inertia: true,
        // keep the element within the area of it's parent
        restrict: {
          restriction: "parent",
          endOnly: true,
          elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
        },
        // enable autoScroll
        autoScroll: true,

        // call this function on every dragmove event
        onmove: dragMoveListener,
        // call this function on every dragend event
        onend: function (event) {
          var textEl = event.target.querySelector('p');
          textEl && (textEl.textContent = 'moved a distance of ' + (Math.sqrt(event.dx * event.dx + event.dy * event.dy)|0) + 'px');
        }
    });

    function dragMoveListener (event) {
        var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        // translate the element
        target.style.webkitTransform =
        target.style.transform =
          'translate(' + x + 'px, ' + y + 'px)';

        // update the posiion attributes
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
    }

    // this is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener;
    /* The dragging code for '.draggable' from the demo above
    * applies to this demo as well so it doesn't have to be repeated. */

    // enable draggables to be dropped into this
    interact('.dropzone').dropzone({
        // only accept elements matching this CSS selector
        accept: '.draggable',
        // Require a 75% element overlap for a drop to be possible
        overlap: 0.75,

        // listen for drop related events:

        ondropactivate: function(event) {
          // add active dropzone feedback
          event.target.classList.add('drop-active');
        },
        ondragenter: function(event) {
          var draggableElement = event.relatedTarget,
            dropzoneElement = event.target;

          // feedback the possibility of a drop
          dropzoneElement.classList.add('drop-target');
          draggableElement.classList.add('can-drop');
        },
        ondragleave: function(event) {
          // remove the drop feedback style
          event.target.classList.remove('drop-target');
          event.relatedTarget.classList.remove('can-drop');
              var iddiv = event.relatedTarget.id.substr(4);//removing "div_" prefix.
              document.getElementById(iddiv).value = "";
        },
        ondrop: function(event) {
              var iddiv = event.relatedTarget.id.substr(4);//removing "div_" prefix.
              document.getElementById(iddiv).value = event.target.id;
        },
        ondropdeactivate: function(event) {
          // remove active dropzone feedback
          event.target.classList.remove('drop-active');
          event.target.classList.remove('drop-target');
        }
    });
});

